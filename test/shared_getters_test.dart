// ignore_for_file: prefer_const_constructors
// ignore_for_file: prefer_const_declarations
// ignore_for_file: prefer_const_literals_to_create_immutables

import 'package:captainfoodmaker/data_models/recipe.dart';
import 'package:captainfoodmaker/shared_getters.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  test('selectableLabelColors has expected content', () {
    expect(
      selectableLabelColors,
      [
        Colors.blue,
        Colors.indigo,
        Colors.deepPurple,
        Colors.purple,
        Colors.pink,
        Colors.red,
        Colors.deepOrange,
        Colors.orange,
        Colors.amber,
        Colors.yellow,
        Colors.lime,
        Colors.lightGreen,
        Colors.green,
        Colors.teal,
        Colors.cyan,
        Colors.lightBlue,
        Colors.blueGrey,
        Colors.brown,
        Colors.grey,
      ],
    );
  });

  test('editedRecipes has expected initial content', () {
    expect(editedRecipes, <String, Recipe>{});
  });

  test('`CompletedIngredient`s are `equal` when they have same content', () {
    expect(
      CompletedIngredient(Ingredient("PC", "load"), recipeTitle: "letter"),
      isNot(CompletedIngredient(Ingredient("TPS", "cover"), recipeTitle: "sheet")),
    );
    expect(
      CompletedIngredient(Ingredient("PC", "load"), recipeTitle: "load letter"),
      CompletedIngredient(Ingredient("PC", "load"), recipeTitle: "load letter"),
    );
  });

  test('`CompletedStep`s are `equal` when they have same content', () {
    expect(
      CompletedStep("PC", recipeTitle: "load letter"),
      isNot(CompletedStep("TPS", recipeTitle: "cover sheet")),
    );
    expect(
      CompletedStep("PC", recipeTitle: "load letter"),
      CompletedStep("PC", recipeTitle: "load letter"),
    );
  });
}
