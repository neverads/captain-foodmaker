import 'package:captainfoodmaker/data_models/recipe.dart';
import 'package:flutter_test/flutter_test.dart';

// ignore_for_file: prefer_const_constructors
// ignore_for_file: prefer_const_declarations
// ignore_for_file: prefer_const_literals_to_create_immutables

void main() {
  group('Recipe', () {
    group('on init', () {
      test('has attributes, including url', () {
        final recipe = Recipe(
          "my_recipe",
          labels: ["0", "1"],
          ingredients: [
            Ingredient("11", "things"),
            Ingredient("12", "stuff"),
          ],
          steps: [
            "do stuff",
            "do more stuff",
          ],
          url: "http://www.neverads.org",
          isDeleted: true,
        );

        expect(recipe.title, "my_recipe");
        expect(recipe.labels, ["0", "1"]);
        expect(
          recipe.ingredients,
          [
            Ingredient("11", "things"),
            Ingredient("12", "stuff"),
          ],
        );
        expect(
          recipe.steps,
          [
            "do stuff",
            "do more stuff",
          ],
        );
        expect(recipe.url, "http://www.neverads.org");
        expect(recipe.isDeleted, true);

        expect(recipe.labelsIndexes, Iterable<int>.generate(recipe.labelsIndexes.length));
        expect(recipe.ingredientsIndexes, Iterable<int>.generate(recipe.ingredientsIndexes.length));
        expect(recipe.stepsIndexes, Iterable<int>.generate(recipe.steps.length));

        final expectedProps = [];
        expectedProps.add(recipe.title);
        expectedProps.addAll(recipe.labels);
        expectedProps.addAll(recipe.ingredients);
        expectedProps.addAll(recipe.steps);
        expectedProps.add(recipe.url);
        expect(recipe.props, expectedProps);
        expect(
          recipe.toString(),
          "my_recipe<DELETED>\n"
          "\n"
          "Labels:\n"
          "0, 1\n"
          "\n"
          "Ingredients:\n"
          "• 11 things\n"
          "• 12 stuff\n"
          "\n"
          "Steps:\n"
          "1) do stuff\n"
          "2) do more stuff\n"
          "\n"
          "Web-link:\n"
          "http://www.neverads.org",
        );
      });

      test('has attributes, excluding url', () {
        final recipe = Recipe(
          "my_recipe",
          labels: ["0", "1"],
          ingredients: [
            Ingredient("11", "things"),
            Ingredient("12", "stuff"),
          ],
          steps: [
            "do stuff",
            "do more stuff",
          ],
        );

        expect(recipe.title, "my_recipe");
        expect(recipe.labels, ["0", "1"]);
        expect(
          recipe.ingredients,
          [
            Ingredient("11", "things"),
            Ingredient("12", "stuff"),
          ],
        );
        expect(
          recipe.steps,
          [
            "do stuff",
            "do more stuff",
          ],
        );
        expect(recipe.url, null);
        expect(recipe.isDeleted, false);

        expect(
          recipe.props,
          [
            "my_recipe",
            "0",
            "1",
            Ingredient("11", "things"),
            Ingredient("12", "stuff"),
            "do stuff",
            "do more stuff",
            null,
          ],
        );
        expect(
          recipe.toString(),
          "my_recipe\n"
          "\n"
          "Labels:\n"
          "0, 1\n"
          "\n"
          "Ingredients:\n"
          "• 11 things\n"
          "• 12 stuff\n"
          "\n"
          "Steps:\n"
          "1) do stuff\n"
          "2) do more stuff\n"
          "\n"
          "Web-link:\n"
          "null",
        );
      });
    });

    test('factory for "blank" recipe', () {
      expect(
        Recipe.blank,
        Recipe("", labels: [], ingredients: [], steps: [], url: null),
      );
      expect(Recipe.blank.title, "");
      expect(Recipe.blank.labels.length, 0);
      expect(Recipe.blank.ingredients.length, 0);
      expect(Recipe.blank.steps.length, 0);
      expect(Recipe.blank.url, null);
    });

    group('equality', () {
      test('empty values', () {
        expect(
          Recipe("", labels: [], ingredients: [], steps: []),
          Recipe("", labels: [], ingredients: [], steps: []),
        );
      });

      test('typical values', () {
        expect(
          Recipe(
            "chocolate chip cookies",
            labels: ["sweet", "dessert"],
            ingredients: [Ingredient("2", "things")],
            steps: ["do stuff", "do more stuff"],
            url: "http://www.neverads.org",
          ),
          Recipe(
            "chocolate chip cookies",
            labels: ["sweet", "dessert"],
            ingredients: [Ingredient("2", "things")],
            steps: ["do stuff", "do more stuff"],
            url: "http://www.neverads.org",
          ),
        );
      });
    });

    test('fromString parses correctly', () {
      expect(
        Recipe.fromString("\n\nLabels:\n\n\nIngredients:\n\n\nSteps:\n\n\nWeb-link:\nnull"),
        Recipe("", labels: [], ingredients: [], steps: []),
      );
      expect(
        Recipe.fromString("<DELETED>\n\nLabels:\n\n\nIngredients:\n\n\nSteps:\n\n\nWeb-link:\nnull"),
        Recipe("", labels: [], ingredients: [], steps: [], isDeleted: true),
      );
      expect(
        Recipe.fromString("\n\nLabels:\n\n\nIngredients:\n\n\nSteps:\n1) \n\nWeb-link:\nnull"),
        Recipe("", labels: [], ingredients: [], steps: []),
      );
      expect(
        Recipe.fromString("\n\nLabels:\n\n\nIngredients:\n\n\nSteps:\n1) things\n2) stuff\n\nWeb-link:\nnull"),
        Recipe("", labels: [], ingredients: [], steps: ["things", "stuff"]),
      );
      expect(
        Recipe.fromString(
            "initech<DELETED>\n\nLabels:\nTPS, reports\n\nIngredients:\n• 2c flour\n• 1/2 cup butter\n\nSteps:\n1) PC\n2) load letter\n\nWeb-link:\nneverads.org"),
        Recipe("initech",
            labels: ["TPS", "reports"],
            ingredients: [Ingredient("2c", "flour"), Ingredient("1/2 cup", "butter")],
            steps: ["PC", "load letter"],
            url: "neverads.org",
            isDeleted: true),
      );
    });

    test('fromString & toString work together as expected', () {
      final recipe = Recipe(
        "initech",
        labels: ["TPS", "reports"],
        ingredients: [Ingredient("2c", "flour"), Ingredient("", "butter")],
        steps: ["PC", "load letter"],
        url: "neverads.org",
        isDeleted: true,
      );
      final recipeAsString =
          "initech<DELETED>\n\nLabels:\nTPS, reports\n\nIngredients:\n• 2c flour\n• butter\n\nSteps:\n1) PC\n2) load letter\n\nWeb-link:\nneverads.org";
      expect(
        Recipe.fromString(recipeAsString),
        recipe,
      );
      expect(
        recipeAsString,
        recipe.toString(),
      );
      expect(
        recipeAsString,
        Recipe.fromString(recipeAsString).toString(),
      );
    });

    group('copyWith', () {
      final simpleRecipe = Recipe(
        "TPS",
        labels: ["report", "cover sheet"],
        ingredients: [Ingredient("", "stuff"), Ingredient("", "things")],
        steps: ["PC", "load letter"],
        url: "initech.net",
      );

      test('only default args, returns same recipe', () {
        final copy = simpleRecipe.copyWith();
        expect(copy.title, simpleRecipe.title);
        expect(copy.labels, simpleRecipe.labels);
        expect(copy.ingredients, simpleRecipe.ingredients);
        expect(copy.steps, simpleRecipe.steps);
        expect(copy.url, simpleRecipe.url);
        expect(copy.isDeleted, simpleRecipe.isDeleted);
        expect(copy, simpleRecipe);
      });

      group('blankElementsRemoved = true', () {
        test('_with_ empty/blank elements -- removes from lists', () {
          final recipeWithBlankElements = Recipe(
            "TPS",
            labels: ["report", "  ", "cover sheet"],
            ingredients: [Ingredient.empty, Ingredient("", "  "), Ingredient("", "stuff"), Ingredient("", "things")],
            steps: ["\t", "PC", "load letter", "  \n"],
            url: "initech.net",
          );

          final copy = recipeWithBlankElements.copyWith(blankElementsRemoved: true);

          expect(copy.title, recipeWithBlankElements.title);

          expect(copy.labels.length, 2);
          expect(copy.labels[0], recipeWithBlankElements.labels[0]);
          expect(copy.labels[1], recipeWithBlankElements.labels[2]);

          expect(copy.ingredients.length, 2);
          expect(copy.ingredients[0], recipeWithBlankElements.ingredients[2]);
          expect(copy.ingredients[1], recipeWithBlankElements.ingredients[3]);

          expect(copy.steps.length, 2);
          expect(copy.steps[0], recipeWithBlankElements.steps[1]);
          expect(copy.steps[1], recipeWithBlankElements.steps[2]);

          expect(copy.url, recipeWithBlankElements.url);
        });

        test('_without empty/blank_ elements -- no effect', () {
          final copy = simpleRecipe.copyWith(blankElementsRemoved: true);

          expect(copy.title, simpleRecipe.title);

          expect(copy.labels.length, simpleRecipe.labels.length);
          for (int labelIndex in copy.labelsIndexes) {
            expect(copy.labels[labelIndex], simpleRecipe.labels[labelIndex]);
          }

          expect(copy.ingredients.length, simpleRecipe.ingredients.length);
          for (int ingredientIndex in copy.ingredientsIndexes) {
            expect(copy.ingredients[ingredientIndex], simpleRecipe.ingredients[ingredientIndex]);
          }

          expect(copy.steps.length, simpleRecipe.steps.length);
          for (int stepIndex in copy.stepsIndexes) {
            expect(copy.steps[stepIndex], simpleRecipe.steps[stepIndex]);
          }

          expect(copy.url, simpleRecipe.url);
        });
      });

      group('addedIngredient', () {
        test('adds ingredient to ingredients', () {
          final newIngredient = Ingredient("15", "pieces of flair");
          final copy = simpleRecipe.copyWith(addedIngredient: newIngredient);

          expect(copy.title, simpleRecipe.title);

          expect(copy.labels.length, 2);
          expect(copy.labels[0], simpleRecipe.labels[0]);
          expect(copy.labels[1], simpleRecipe.labels[1]);

          expect(copy.ingredients.length, 3);
          expect(copy.ingredients[0], simpleRecipe.ingredients[0]);
          expect(copy.ingredients[1], simpleRecipe.ingredients[1]);
          expect(copy.ingredients[2], newIngredient);

          expect(copy.steps.length, 2);
          expect(copy.steps[0], simpleRecipe.steps[0]);
          expect(copy.steps[1], simpleRecipe.steps[1]);

          expect(copy.url, simpleRecipe.url);
        });
      });

      group('addedStep', () {
        test('adds string to steps', () {
          final newStep = "destroy copier";
          final copy = simpleRecipe.copyWith(addedStep: newStep);

          expect(copy.title, simpleRecipe.title);

          expect(copy.labels.length, 2);
          expect(copy.labels[0], simpleRecipe.labels[0]);
          expect(copy.labels[1], simpleRecipe.labels[1]);

          expect(copy.ingredients.length, 2);
          expect(copy.ingredients[0], simpleRecipe.ingredients[0]);
          expect(copy.ingredients[1], simpleRecipe.ingredients[1]);

          expect(copy.steps.length, 3);
          expect(copy.steps[0], simpleRecipe.steps[0]);
          expect(copy.steps[1], simpleRecipe.steps[1]);
          expect(copy.steps[2], newStep);

          expect(copy.url, simpleRecipe.url);
        });
      });

      group('addedLabel', () {
        test('adds string to labels', () {
          final newLabel = "TPS";
          final copy = simpleRecipe.copyWith(addedLabel: newLabel);

          expect(copy.title, simpleRecipe.title);

          expect(copy.labels.length, 3);
          expect(copy.labels[0], simpleRecipe.labels[0]);
          expect(copy.labels[1], simpleRecipe.labels[1]);
          expect(copy.labels[2], newLabel);

          expect(copy.ingredients.length, 2);
          expect(copy.ingredients[0], simpleRecipe.ingredients[0]);
          expect(copy.ingredients[1], simpleRecipe.ingredients[1]);

          expect(copy.steps.length, 2);
          expect(copy.steps[0], simpleRecipe.steps[0]);
          expect(copy.steps[1], simpleRecipe.steps[1]);

          expect(copy.url, simpleRecipe.url);
        });
      });

      group('trimming = true', () {
        test('_with_ whitespace -- trimmed', () {
          final recipeWithWhitespace = Recipe(
            " TPS",
            labels: ["report\n", "  cover sheet"],
            ingredients: [Ingredient("\t", " stuff"), Ingredient(" ", "things  ")],
            steps: ["PC", "load letter"],
            url: "initech.net",
          );

          final copy = recipeWithWhitespace.copyWith(elementsTrimmed: true);

          expect(copy.title, recipeWithWhitespace.title.trim());

          expect(copy.labels.length, 2);
          expect(copy.labels[0], recipeWithWhitespace.labels[0].trim());
          expect(copy.labels[1], recipeWithWhitespace.labels[1].trim());

          expect(copy.ingredients.length, 2);
          expect(copy.ingredients[0].amount, recipeWithWhitespace.ingredients[0].amount.trim());
          expect(copy.ingredients[0].description, recipeWithWhitespace.ingredients[0].description.trim());
          expect(copy.ingredients[1].amount, recipeWithWhitespace.ingredients[1].amount.trim());
          expect(copy.ingredients[1].description, recipeWithWhitespace.ingredients[1].description.trim());

          expect(copy.steps.length, 2);
          expect(copy.steps[0], recipeWithWhitespace.steps[0].trim());
          expect(copy.steps[1], recipeWithWhitespace.steps[1].trim());

          expect(copy.url, recipeWithWhitespace.url!.trim());
        });

        test('_without_ whitespace -- no effect', () {
          final copy = simpleRecipe.copyWith(elementsTrimmed: true);

          expect(copy.title, simpleRecipe.title);

          expect(copy.labels.length, simpleRecipe.labels.length);
          for (int labelIndex in copy.labelsIndexes) {
            expect(copy.labels[labelIndex], simpleRecipe.labels[labelIndex]);
          }

          expect(copy.ingredients.length, simpleRecipe.ingredients.length);
          for (int ingredientIndex in copy.ingredientsIndexes) {
            expect(copy.ingredients[ingredientIndex], simpleRecipe.ingredients[ingredientIndex]);
          }

          expect(copy.steps.length, simpleRecipe.steps.length);
          for (int stepIndex in copy.stepsIndexes) {
            expect(copy.steps[stepIndex], simpleRecipe.steps[stepIndex]);
          }

          expect(copy.url, simpleRecipe.url);
        });
      });

      test('isDeleted', () {
        final copyDeleted = simpleRecipe.copyWith(isDeleted: true);

        expect(copyDeleted.title, simpleRecipe.title.trim());

        expect(copyDeleted.labels.length, 2);
        expect(copyDeleted.labels[0], simpleRecipe.labels[0].trim());
        expect(copyDeleted.labels[1], simpleRecipe.labels[1].trim());

        expect(copyDeleted.ingredients.length, 2);
        expect(copyDeleted.ingredients[0].amount, simpleRecipe.ingredients[0].amount.trim());
        expect(copyDeleted.ingredients[0].description, simpleRecipe.ingredients[0].description.trim());
        expect(copyDeleted.ingredients[1].amount, simpleRecipe.ingredients[1].amount.trim());
        expect(copyDeleted.ingredients[1].description, simpleRecipe.ingredients[1].description.trim());

        expect(copyDeleted.steps.length, 2);
        expect(copyDeleted.steps[0], simpleRecipe.steps[0].trim());
        expect(copyDeleted.steps[1], simpleRecipe.steps[1].trim());

        expect(copyDeleted.url, simpleRecipe.url!.trim());

        expect(copyDeleted.isDeleted, true);

        final copyRestored = copyDeleted.copyWith(isDeleted: false);

        expect(copyRestored.title, simpleRecipe.title.trim());

        expect(copyRestored.labels.length, 2);
        expect(copyRestored.labels[0], simpleRecipe.labels[0].trim());
        expect(copyRestored.labels[1], simpleRecipe.labels[1].trim());

        expect(copyRestored.ingredients.length, 2);
        expect(copyRestored.ingredients[0].amount, simpleRecipe.ingredients[0].amount.trim());
        expect(copyRestored.ingredients[0].description, simpleRecipe.ingredients[0].description.trim());
        expect(copyRestored.ingredients[1].amount, simpleRecipe.ingredients[1].amount.trim());
        expect(copyRestored.ingredients[1].description, simpleRecipe.ingredients[1].description.trim());

        expect(copyRestored.steps.length, 2);
        expect(copyRestored.steps[0], simpleRecipe.steps[0].trim());
        expect(copyRestored.steps[1], simpleRecipe.steps[1].trim());

        expect(copyRestored.url, simpleRecipe.url!.trim());

        expect(copyRestored.isDeleted, false);
      });
    });

    test('isDeleted is mutable', () {
      final recipe = Recipe(
        "my_recipe",
        labels: ["0", "1"],
        ingredients: [
          Ingredient("11", "things"),
          Ingredient("12", "stuff"),
        ],
        steps: [
          "do stuff",
          "do more stuff",
        ],
        url: "http://www.neverads.org",
      );
      expect(recipe.isDeleted, false);

      recipe.isDeleted = true;
      expect(recipe.isDeleted, true);
    });
  });

  group('defaultRecipes', () {
    test('are convertible to & from strings', () {
      for (final recipe in defaultRecipes) {
        expect(recipe, Recipe.fromString(recipe.toString()));
      }
    });
    test('recipes are as expected', () {
      expect(
        defaultRecipes,
        [
          Recipe(
            'Chocolate Sandwich Cookies',
            labels: [
              'sweet',
            ],
            ingredients: [
              Ingredient('8 oz', 'butter, softened'),
              Ingredient('7.5 oz', 'sugar'),
              Ingredient('2 tsp', 'salt'),
              Ingredient('2', 'eggs'),
              Ingredient('10 oz', 'flour'),
              Ingredient('5.5 oz', 'cocoa powder'),
              Ingredient('1/2 tsp', 'baking soda'),
            ],
            steps: [
              'Heat oven to 160°C.',
              'Cream butter, sugar, and salt in stand mixer until smooth.',
              'Add eggs and mix.',
              'Sift flour, cocoa powder, and baking soda into bowl and mix until combined.',
              'Transfer to floured surface & knead until it comes together.',
              'Wrap & chill for 1 hour.',
              'Roll out & cut into circles with small cookie cutter or champagne glass.',
              'Bake on a cookie tray with silpat at 160°C for 15 min.',
              'Cool on wire rack.',
            ],
          ),
          Recipe(
            'Chocolate Sandwich Cookies (Filling)',
            labels: [
              'sweet',
            ],
            ingredients: [
              Ingredient('4 oz', 'butter'),
              Ingredient('8.8 oz', 'powdered sugar, sifted'),
              Ingredient('1 tsp', 'vanilla'),
            ],
            steps: [
              'Beat all ingredients in stand mixer bowl until smooth.',
              'Add a small amount to the bottom of half the cookies and top with the other half.',
            ],
          ),
          Recipe(
            'Oatmeal Cream Pies (Cookies)',
            labels: [
              'sweet',
            ],
            ingredients: [
              Ingredient('8 oz', 'butter, softened'),
              Ingredient('5.6 oz', 'brown sugar'),
              Ingredient('3.75 oz', 'white sugar'),
              Ingredient('2', 'eggs'),
              Ingredient('1 tsp', 'vanilla'),
              Ingredient('1 Tbsp', 'honey'),
              Ingredient('8.75 oz', 'flour'),
              Ingredient('1 tsp', 'baking soda'),
              Ingredient('1/4 tsp', 'cinnamon'),
              Ingredient('1/2 tsp', 'salt'),
              Ingredient('7.5 oz', 'oats'),
            ],
            steps: [
              'Heat oven to 180°C.',
              'Cream butter & sugars in stand mixer bowl until light & fluffy.',
              'Add eggs, honey, vanilla.  Mix.',
              'In a separate bowl, mix all dry ingredients except oats.  Add to wet and mix.',
              'Fold in oats.',
              'Transfer to a different bowl & refrigerate for 15 minutes.',
              'Bake at 180°C 8-10 minutes until the edges start to brown.',
            ],
          ),
          Recipe(
            'Oatmeal Cream Pies (Marshmallow Frosting)',
            labels: [
              'sweet',
            ],
            ingredients: [
              Ingredient('2', 'egg whites'),
              Ingredient('3.75 oz', 'white sugar'),
              Ingredient('1/4 tsp', 'cream of tartar'),
              Ingredient('1/2 tsp', 'vanilla'),
              Ingredient('1/4 tsp', 'ground clove'),
              Ingredient('1/4 tsp', 'cinnamon'),
            ],
            steps: [
              'In a double boiler, whisk egg whites, sugar, and cream of tartar until eggs become frothy and sugar dissolves.',
              'Pour egg mixture into a stand mixer bowl & whip on high until stiff peaks form.',
              'Fold in vanilla & spices.',
              'Using a piping bag, add frosting to half the cookies & top with the other half.',
            ],
          ),
          Recipe(
            'Tomato Pie',
            labels: [
              'savory',
            ],
            ingredients: [
              Ingredient('1 recipe', 'pie dough'),
              Ingredient('1 kg', 'heirloom tomatoes'),
              Ingredient('1', 'onion, sliced'),
              Ingredient('', 'butter'),
              Ingredient('3 cloves', 'garlic minced'),
              Ingredient('', 'salt & pepper'),
            ],
            steps: [
              'Heat oven to 180°C.',
              'Melt butter in a medium pan.',
              'Add onions & sauté until translucent.',
              'Add garlic & cook a few more minutes.  Add salt & pepper to taste.',
              'Roll out dough & place on a baking tray.',
              'Add sautéed onions & garlic.  Top with all the sliced tomatoes, sprinkling salt on each layer.',
              'Fold edges of dough over tomatoes.',
              'Bake at 180°C until dough is golden brown.',
            ],
          ),
          Recipe(
            'Veggie Pot Pie',
            labels: [
              'savory',
            ],
            ingredients: [
              Ingredient('2', 'medium onions, diced'),
              Ingredient('200 grams', 'oyster mushrooms, chopped'),
              Ingredient('4 stalks', 'celery, diced'),
              Ingredient('250 grams', 'green beans, trimmed and cut'),
              Ingredient('400 grams', 'potatoes, cubed'),
              Ingredient('', 'butter'),
              Ingredient('4 cloves', 'garlic, minced'),
              Ingredient('75 grams', 'flour'),
              Ingredient('2 cups', 'veggie stock'),
              Ingredient('5 sprigs', 'fresh thyme, removed from stems'),
              Ingredient('1/2 cup', 'soy milk'),
              Ingredient('', 'salt & pepper'),
              Ingredient('2x recipe', 'pie dough'),
            ],
            steps: [
              'Heat oven to 180°C.',
              'While dough is chilling, melt butter in a large pan & sauté onions on medium high until translucent.',
              'Add garlic, mushrooms & celery.  Cook another 5 minutes.',
              'Sprinkle flour on veggies & mix to coat.  Cook yet another 5 minutes.',
              'Slowly add veggie stock & mix.  (Should be a bit thick.)  Mix in soy milk.',
              'Add thyme, salt, and pepper.  Remove from heat.',
              'In a large pot, boil potatoes & green beans in salted water for 10 minutes.  Drain & add to sauce.  Mix.',
              'Roll out 2/3 dough.  In an oiled/buttered 9" x 13" glass dish, put the dough on the bottom & up the sides.  '
                  'Make sure there are no holes.',
              'Pour the veggie sauce mixture into the dish.',
              'Roll out the other 1/3 of the dough.  Make it big enough to cover the top of the pie.  '
                  'Cut a few slits in the dough to let steam escape.  Place on top of the pie, and fold or crimp the edges.',
              'Bake for 30-40 minutes at 180°C until golden brown.',
            ],
          ),
        ],
      );
    });
  });
}
