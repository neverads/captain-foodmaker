import 'package:captainfoodmaker/data_models/label_colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

// ignore_for_file: prefer_const_constructors
// ignore_for_file: prefer_const_declarations
// ignore_for_file: prefer_const_literals_to_create_immutables

void main() {
  group('LabelColor', () {
    late LabelColor labelColor;
    setUp(() {
      labelColor = LabelColor("TPS", Colors.red.shade700);
    });

    test('is equal when has same label & color', () {
      expect(labelColor, LabelColor("TPS", Colors.red.shade700));
    });

    group('storage-conversions', () {
      test('toMapEntry has equal key & value', () {
        expect(
          labelColor.toMapEntry().key,
          MapEntry("TPS", Colors.red.shade700.value).key,
        );
        expect(
          labelColor.toMapEntry().value,
          MapEntry("TPS", Colors.red.shade700.value).value,
        );
      });

      test('fromMap', () {
        expect(
          LabelColor.fromMapEntry(MapEntry("TPS", Colors.indigo.value)),
          LabelColor("TPS", Colors.indigo),
        );
      });
    });

    test('lists with same LabelColor contents are equal', () {
      expect(
        [
          LabelColor("TPS", Colors.red.shade700),
          LabelColor("TPS", Colors.cyan.shade500),
        ],
        [
          LabelColor("TPS", Colors.red.shade700),
          LabelColor("TPS", Colors.cyan),
        ],
      );
    });

    test('has attributes', () {
      expect(labelColor.label, "TPS");
      expect(labelColor.color, Colors.red.shade700);
    });

    test('equality when using copy', () {
      final LabelColor copy = labelColor.copy();
      expect(labelColor, copy);
    });

    test('equality (by using copyWith)', () {
      final LabelColor copiedLabelColor = labelColor.copyWith(newColor: Colors.green.shade300);

      expect(labelColor.label, "TPS");
      expect(labelColor.color, Colors.red.shade700);

      expect(copiedLabelColor.label, "TPS");
      expect(copiedLabelColor.color, Colors.green.shade300);

      expect(labelColor.color, isNot(copiedLabelColor.color));
      expect(labelColor, isNot(copiedLabelColor));

      expect(
        copiedLabelColor,
        LabelColor("TPS", Colors.green.shade300),
      );
      expect(
        labelColor.copyWith(newColor: Colors.green.shade300),
        labelColor.copyWith(newColor: Colors.green.shade300),
      );
    });
  });

  group("extension method for getting color from List<LabelColor>", () {
    test("getColor returns value from list", () {
      expect(
        [LabelColor("load", Colors.lime)].getColor("load"),
        Colors.lime,
      );
    });

    test("getColor returns implicit default value", () {
      expect(
        [LabelColor("load", Colors.lime)].getColor("letter"),
        Colors.grey,
      );
    });

    test("getColor returns explicit default value", () {
      expect(
        [LabelColor("load", Colors.lime)].getColor("letter", backupColor: Colors.red),
        Colors.red,
      );
    });
  });

  group("extension method for getting color from List<LabelColor>", () {
    test("setColor updates list", () {
      final List<LabelColor> labelColors = [
        LabelColor("load", Colors.orange),
        LabelColor("letter", Colors.purple),
      ];
      labelColors.setColor("load", Colors.indigo);
      expect(
        labelColors,
        [
          LabelColor("load", Colors.indigo),
          LabelColor("letter", Colors.purple),
        ],
      );
    });

    test("setColor which is not yet in list", () {
      final List<LabelColor> labelColors = [
        LabelColor("PC", Colors.orange),
        LabelColor("load", Colors.purple),
      ];
      labelColors.setColor("letter", Colors.indigo);
      expect(
        labelColors,
        [
          LabelColor("PC", Colors.orange),
          LabelColor("load", Colors.purple),
          LabelColor("letter", Colors.indigo),
        ],
      );
    });
  });

  test("extension method for getting labels from List<LabelColor>", () {
    final List<LabelColor> labelColors = [
      LabelColor("load", Colors.orange),
      LabelColor("letter", Colors.purple),
    ];
    expect(
      labelColors.labels,
      ["load", "letter"],
    );
    expect(
      labelColors.labels,
      labelColors.labels,
    );
  });

  test("extension method for getting colors-values from List<LabelColor>", () {
    /// this can be used to see if the colors part of such a list has changed
    final List<LabelColor> labelColors = [
      LabelColor("load", Colors.orange),
      LabelColor("letter", Colors.purple),
    ];
    expect(
      labelColors.colorsValues,
      [Colors.orange.value, Colors.purple.value],
    );
    expect(
      labelColors.colorsValues,
      labelColors.colorsValues,
    );
  });

  group("extension methods for converting to and from storage", () {
    test("toMap (for storage)", () {
      expect(
        [
          LabelColor("load", Colors.orange),
          LabelColor("letter", Colors.purple),
        ].toMap(),
        {
          "load": Colors.orange.value,
          "letter": Colors.purple.value,
        },
      );
    });

    test("fromMap (from storage)", () {
      final List<LabelColor> labelColors = [];
      labelColors.setFromMap({
        "load": Colors.orange.value,
        "letter": Colors.purple.value,
      });
      expect(
        labelColors,
        [
          LabelColor("load", Colors.orange),
          LabelColor("letter", Colors.purple),
        ],
      );
    });
  });
}
