import 'package:captainfoodmaker/data_models/recipe.dart';
import 'package:flutter_test/flutter_test.dart';

// ignore_for_file: prefer_const_constructors
// ignore_for_file: prefer_const_declarations
// ignore_for_file: prefer_const_literals_to_create_immutables

void main() {
  group('Ingredient', () {
    test('has attributes', () {
      final ingredient = Ingredient("2", "stuff");

      expect(ingredient.amount, "2");
      expect(ingredient.description, "stuff");

      expect(ingredient.props, [ingredient.amount, ingredient.description]);
      expect(ingredient.toString(), "• 2 stuff");
    });

    test('same attributes means equal', () {
      expect(
        Ingredient("2", "stuff"),
        Ingredient("2", "stuff"),
      );
    });

    test('toString', () {
      expect(
        Ingredient("2c", "stuff").toString(),
        "• 2c stuff",
      );
      expect(
        Ingredient("", "butter").toString(),
        "• butter",
      );
    });

    test('fromString() is parsed correctly', () {
      expect(
        Ingredient.fromString(""),
        Ingredient("", ""),
      );
      expect(
        Ingredient.fromString("• "),
        Ingredient("", ""),
      );
      expect(
        Ingredient.fromString("\t"),
        Ingredient("", ""),
      );
      expect(
        Ingredient.fromString("2\tcups  whole wheat   flour"),
        Ingredient("2 cups", "whole wheat flour"),
      );
      expect(
        Ingredient.fromString("stuff"),
        Ingredient("", "stuff"),
      );
      expect(
        Ingredient.fromString("2 cups sugar"),
        Ingredient("2 cups", "sugar"),
      );
      expect(
        Ingredient.fromString("• 2 cups sugar"),
        Ingredient("2 cups", "sugar"),
      );
      expect(
        Ingredient.fromString("2 cups brown sugar"),
        Ingredient("2 cups", "brown sugar"),
      );
      expect(
        Ingredient.fromString("2 large eggs"),
        Ingredient("2", "large eggs"),
      );
      expect(
        Ingredient.fromString("1 egg"),
        Ingredient("1", "egg"),
      );
      // todo -- handle this
      // expect(
      //   Ingredient.fromString("2 eggs, beaten"),
      //   Ingredient("2", "eggs, beaten"),
      // );
      expect(
        Ingredient.fromString("1c flour"),
        Ingredient("1c", "flour"),
      );
      expect(
        Ingredient.fromString("100g butter"),
        Ingredient("100g", "butter"),
      );
      expect(
        Ingredient.fromString("5 plums"),
        Ingredient("5", "plums"),
      );
      expect(
        Ingredient.fromString("1 shortcrust pastry"),
        Ingredient("1", "shortcrust pastry"),
      );
      expect(
        Ingredient.fromString("salt & pepper"),
        Ingredient("", "salt & pepper"),
      );
      expect(
        Ingredient.fromString("• 1 onion, sliced"),
        Ingredient("1", "onion, sliced"),
      );
      expect(
        Ingredient.fromString("• 2 medium onions, diced"),
        Ingredient("2", "medium onions, diced"),
      );
      expect(
        Ingredient.fromString("1 1/2 teaspoons pure vanilla extract"),
        Ingredient("1 1/2 teaspoons", "pure vanilla extract"),
      );
      expect(
        Ingredient.fromString("1 1/2 tsp pure vanilla extract"),
        Ingredient("1 1/2 tsp", "pure vanilla extract"),
      );
      expect(
        Ingredient.fromString("1 1/2 cups rolled oats"),
        Ingredient("1 1/2 cups", "rolled oats"),
      );
      expect(
        Ingredient.fromString("1 1/2 tablespoons honey"),
        Ingredient("1 1/2 tablespoons", "honey"),
      );
      expect(
        Ingredient.fromString("1 1/2 tbsp honey"),
        Ingredient("1 1/2 tbsp", "honey"),
      );
      expect(
        Ingredient.fromString("1 1/2 Tbsp honey"),
        Ingredient("1 1/2 Tbsp", "honey"),
      );
      expect(
        Ingredient.fromString("4 cups diced rhubarb, about 3/8&#8217;s to 1/2&#8243; an inch"),
        Ingredient("4 cups", "diced rhubarb, about 3/8's to 1/2\" an inch"),
      );
    });

    test('empty returns an Ingredient with no amount & no description', () {
      expect(Ingredient.empty.amount, "");
      expect(Ingredient.empty.description, "");
      expect(Ingredient.empty, Ingredient("", ""));
    });

    test('isBlank', () {
      expect(Ingredient.empty.isBlank, true);
      expect(Ingredient("PC ", "load letter").isBlank, false);
      expect(Ingredient(" ", "   ").isBlank, true);
      expect(Ingredient(" \n", "\t ").isBlank, true);
    });
  });
}
