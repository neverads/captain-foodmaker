import 'package:captainfoodmaker/shared_widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

// ignore_for_file: prefer_const_constructors
// ignore_for_file: prefer_const_declarations
// ignore_for_file: prefer_const_literals_to_create_immutables

void main() {
  group('LabelCard', () {
    testWidgets('has correct UI', (WidgetTester tester) async {
      final Color color = Colors.red;
      await tester.pumpWidget(
        MaterialApp(
          home: Scaffold(
            body: LabelCard(
              "PC load letter",
              color: color,
            ),
          ),
        ),
      );
      expect(
        find.byWidgetPredicate((widget) {
          // The color of the card is an alpha_blend, and the result in the default case is Colors.white.  :shrug:
          return widget is Card && widget.color == Colors.white;
        }),
        findsOneWidget,
      );
      expect(find.byType(Padding), findsNWidgets(2)); // margin & padding
      expect(find.byType(Row), findsOneWidget);
      expect(find.byType(Text), findsOneWidget);
      expect(find.text("PC load letter"), findsOneWidget);
    });
  });

  test("ScrollViewPadding is exact EdgeInsets", () {
    expect(ScrollViewEdgeInsets(), EdgeInsets.only(top: 12, bottom: 64));
    expect(ScrollViewEdgeInsets.onlyTop, EdgeInsets.only(top: 12));
    expect(ScrollViewEdgeInsets.onlyBottom, EdgeInsets.only(bottom: 64));
    expect(ScrollViewEdgeInsets.none, EdgeInsets.zero);
  });

  group('PaddedListView', () {
    testWidgets('has correct padding & UI', (WidgetTester tester) async {
      final PaddedListView paddedListView = PaddedListView(
        itemCount: 3,
        itemBuilder: (BuildContext context, int index) {
          return Text("PC load letter");
        },
      );
      expect(paddedListView.padding, ScrollViewEdgeInsets());
      expect(paddedListView.shrinkWrap, false);

      await tester.pumpWidget(
        MaterialApp(
          home: Scaffold(
            body: paddedListView,
          ),
        ),
      );
      expect(find.text("PC load letter"), findsNWidgets(3));
    });
  });

  test('PaddedReorderableListView has correct padding', () {
    final PaddedReorderableListView customReorderableListView = PaddedReorderableListView(
      children: [],
      onReorder: (int oldIndex, int newIndex) {},
    );
    expect(customReorderableListView.padding, ScrollViewEdgeInsets());
  });

  group('CustomPopupMenuItem classes', () {
    group('CustomPopupMenuItem (base-class)', () {
      testWidgets('has correct UI', (WidgetTester tester) async {
        final _text = "PC";
        final _value = "load letter";
        await tester.pumpWidget(
          MaterialApp(
            home: Scaffold(
              body: CustomPopupMenuItem(
                text: _text,
                value: _value,
              ),
            ),
          ),
        );
        expect(find.byType(CustomPopupMenuItem), findsOneWidget);
        expect(
          find.byWidgetPredicate((widget) {
            return widget is PopupMenuItem<String> &&
                widget is CustomPopupMenuItem &&
                widget.value == _value &&
                widget.child is Text &&
                (widget.child as Text).data == _text;
          }),
          findsOneWidget,
        );
      });
    });

    group('PopupMenuItemAddNewRecipe', () {
      testWidgets('has correct UI', (WidgetTester tester) async {
        await tester.pumpWidget(
          MaterialApp(
            home: Scaffold(
              body: PopupMenuItemAddNewRecipe(),
            ),
          ),
        );
        expect(PopupMenuItemAddNewRecipe.valueString, "_add_new_recipe");
        expect(find.byType(PopupMenuItemAddNewRecipe), findsOneWidget);
        expect(
          find.byWidgetPredicate((widget) {
            return widget is CustomPopupMenuItem &&
                widget is PopupMenuItemAddNewRecipe &&
                (widget.child as Text).data == "Add new recipe";
          }),
          findsOneWidget,
        );
      });
    });

    group('PopupMenuItemExport', () {
      testWidgets('has correct UI', (WidgetTester tester) async {
        await tester.pumpWidget(
          MaterialApp(
            home: Scaffold(
              body: PopupMenuItemExport(),
            ),
          ),
        );
        expect(PopupMenuItemExport.valueString, "_export_share_recipes");
        expect(find.byType(PopupMenuItemExport), findsOneWidget);
        expect(
          find.byWidgetPredicate((widget) {
            return widget is CustomPopupMenuItem &&
                widget is PopupMenuItemExport &&
                (widget.child as Text).data == "Export / share";
          }),
          findsOneWidget,
        );
      });
    });

    group('PopupMenuItemEditLabels', () {
      testWidgets('has correct UI', (WidgetTester tester) async {
        await tester.pumpWidget(
          MaterialApp(
            home: Scaffold(
              body: PopupMenuItemEditLabels(),
            ),
          ),
        );
        expect(PopupMenuItemEditLabels.valueString, "_edit_labels");
        expect(find.byType(PopupMenuItemEditLabels), findsOneWidget);
        expect(
          find.byWidgetPredicate((widget) {
            return widget is CustomPopupMenuItem &&
                widget is PopupMenuItemEditLabels &&
                (widget.child as Text).data == "Edit labels";
          }),
          findsOneWidget,
        );
      });
    });

    group('PopupMenuItemDeletedRecipes', () {
      testWidgets('has correct UI', (WidgetTester tester) async {
        await tester.pumpWidget(
          MaterialApp(
            home: Scaffold(
              body: PopupMenuItemDeletedRecipes(),
            ),
          ),
        );
        expect(PopupMenuItemDeletedRecipes.valueString, "_deleted_recipes");
        expect(find.byType(PopupMenuItemDeletedRecipes), findsOneWidget);
        expect(
          find.byWidgetPredicate((widget) {
            return widget is CustomPopupMenuItem &&
                widget is PopupMenuItemDeletedRecipes &&
                (widget.child as Text).data == "Deleted recipes";
          }),
          findsOneWidget,
        );
      });
    });

    group('PopupMenuItemDeleteRecipe', () {
      testWidgets('has correct UI', (WidgetTester tester) async {
        await tester.pumpWidget(
          MaterialApp(
            home: Scaffold(
              body: PopupMenuItemDeleteRecipe(),
            ),
          ),
        );
        expect(PopupMenuItemDeleteRecipe.valueString, "_delete_recipe");
        expect(find.byType(PopupMenuItemDeleteRecipe), findsOneWidget);
        expect(
          find.byWidgetPredicate((widget) {
            return widget is CustomPopupMenuItem &&
                widget is PopupMenuItemDeleteRecipe &&
                (widget.child as Text).data == "Delete";
          }),
          findsOneWidget,
        );
      });
    });

    group('PopupMenuItemRestoreRecipe', () {
      testWidgets('has correct UI', (WidgetTester tester) async {
        await tester.pumpWidget(
          MaterialApp(
            home: Scaffold(
              body: PopupMenuItemRestoreRecipe(),
            ),
          ),
        );
        expect(PopupMenuItemRestoreRecipe.valueString, "_restore_recipe");
        expect(find.byType(PopupMenuItemRestoreRecipe), findsOneWidget);
        expect(
          find.byWidgetPredicate((widget) {
            return widget is CustomPopupMenuItem &&
                widget is PopupMenuItemRestoreRecipe &&
                (widget.child as Text).data == "Restore (un-delete)";
          }),
          findsOneWidget,
        );
      });
    });
  });

  group('ReorderIconButton', () {
    testWidgets('has correct UI & displays snack_bar when tapped', (WidgetTester tester) async {
      await tester.pumpWidget(
        MaterialApp(
          home: Scaffold(
            body: ReorderIconButton(),
          ),
        ),
      );
      expect(find.byType(IconButton), findsOneWidget);
      expect(
        find.byWidgetPredicate((widget) {
          return widget is IconButton &&
              (widget.icon as Icon).icon == Icons.drag_indicator &&
              widget.visualDensity == VisualDensity.compact &&
              widget.color == Colors.grey;
        }),
        findsOneWidget,
      );

      await tester.tap(find.byIcon(Icons.drag_indicator));
      await tester.pumpAndSettle();
      expect(find.byType(SnackBar), findsOneWidget);
      expect(
        find.byWidgetPredicate((widget) {
          return widget is SnackBar &&
              widget.content is Text &&
              (widget.content as Text).data == "Long-press and drag to reorder." &&
              widget.action == null;
        }),
        findsOneWidget,
      );
    });
  });
}
