import 'package:captainfoodmaker/data_models/label_colors.dart';
import 'package:captainfoodmaker/data_models/recipe.dart';
import 'package:captainfoodmaker/home/home_screen.dart';
import 'package:captainfoodmaker/interfaces/clipboard_interface.dart';
import 'package:captainfoodmaker/interfaces/hive_storage_interface.dart';
import 'package:captainfoodmaker/interfaces/web_interface.dart';
import 'package:captainfoodmaker/main.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

// ignore_for_file: prefer_const_constructors
// ignore_for_file: prefer_const_declarations
// ignore_for_file: prefer_const_literals_to_create_immutables

class MockHiveStorageInterface extends Mock implements HiveStorageInterface {}

class MockClipboardInterface extends Mock implements ClipboardInterface {}

class MockWebInterface extends Mock implements WebInterface {}

void main() {
  group('entire app, on init', () {
    late HiveStorageInterface hiveStorageInterface;
    late ClipboardInterface clipboardInterface;
    late WebInterface webInterface;

    setUp(() {
      hiveStorageInterface = MockHiveStorageInterface();
      clipboardInterface = MockClipboardInterface();
      webInterface = MockWebInterface();
    });

    testWidgets('has basic UI component and calls interface methods', (WidgetTester tester) async {
      final List<Recipe> recipes = [
        Recipe("PC", labels: [], ingredients: [], steps: []),
        Recipe("load letter", labels: [], ingredients: [], steps: []),
      ];
      when(() => hiveStorageInterface.getRecipes(excludeDeletedRecipes: true)).thenReturn(recipes);
      when(hiveStorageInterface.getLabelColors).thenReturn([LabelColor("initech", Colors.cyan)]);
      await tester.pumpWidget(
        CaptainFoodmakerApp(
          storageInterface: hiveStorageInterface,
          clipboardInterface: clipboardInterface,
          webInterface: webInterface,
        ),
      );
      verify(() => hiveStorageInterface.getRecipes(excludeDeletedRecipes: true)).called(1);
      verify(hiveStorageInterface.getLabelColors).called(1);
      expect(find.byType(HomeScreen), findsOneWidget);
      expect(
        find.byWidgetPredicate((widget) {
          return widget is HomeScreen && widget.title == "Captain Foodmaker";
        }),
        findsOneWidget,
      );
    });
  });
}
