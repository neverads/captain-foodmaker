import 'package:bloc_test/bloc_test.dart';
import 'package:captainfoodmaker/data_models/label_colors.dart';
import 'package:captainfoodmaker/edit_labels/edit_labels_cubit.dart';
import 'package:captainfoodmaker/edit_labels/edit_labels_screen.dart';
import 'package:captainfoodmaker/shared_widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

// ignore_for_file: prefer_const_constructors
// ignore_for_file: prefer_const_declarations
// ignore_for_file: prefer_const_literals_to_create_immutables

class MockEditLabelsCubit extends MockCubit<EditLabelsState> implements EditLabelsCubit {}

void main() {
  late EditLabelsCubit cubit;
  WidgetController.hitTestWarningShouldBeFatal = true;

  setUp(() {
    cubit = MockEditLabelsCubit();
    when(() => cubit.state).thenReturn(
      EditLabelsState([
        LabelColor("PC", Colors.grey),
        LabelColor("load", Colors.red),
        LabelColor("letter", Colors.lime),
      ]),
    );
  });

  group("EditLabelsScreen on init", () {
    testWidgets("has correct UI", (WidgetTester tester) async {
      await tester.pumpWidget(
        BlocProvider.value(
          value: cubit,
          child: MaterialApp(
            home: EditLabelsView(),
          ),
        ),
      );
      expect(find.byType(AppBar), findsOneWidget);
      expect(
        find.byWidgetPredicate((widget) {
          return widget is AppBar &&
              widget.title is Text &&
              (widget.title as Text).data == "Edit labels" &&
              widget.actions == null;
        }),
        findsOneWidget,
      );
      expect(find.byType(ReorderableListView), findsOneWidget);
      expect(find.byType(ListTile), findsNWidgets(3));
      final List<Color> possibleLabelColors = [
        Colors.blue,
        Colors.indigo,
        Colors.deepPurple,
        Colors.purple,
        Colors.pink,
        Colors.red,
        Colors.deepOrange,
        Colors.orange,
        Colors.amber,
        Colors.yellow,
        Colors.lime,
        Colors.lightGreen,
        Colors.green,
        Colors.teal,
        Colors.cyan,
        Colors.lightBlue,
        Colors.blueGrey,
        Colors.brown,
        Colors.grey,
      ];
      for (final labelColor in cubit.state.labelColors) {
        final String label = labelColor.label;
        final Color selectedColor = labelColor.color;
        expect(
          find.byWidgetPredicate((widget) {
            return widget is Container &&
                widget.key is ValueKey &&
                (widget.key as ValueKey).value == label &&
                widget.decoration is BoxDecoration &&
                (widget.decoration as BoxDecoration).gradient is LinearGradient &&
                ((widget.decoration as BoxDecoration).gradient as LinearGradient).begin == Alignment.centerLeft &&
                ((widget.decoration as BoxDecoration).gradient as LinearGradient).end == Alignment.centerRight &&
                ((widget.decoration as BoxDecoration).gradient as LinearGradient).stops!.length == 2 &&
                ((widget.decoration as BoxDecoration).gradient as LinearGradient).stops![0] == 0.3 &&
                ((widget.decoration as BoxDecoration).gradient as LinearGradient).stops![1] == 0.8 &&
                ((widget.decoration as BoxDecoration).gradient as LinearGradient).colors.length == 2 &&
                ((widget.decoration as BoxDecoration).gradient as LinearGradient).colors[0] ==
                    selectedColor.withOpacity(0.1) &&
                ((widget.decoration as BoxDecoration).gradient as LinearGradient).colors[1] ==
                    selectedColor.withOpacity(0.3) &&
                widget.child is ListTile &&
                (widget.child as ListTile).title is TextField &&
                ((widget.child as ListTile).title as TextField).controller!.text == label &&
                ((widget.child as ListTile).title as TextField).decoration!.border == InputBorder.none &&
                ((widget.child as ListTile).title as TextField).cursorColor == Colors.grey &&
                (widget.child as ListTile).leading is DropdownButton<Color> &&
                ((widget.child as ListTile).leading as DropdownButton).value is Color &&
                (((widget.child as ListTile).leading as DropdownButton).value as Color).value == selectedColor.value &&
                ((widget.child as ListTile).leading as DropdownButton).underline is SizedBox &&
                (((widget.child as ListTile).leading as DropdownButton).underline as SizedBox).width == null &&
                (((widget.child as ListTile).leading as DropdownButton).underline as SizedBox).height == null &&
                (((widget.child as ListTile).leading as DropdownButton).underline as SizedBox).child == null &&
                ((widget.child as ListTile).leading as DropdownButton).items!.length == possibleLabelColors.length &&
                ((widget.child as ListTile).leading as DropdownButton).items!.every((item) {
                  return item is DropdownMenuItem<Color>;
                }) &&
                possibleLabelColors.every((possibleColor) {
                  final int index = possibleLabelColors.indexOf(possibleColor);
                  final DropdownMenuItem item = ((widget.child as ListTile).leading as DropdownButton).items![index];
                  return item.value is Color &&
                      (item.value as Color).value == possibleColor.value &&
                      item.child is SizedBox &&
                      (item.child as SizedBox).width == 32 &&
                      (item.child as SizedBox).height == 20 &&
                      (item.child as SizedBox).child is Card &&
                      ((item.child as SizedBox).child as Card).margin == EdgeInsets.zero &&
                      ((item.child as SizedBox).child as Card).color == possibleColor.withOpacity(0.6);
                }) &&
                (widget.child as ListTile).trailing is ReorderIconButton;
          }),
          findsOneWidget,
        );
      }
    });
  });

  group("reordering via long-press-drag-and-drop", () {
    testWidgets("calls method from cubit", (WidgetTester tester) async {
      await tester.pumpWidget(
        BlocProvider.value(
          value: cubit,
          child: MaterialApp(
            home: EditLabelsView(),
          ),
        ),
      );

      final Offset startDragLocation = tester.getCenter(find.byType(ReorderIconButton).first);
      final Offset endDragLocation = tester.getCenter(find.byType(ReorderIconButton).last);

      final TestGesture gesture = await tester.startGesture(startDragLocation);
      await tester.pump(Duration(milliseconds: 600)); // simulate a long-press, by waiting while down is held
      await gesture.moveTo(endDragLocation);
      await gesture.up();

      verify(() => cubit.reorder(0, 2)).called(1);
    });
  });

  group("changing the label-text", () {
    testWidgets("calls method from cubit", (WidgetTester tester) async {
      when(() => cubit.updateLabelText(LabelColor("load", Colors.red), "TPS")).thenAnswer((_) async {});

      await tester.pumpWidget(
        BlocProvider.value(
          value: cubit,
          child: MaterialApp(
            home: EditLabelsView(),
          ),
        ),
      );

      await tester.enterText(find.byType(TextField).at(1), "TPS");
      verify(() => cubit.updateLabelText(LabelColor("load", Colors.red), "TPS")).called(1);
    });
  });

  // todo -- fix this test (test manually until then)
  // group("selecting a new color for a label", () {
  //   testWidgets("calls method from cubit", (WidgetTester tester) async {
  //     await tester.pumpWidget(
  //       BlocProvider.value(
  //         value: cubit,
  //         child: MaterialApp(
  //           home: EditLabelsView(),
  //         ),
  //       ),
  //     );
  //
  //     await tester.pumpAndSettle();
  //     await tester.tap(find.byType(DropdownButton<Color>).first);
  //     await tester.pumpAndSettle();
  //     expect(find.byType(DropdownMenuItem<Color>), findsNWidgets(68));
  //     expect(find.byType(Card), findsNWidgets(68));
  //     final evaluated = find.byType(DropdownMenuItem<Color>).evaluate();
  //     expect(
  //       find.byWidgetPredicate((widget) {
  //         return widget is Card && widget.color!.value == Colors.amber.withOpacity(0.6).value;
  //       }),
  //       findsNWidgets(4),
  //     );
  //     // await tester.tap(
  //     //   // find.byWidgetPredicate((widget) {
  //     //   //   // if (widget is DropdownMenuItem<Color> &&
  //     //   //   //     widget.value is Color &&
  //     //   //   //     (widget.value as Color).value == Colors.amber.value) {
  //     //   //   //   print("");
  //     //   //   // }
  //     //   //   return widget is Card && widget.color!.value == Colors.amber.withOpacity(0.6).value;
  //     //   // }).first,
  //     //
  //     //   // find.byType(Card).at(3),
  //     //
  //     //   find.byWidgetPredicate((widget) {
  //     //     return widget is Card && widget.color!.value == Colors.amber.withOpacity(0.6).value;
  //     //   }).at(0),
  //     // );
  //     await tester.tapAt(Offset(16, 20));
  //     await tester.pumpAndSettle();
  //     verify(() => cubit.updateLabelColors("letter", Colors.amber)).called(1);
  //   });
  // });
}
