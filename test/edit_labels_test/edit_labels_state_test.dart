import 'package:captainfoodmaker/data_models/label_colors.dart';
import 'package:captainfoodmaker/edit_labels/edit_labels_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

// ignore_for_file: prefer_const_constructors
// ignore_for_file: prefer_const_declarations
// ignore_for_file: prefer_const_literals_to_create_immutables

void main() {
  group('EditLabelsState', () {
    group('empty params/arguments', () {
      final state = EditLabelsState([]);

      test('has attributes', () {
        expect(state.labelColors, []);
        expect(state.props, [[]]);
      });

      test('is equatable', () {
        expect(
          state,
          EditLabelsState([]),
        );
        expect(
          state,
          isNot(
            EditLabelsState([LabelColor("TPS", Colors.blue)]),
          ),
        );
      });
    });

    group('with params/arguments', () {
      final state = EditLabelsState([
        LabelColor("PC", Colors.grey),
        LabelColor("load", Colors.cyan),
        LabelColor("letter", Colors.teal),
      ]);

      test('has attributes', () {
        expect(state.labelColors, [
          LabelColor("PC", Colors.grey),
          LabelColor("load", Colors.cyan),
          LabelColor("letter", Colors.teal),
        ]);
        expect(state.props, [
          [
            LabelColor("PC", Colors.grey),
            LabelColor("load", Colors.cyan),
            LabelColor("letter", Colors.teal),
          ],
        ]);
      });

      test('is equatable', () {
        expect(
          state,
          EditLabelsState([
            LabelColor("PC", Colors.grey),
            LabelColor("load", Colors.cyan),
            LabelColor("letter", Colors.teal),
          ]),
        );
      });
    });

    test('has getter for comprehensive label-colors', () {
      final state = EditLabelsState([
        LabelColor("PC", Colors.grey),
        LabelColor("load", Colors.cyan),
        LabelColor("letter", Colors.pink),
      ]);
      expect(state.labelColors, [
        LabelColor("PC", Colors.grey),
        LabelColor("load", Colors.cyan),
        LabelColor("letter", Colors.pink),
      ]);
    });
  });
}
