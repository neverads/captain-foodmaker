import 'package:bloc_test/bloc_test.dart';
import 'package:captainfoodmaker/data_models/label_colors.dart';
import 'package:captainfoodmaker/edit_labels/edit_labels_cubit.dart';
import 'package:captainfoodmaker/interfaces/hive_storage_interface.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

// ignore_for_file: prefer_const_constructors
// ignore_for_file: prefer_const_declarations
// ignore_for_file: prefer_const_literals_to_create_immutables

class MockHiveStorageInterface extends Mock implements HiveStorageInterface {}

void main() {
  late EditLabelsCubit cubit;
  late HiveStorageInterface hiveStorageInterface;

  setUp(() {
    hiveStorageInterface = MockHiveStorageInterface();
    when(() => hiveStorageInterface.storeLabelColors(any())).thenAnswer((_) async {});
    when(() => hiveStorageInterface.storeRecipes(any())).thenAnswer((_) async {});
    cubit = EditLabelsCubit(
      hiveStorageInterface,
      labelColors: [
        LabelColor("PC", Colors.grey),
        LabelColor("load", Colors.pink),
        LabelColor("letter", Colors.teal),
      ],
    );
  });

  tearDown(() {
    cubit.close();
  });

  group('initial state', () {
    blocTest<EditLabelsCubit, EditLabelsState>(
      'state has proper list of labels with colors',
      build: () => cubit,
      expect: () => [],
      verify: (_) {
        expect(
          cubit.state,
          EditLabelsState([
            LabelColor("PC", Colors.grey),
            LabelColor("load", Colors.pink),
            LabelColor("letter", Colors.teal),
          ]),
        );
      },
    );
  });

  group('reorder triggers new state & stores new labels', () {
    blocTest<EditLabelsCubit, EditLabelsState>(
      'moving 0 -> 1',
      build: () => cubit,
      act: (cubit) => cubit.reorder(0, 1),
      expect: () => [
        EditLabelsState([
          LabelColor("load", Colors.pink),
          LabelColor("PC", Colors.grey),
          LabelColor("letter", Colors.teal),
        ]),
      ],
      verify: (_) {
        verify(() => hiveStorageInterface.storeLabelColors([
              LabelColor("load", Colors.pink),
              LabelColor("PC", Colors.grey),
              LabelColor("letter", Colors.teal),
            ])).called(1);
      },
    );

    blocTest<EditLabelsCubit, EditLabelsState>(
      'moving 0 -> 2',
      build: () => cubit,
      act: (cubit) => cubit.reorder(0, 2),
      expect: () => [
        EditLabelsState([
          LabelColor("load", Colors.pink),
          LabelColor("letter", Colors.teal),
          LabelColor("PC", Colors.grey),
        ]),
      ],
      verify: (_) {
        verify(() => hiveStorageInterface.storeLabelColors([
              LabelColor("load", Colors.pink),
              LabelColor("letter", Colors.teal),
              LabelColor("PC", Colors.grey),
            ])).called(1);
      },
    );

    blocTest<EditLabelsCubit, EditLabelsState>(
      'moving 2 -> 1',
      build: () => cubit,
      act: (cubit) => cubit.reorder(2, 1),
      expect: () => [
        EditLabelsState([
          LabelColor("PC", Colors.grey),
          LabelColor("letter", Colors.teal),
          LabelColor("load", Colors.pink),
        ]),
      ],
      verify: (_) {
        verify(() => hiveStorageInterface.storeLabelColors([
              LabelColor("PC", Colors.grey),
              LabelColor("letter", Colors.teal),
              LabelColor("load", Colors.pink),
            ])).called(1);
      },
    );
  });

  group("updateLabelColors", () {
    blocTest<EditLabelsCubit, EditLabelsState>("emits new state; calls storage method",
        build: () => cubit,
        act: (cubit) => cubit.updateLabelColors("load", Colors.brown),
        expect: () => [
              EditLabelsState([
                LabelColor("PC", Colors.grey),
                LabelColor("load", Colors.brown),
                LabelColor("letter", Colors.teal),
              ]),
            ],
        verify: (_) {
          verify(() => hiveStorageInterface.storeLabelColors([
                LabelColor("PC", Colors.grey),
                LabelColor("load", Colors.brown),
                LabelColor("letter", Colors.teal),
              ])).called(1);
        });
  });

  group("updateLabelText", () {
    blocTest<EditLabelsCubit, EditLabelsState>("emits new state; calls storage method",
        build: () {
          when(hiveStorageInterface.getRecipes).thenReturn([]);
          return cubit;
        },
        act: (cubit) => cubit.updateLabelText(LabelColor("PC", Colors.grey), "TPS"),
        expect: () => [
              EditLabelsState([
                LabelColor("TPS", Colors.grey),
                LabelColor("load", Colors.pink),
                LabelColor("letter", Colors.teal),
              ]),
            ],
        verify: (_) {
          // todo -- make test more precise by checking that correct args passed to `storeLabelColors`
          // not yet possible, because of dart's list-validation
          verify(() => hiveStorageInterface.storeLabelColors(any())).called(1);
          verify(hiveStorageInterface.getRecipes).called(1);
        });
  });
}
