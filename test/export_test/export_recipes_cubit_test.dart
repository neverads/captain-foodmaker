import 'package:bloc_test/bloc_test.dart';
import 'package:captainfoodmaker/export/export_recipes_cubit.dart';
import 'package:captainfoodmaker/interfaces/clipboard_interface.dart';
import 'package:captainfoodmaker/data_models/recipe.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

// ignore_for_file: prefer_const_constructors
// ignore_for_file: prefer_const_declarations
// ignore_for_file: prefer_const_literals_to_create_immutables

class MockClipboardInterface extends Mock implements ClipboardInterface {}

void main() {
  late ExportRecipesCubit cubit;
  late ClipboardInterface clipboardInterface;

  setUp(() {
    clipboardInterface = MockClipboardInterface();
    cubit = ExportRecipesCubit(
      clipboardInterface,
      [
        Recipe("PC", labels: [], ingredients: [], steps: []),
        Recipe("load letter", labels: [], ingredients: [], steps: []),
      ],
    );
  });

  tearDown(() {
    cubit.close();
  });

  group('initial state with recipes', () {
    blocTest<ExportRecipesCubit, ExportRecipesState>(
      'state has those recipes as attributes',
      build: () => cubit,
      expect: () => [],
      verify: (_) {
        expect(
          cubit.state,
          ExportRecipesState(
            allRecipes: [
              Recipe("PC", labels: [], ingredients: [], steps: []),
              Recipe("load letter", labels: [], ingredients: [], steps: []),
            ],
            selectedRecipes: [
              Recipe("PC", labels: [], ingredients: [], steps: []),
              Recipe("load letter", labels: [], ingredients: [], steps: []),
            ],
          ),
        );
      },
    );
  });

  group('selectRecipe', () {
    blocTest<ExportRecipesCubit, ExportRecipesState>(
      'emits state with adjusted recipes',
      build: () {
        when(() => clipboardInterface.copyTextToClipboard(any())).thenAnswer((_) async => Future<void>);
        return cubit;
      },
      act: (cubit) => cubit.selectRecipe(
        Recipe("TPS reports", labels: [], ingredients: [], steps: []),
      ),
      expect: () => [
        ExportRecipesState(
          allRecipes: [
            Recipe("PC", labels: [], ingredients: [], steps: []),
            Recipe("load letter", labels: [], ingredients: [], steps: []),
          ],
          selectedRecipes: [
            Recipe("PC", labels: [], ingredients: [], steps: []),
            Recipe("load letter", labels: [], ingredients: [], steps: []),
            Recipe("TPS reports", labels: [], ingredients: [], steps: []),
          ],
        ),
      ],
    );
  });

  group('deselectRecipe', () {
    blocTest<ExportRecipesCubit, ExportRecipesState>(
      'emits state with adjusted recipes',
      build: () {
        when(() => clipboardInterface.copyTextToClipboard(any())).thenAnswer((_) async => Future<void>);
        return cubit;
      },
      act: (cubit) => cubit.deselectRecipe(
        Recipe("PC", labels: [], ingredients: [], steps: []),
      ),
      expect: () => [
        ExportRecipesState(
          allRecipes: [
            Recipe("PC", labels: [], ingredients: [], steps: []),
            Recipe("load letter", labels: [], ingredients: [], steps: []),
          ],
          selectedRecipes: [
            Recipe("load letter", labels: [], ingredients: [], steps: []),
          ],
        ),
      ],
    );
  });

  group('exportRecipes', () {
    blocTest<ExportRecipesCubit, ExportRecipesState>(
      'calls copyTextToClipboard()',
      build: () {
        when(() => clipboardInterface.copyTextToClipboard(any())).thenAnswer((_) async => Future<void>);
        return cubit;
      },
      act: (cubit) async => await cubit.exportRecipes(),
      expect: () => [],
      verify: (_) {
        verify(() => clipboardInterface.copyTextToClipboard(any())).called(1);
      },
    );
  });
}
