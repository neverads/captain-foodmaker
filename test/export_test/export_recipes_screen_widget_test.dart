import 'package:bloc_test/bloc_test.dart';
import 'package:captainfoodmaker/data_models/label_colors.dart';
import 'package:captainfoodmaker/data_models/recipe.dart';
import 'package:captainfoodmaker/export/export_recipes_cubit.dart';
import 'package:captainfoodmaker/export/export_recipes_screen.dart';
import 'package:captainfoodmaker/shared_widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

// ignore_for_file: prefer_const_constructors
// ignore_for_file: prefer_const_declarations
// ignore_for_file: prefer_const_literals_to_create_immutables

class MockExportRecipesCubit extends MockCubit<ExportRecipesState> implements ExportRecipesCubit {}

void main() {
  late ExportRecipesCubit exportRecipesCubit;

  setUp(() {
    exportRecipesCubit = MockExportRecipesCubit();
    when(() => exportRecipesCubit.state).thenReturn(
      ExportRecipesState(
        allRecipes: [],
        selectedRecipes: [],
      ),
    );
  });

  group("ExportRecipesView on init", () {
    testWidgets("has correct UI", (WidgetTester tester) async {
      await tester.pumpWidget(
        BlocProvider.value(
          value: exportRecipesCubit,
          child: MaterialApp(
            home: ExportRecipesView(
              labelColors: [],
            ),
          ),
        ),
      );
      expect(find.byType(AppBar), findsOneWidget);
      expect(
        find.byWidgetPredicate((widget) {
          return widget is AppBar &&
              widget.title is Text &&
              (widget.title as Text).data == "Export / share" &&
              widget.actions!.length == 1 &&
              widget.actions![0] is IconButton &&
              (widget.actions![0] as IconButton).icon.toString() == Icon(Icons.info_outline).toString();
        }),
        findsOneWidget,
      );
      expect(find.byType(Column), findsOneWidget);
      expect(find.byType(ListViewOfRecipesToExport), findsOneWidget);
      expect(find.byType(ExportButton), findsOneWidget);
    });
  });

  group("info-button", () {
    testWidgets("tapping triggers alert-dialog", (WidgetTester tester) async {
      await tester.pumpWidget(
        BlocProvider.value(
          value: exportRecipesCubit,
          child: MaterialApp(
            home: ExportRecipesView(
              labelColors: [],
            ),
          ),
        ),
      );

      await tester.tap(
        find.byWidgetPredicate((widget) {
          return widget is Icon && widget.icon == Icons.info_outline;
        }),
      );
      await tester.pumpAndSettle();
      expect(find.byType(AlertDialog), findsOneWidget);
      expect(find.text("ok"), findsOneWidget);
      expect(
        find.byWidgetPredicate((widget) {
          return widget is AlertDialog &&
              widget.title is Text &&
              (widget.title as Text).data == "Why export/share my recipes?" &&
              widget.content is Text &&
              (widget.content as Text).data ==
                  "Maybe you just want to send a recipe to a friend.  "
                      "If they also have this app, they can import it to keep a copy.\n"
                      "\n"
                      "Also, we don't collect your data - at all.\n"
                      "\n"
                      "That means your recipes exist only on your device - not on a computer somewhere in the cloud.  "
                      "If you accidentally delete something, or if a toddler destroys your device, you might want a backup.\n"
                      "\n"
                      "When you tap the 'Export' button, the app will copy your recipes so that you can paste them somewhere - "
                      "as an email, for example." &&
              widget.actions!.length == 1 &&
              widget.actions![0] is OutlinedButton &&
              (widget.actions![0] as OutlinedButton).child is Text &&
              ((widget.actions![0] as OutlinedButton).child as Text).data == "ok";
        }),
        findsOneWidget,
      );

      await tester.tap(find.text("ok"));
      await tester.pumpAndSettle();
      expect(find.byType(AlertDialog), findsNothing);
    });
  });

  group("ListViewOfRecipesToExport", () {
    testWidgets("has correct UI", (WidgetTester tester) async {
      final List<Recipe> _allRecipes = [
        Recipe("PC", labels: ["initech"], ingredients: [], steps: []),
        Recipe("load letter", labels: ["initech"], ingredients: [], steps: []),
        Recipe("flair", labels: ["chotchkie's"], ingredients: [], steps: []),
      ];
      when(() => exportRecipesCubit.state).thenReturn(
        ExportRecipesState(
          allRecipes: _allRecipes,
          selectedRecipes: [],
        ),
      );
      await tester.pumpWidget(
        BlocProvider.value(
          value: exportRecipesCubit,
          child: MaterialApp(
            home: Scaffold(
              body: Column(
                children: [
                  ListViewOfRecipesToExport(labelColors: []),
                ],
              ),
            ),
          ),
        ),
      );

      expect(find.byType(Expanded), findsOneWidget);
      expect(find.byType(PaddedListView), findsOneWidget);
      expect(find.byType(RecipeCheckboxListTile), findsNWidgets(_allRecipes.length));
    });
  });

  group("RecipeCheckboxListTile", () {
    testWidgets("when checked (recipe is in selected-list)", (WidgetTester tester) async {
      final Recipe recipe = Recipe("initech", labels: ["PC", "load letter"], steps: [], ingredients: []);
      when(() => exportRecipesCubit.state).thenReturn(
        ExportRecipesState(
          allRecipes: [],
          selectedRecipes: [recipe],
        ),
      );
      final List<LabelColor> labelColors = [
        LabelColor("PC", Colors.amber),
        LabelColor("load letter", Colors.lime),
      ];
      await tester.pumpWidget(
        BlocProvider.value(
          value: exportRecipesCubit,
          child: MaterialApp(
            home: Scaffold(
              body: RecipeCheckboxListTile(recipe, labelColors: labelColors),
            ),
          ),
        ),
      );
      expect(find.byType(CheckboxListTile), findsOneWidget);
      expect(
        find.byWidgetPredicate((widget) {
          return widget is CheckboxListTile &&
              widget.value == true &&
              widget.title is Text &&
              (widget.title as Text).data == recipe.title &&
              widget.subtitle is LabelsRow &&
              (widget.subtitle as LabelsRow).labels == recipe.labels &&
              (widget.subtitle as LabelsRow).labelColors.length == labelColors.length &&
              (widget.subtitle as LabelsRow).labelColors.getColor("PC") == Colors.amber &&
              (widget.subtitle as LabelsRow).labelColors.getColor("load letter") == Colors.lime;
        }),
        findsOneWidget,
      );

      await tester.tap(find.byType(RecipeCheckboxListTile));
      verify(() => exportRecipesCubit.deselectRecipe(recipe)).called(1);
    });

    testWidgets("when unchecked (recipe is _not_ in selected-list)", (WidgetTester tester) async {
      final Recipe recipe = Recipe("initech", labels: ["PC", "load letter"], steps: [], ingredients: []);
      when(() => exportRecipesCubit.state).thenReturn(
        ExportRecipesState(
          allRecipes: [],
          selectedRecipes: [],
        ),
      );
      final List<LabelColor> labelColors = [
        LabelColor("PC", Colors.amber),
        LabelColor("load letter", Colors.lime),
      ];
      await tester.pumpWidget(
        BlocProvider.value(
          value: exportRecipesCubit,
          child: MaterialApp(
            home: Scaffold(
              body: RecipeCheckboxListTile(recipe, labelColors: labelColors),
            ),
          ),
        ),
      );
      expect(find.byType(CheckboxListTile), findsOneWidget);
      expect(
        find.byWidgetPredicate((widget) {
          return widget is CheckboxListTile &&
              widget.value == false &&
              widget.title is Text &&
              (widget.title as Text).data == recipe.title &&
              widget.subtitle is LabelsRow &&
              (widget.subtitle as LabelsRow).labels == recipe.labels &&
              (widget.subtitle as LabelsRow).labelColors.length == labelColors.length &&
              (widget.subtitle as LabelsRow).labelColors.getColor("PC") == Colors.amber &&
              (widget.subtitle as LabelsRow).labelColors.getColor("load letter") == Colors.lime;
        }),
        findsOneWidget,
      );

      await tester.tap(find.byType(RecipeCheckboxListTile));
      verify(() => exportRecipesCubit.selectRecipe(recipe)).called(1);
    });
  });

  group("ExportButton", () {
    testWidgets("has correct UI", (WidgetTester tester) async {
      await tester.pumpWidget(
        BlocProvider.value(
          value: exportRecipesCubit,
          child: MaterialApp(
            home: Scaffold(
              body: ExportButton(),
            ),
          ),
        ),
      );

      expect(find.byType(Row), findsOneWidget);
      expect(
        find.byWidgetPredicate((widget) {
          return widget is Row &&
              widget.mainAxisAlignment == MainAxisAlignment.center &&
              widget.children.length == 1 &&
              widget.children[0] is Padding;
        }),
        findsOneWidget,
      );
      expect(
        find.byWidgetPredicate((widget) {
          return widget is Padding &&
              widget.padding == EdgeInsets.symmetric(vertical: 16) &&
              widget.child is ElevatedButton;
        }),
        findsOneWidget,
      );
      expect(find.byType(ElevatedButton), findsOneWidget);
      expect(
        find.byWidgetPredicate((widget) {
          return widget is ElevatedButton && widget.child is Text && (widget.child as Text).data == "Export";
        }),
        findsOneWidget,
      );
    });

    testWidgets("tapping triggers cubit to export recipes & display message", (WidgetTester tester) async {
      await tester.pumpWidget(
        BlocProvider.value(
          value: exportRecipesCubit,
          child: MaterialApp(
            home: ExportRecipesView(labelColors: []),
          ),
        ),
      );
      when(exportRecipesCubit.exportRecipes).thenAnswer((_) async => {});

      await tester.tap(find.byType(ExportButton));
      verify(exportRecipesCubit.exportRecipes).called(1);
      await tester.pump();
      expect(find.byType(SnackBar), findsOneWidget);
      expect(
        find.text("Recipes copied to clipboard!  You can paste them in a email, text document, etc."),
        findsOneWidget,
      );
    });
  });
}
