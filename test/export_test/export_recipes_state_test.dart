import 'package:captainfoodmaker/export/export_recipes_cubit.dart';
import 'package:captainfoodmaker/data_models/recipe.dart';
import 'package:flutter_test/flutter_test.dart';

// ignore_for_file: prefer_const_constructors
// ignore_for_file: prefer_const_declarations
// ignore_for_file: prefer_const_literals_to_create_immutables

void main() {
  group('empty recipes-attributes', () {
    test('has attributes', () {
      final state = ExportRecipesState(
        allRecipes: [],
        selectedRecipes: [],
      );
      expect(state.allRecipes, []);
      expect(state.selectedRecipes, []);
    });
    test('is equatable', () {
      expect(
        ExportRecipesState(allRecipes: [], selectedRecipes: []),
        ExportRecipesState(allRecipes: [], selectedRecipes: []),
      );
    });
  });

  group('with recipes', () {
    test('has attributes', () {
      final state = ExportRecipesState(
        allRecipes: [
          Recipe("TPS", labels: [], ingredients: [], steps: []),
          Recipe("report", labels: [], ingredients: [], steps: []),
        ],
        selectedRecipes: [
          Recipe("TPS", labels: [], ingredients: [], steps: []),
        ],
      );
      expect(
        state.allRecipes,
        [
          Recipe("TPS", labels: [], ingredients: [], steps: []),
          Recipe("report", labels: [], ingredients: [], steps: []),
        ],
      );
      expect(
        state.selectedRecipes,
        [
          Recipe("TPS", labels: [], ingredients: [], steps: []),
        ],
      );
    });

    test('is equatable', () {
      expect(
        ExportRecipesState(
          allRecipes: [
            Recipe("TPS", labels: [], ingredients: [], steps: []),
            Recipe("report", labels: [], ingredients: [], steps: []),
          ],
          selectedRecipes: [
            Recipe("TPS", labels: [], ingredients: [], steps: []),
          ],
        ),
        ExportRecipesState(
          allRecipes: [
            Recipe("TPS", labels: [], ingredients: [], steps: []),
            Recipe("report", labels: [], ingredients: [], steps: []),
          ],
          selectedRecipes: [
            Recipe("TPS", labels: [], ingredients: [], steps: []),
          ],
        ),
      );
    });
  });
}
