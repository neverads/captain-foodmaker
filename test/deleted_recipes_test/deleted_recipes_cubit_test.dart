import 'package:bloc_test/bloc_test.dart';
import 'package:captainfoodmaker/data_models/recipe.dart';
import 'package:captainfoodmaker/deleted_recipes/deleted_recipes_cubit.dart';
import 'package:captainfoodmaker/interfaces/hive_storage_interface.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

// ignore_for_file: prefer_const_constructors
// ignore_for_file: prefer_const_declarations
// ignore_for_file: prefer_const_literals_to_create_immutables

class MockHiveStorageInterface extends Mock implements HiveStorageInterface {}

class RecipeFake extends Fake implements Recipe {}

void main() {
  late DeletedRecipesCubit cubit;
  late HiveStorageInterface hiveStorageInterface;

  final recipePc = Recipe("PC", labels: [], ingredients: [], steps: []);
  final recipeLoad = Recipe("load", labels: [], ingredients: [], steps: []);
  final recipeLetter = Recipe("letter", labels: [], ingredients: [], steps: []);
  final recipes = [recipePc, recipeLoad, recipeLetter];

  setUpAll(() {
    registerFallbackValue(RecipeFake());
  });

  setUp(() {
    hiveStorageInterface = MockHiveStorageInterface();
    when(hiveStorageInterface.getDeletedRecipes).thenReturn(recipes);
    when(() => hiveStorageInterface.setDeletionStatusForRecipe(any(), newDeletionStatus: true))
        .thenAnswer((_) async {});
    when(() => hiveStorageInterface.setDeletionStatusForRecipe(any(), newDeletionStatus: false))
        .thenAnswer((_) async {});
    when(() => hiveStorageInterface.removeFromRecipes(any())).thenAnswer((_) async {});
    when(() => hiveStorageInterface.addToRecipes(any())).thenAnswer((_) async {});
    cubit = DeletedRecipesCubit(hiveStorageInterface);
  });

  tearDown(() {
    cubit.close();
  });

  group("initial state", () {
    blocTest<DeletedRecipesCubit, DeletedRecipesState>(
      "is DeletedRecipesStateInitial with attributes",
      build: () => cubit,
      expect: () => [],
      verify: (_) {
        verify(hiveStorageInterface.getDeletedRecipes).called(1);
        expect(
          cubit.state,
          DeletedRecipesStateInitial([
            Recipe("PC", labels: [], ingredients: [], steps: []),
            Recipe("load", labels: [], ingredients: [], steps: []),
            Recipe("letter", labels: [], ingredients: [], steps: []),
          ]),
        );
        expect(
          cubit.state.deletedRecipes,
          [
            Recipe("PC", labels: [], ingredients: [], steps: []),
            Recipe("load", labels: [], ingredients: [], steps: []),
            Recipe("letter", labels: [], ingredients: [], steps: []),
          ],
        );
        expect(
          cubit.state.statuses,
          [
            DeletionStatus.softDeleted,
            DeletionStatus.softDeleted,
            DeletionStatus.softDeleted,
          ],
        );
      },
    );
  });

  group("unDeleteRecipe", () {
    blocTest<DeletedRecipesCubit, DeletedRecipesState>(
      "emits new state",
      build: () => cubit,
      act: (cubit) => cubit.unDeleteRecipe(recipeLetter),
      expect: () => [
        DeletedRecipesState(
          recipes,
          statuses: [
            DeletionStatus.softDeleted,
            DeletionStatus.softDeleted,
            DeletionStatus.restored,
          ],
        ),
      ],
      verify: (_) {
        verify(() => hiveStorageInterface.setDeletionStatusForRecipe(recipeLetter, newDeletionStatus: false)).called(1);
      },
    );
  });

  group("reDeleteRecipe", () {
    blocTest<DeletedRecipesCubit, DeletedRecipesState>(
      "emits new state & calls interface",
      build: () => cubit,
      act: (cubit) {
        cubit.unDeleteRecipe(recipeLoad);
        cubit.reDeleteRecipe(recipeLoad);
      },
      expect: () => [
        DeletedRecipesState(
          recipes,
          statuses: [
            DeletionStatus.softDeleted,
            DeletionStatus.restored,
            DeletionStatus.softDeleted,
          ],
        ),
        DeletedRecipesState(
          recipes,
          statuses: [
            DeletionStatus.softDeleted,
            DeletionStatus.softDeleted,
            DeletionStatus.softDeleted,
          ],
        ),
      ],
      verify: (_) {
        verify(() => hiveStorageInterface.setDeletionStatusForRecipe(recipeLoad, newDeletionStatus: false)).called(1);
        verify(() => hiveStorageInterface.setDeletionStatusForRecipe(recipeLoad, newDeletionStatus: true)).called(1);
      },
    );
  });

  group("foreverDeleteRecipe", () {
    blocTest<DeletedRecipesCubit, DeletedRecipesState>(
      "emits new state & calls interface",
      build: () => cubit,
      act: (cubit) async => await cubit.foreverDeleteRecipe(recipePc),
      expect: () => [
        DeletedRecipesState(
          recipes,
          statuses: [
            DeletionStatus.foreverDeleted,
            DeletionStatus.softDeleted,
            DeletionStatus.softDeleted,
          ],
        ),
      ],
      verify: (_) {
        verify(() => hiveStorageInterface.removeFromRecipes(recipePc)).called(1);
      },
    );
  });

  group("unForeverDeleteRecipe", () {
    blocTest<DeletedRecipesCubit, DeletedRecipesState>(
      "emits new state & calls interface",
      build: () => cubit,
      act: (cubit) async {
        await cubit.foreverDeleteRecipe(recipePc);
        await cubit.unForeverDeleteRecipe(recipePc);
      },
      expect: () => [
        DeletedRecipesState(
          recipes,
          statuses: [
            DeletionStatus.foreverDeleted,
            DeletionStatus.softDeleted,
            DeletionStatus.softDeleted,
          ],
        ),
        DeletedRecipesState(
          recipes,
          statuses: [
            DeletionStatus.softDeleted,
            DeletionStatus.softDeleted,
            DeletionStatus.softDeleted,
          ],
        ),
      ],
      verify: (_) {
        verify(() => hiveStorageInterface.removeFromRecipes(recipePc)).called(1);
        verify(() => hiveStorageInterface.addToRecipes(recipePc)).called(1);
      },
    );
  });
}
