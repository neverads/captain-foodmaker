import 'package:bloc_test/bloc_test.dart';
import 'package:captainfoodmaker/data_models/recipe.dart';
import 'package:captainfoodmaker/deleted_recipes/deleted_recipes_cubit.dart';
import 'package:captainfoodmaker/deleted_recipes/deleted_recipes_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

// ignore_for_file: prefer_const_constructors
// ignore_for_file: prefer_const_declarations
// ignore_for_file: prefer_const_literals_to_create_immutables

class MockDeletedRecipesCubit extends MockCubit<DeletedRecipesState> implements DeletedRecipesCubit {}

class RecipeFake extends Fake implements Recipe {}

void main() {
  group("DeletedRecipesScreen", () {
    late DeletedRecipesCubit cubit;
    final recipes = [
      Recipe("PC", labels: [], ingredients: [], steps: []),
      Recipe("load", labels: [], ingredients: [], steps: []),
      Recipe("letter", labels: [], ingredients: [], steps: []),
    ];

    setUpAll(() {
      registerFallbackValue(RecipeFake());
    });

    setUp(() {
      cubit = MockDeletedRecipesCubit();
      when(() => cubit.state).thenReturn(
        DeletedRecipesStateInitial(recipes),
      );
      when(() => cubit.unDeleteRecipe(any())).thenAnswer((_) async => {});
      when(() => cubit.foreverDeleteRecipe(any())).thenAnswer((_) async => {});
      when(() => cubit.unForeverDeleteRecipe(any())).thenAnswer((_) async => {});
    });

    group("DeletedRecipesView", () {
      group("with _some_ deleted recipes", () {
        testWidgets("has appBar & DeletedRecipesListView", (WidgetTester tester) async {
          await tester.pumpWidget(
            MaterialApp(
              home: BlocProvider.value(
                value: cubit,
                child: DeletedRecipesView(),
              ),
            ),
          );
          expect(find.byType(AppBar), findsOneWidget);
          expect(
            find.byWidgetPredicate((widget) {
              return widget is AppBar &&
                  widget.title is Text &&
                  (widget.title as Text).data == "Deleted recipes" &&
                  widget.actions == null;
            }),
            findsOneWidget,
          );
          expect(find.byType(DeletedRecipesListView), findsOneWidget);
        });
      });

      group("with _no_ deleted recipes", () {
        testWidgets("has `DeletedRecipeListTile`s", (WidgetTester tester) async {
          when(() => cubit.state).thenReturn(DeletedRecipesStateInitial([]));
          await tester.pumpWidget(
            MaterialApp(
              home: BlocProvider.value(
                value: cubit,
                child: DeletedRecipesView(),
              ),
            ),
          );
          expect(find.byType(DeletedRecipeListTile), findsNothing);
          expect(
            find.byWidgetPredicate((widget) {
              return widget is Center && widget.child is Text && (widget.child as Text).data == "No deleted recipes";
            }),
            findsOneWidget,
          );
        });
      });
    });

    group("DeletedRecipesListView", () {
      testWidgets("has `DeletedRecipeListTile`s", (WidgetTester tester) async {
        await tester.pumpWidget(
          BlocProvider.value(
            value: cubit,
            child: MaterialApp(
              home: Scaffold(
                body: DeletedRecipesListView(3),
              ),
            ),
          ),
        );
        expect(find.byType(DeletedRecipeListTile), findsNWidgets(3));
      });
    });

    group("DeletedRecipeListTile", () {
      final recipeTps = Recipe("TPS", labels: [], ingredients: [], steps: []);

      group("with DeletionStatus.softDeleted", () {
        testWidgets("has ListTile with title and actions/buttons", (WidgetTester tester) async {
          when(() => cubit.state).thenReturn(
            DeletedRecipesState(
              [recipeTps],
              statuses: [DeletionStatus.softDeleted],
            ),
          );

          await tester.pumpWidget(
            BlocProvider.value(
              value: cubit,
              child: MaterialApp(
                home: Scaffold(
                  body: DeletedRecipeListTile(index: 0),
                ),
              ),
            ),
          );
          expect(find.byType(ListTile), findsOneWidget);
          expect(
            find.byWidgetPredicate((widget) {
              return widget is ListTile &&
                  widget.title is Text &&
                  (widget.title as Text).data == "TPS" &&
                  widget.subtitle is Text &&
                  (widget.subtitle as Text).data == "Removed from the main screen" &&
                  widget.trailing is Row &&
                  (widget.trailing as Row).children.length == 3 &&
                  (widget.trailing as Row).children[0] is OutlinedButton &&
                  ((widget.trailing as Row).children[0] as OutlinedButton).child is Icon &&
                  (((widget.trailing as Row).children[0] as OutlinedButton).child as Icon).icon ==
                      Icons.restore_from_trash_outlined &&
                  (widget.trailing as Row).children[1] is SizedBox &&
                  ((widget.trailing as Row).children[1] as SizedBox).width == 8.0 &&
                  ((widget.trailing as Row).children[1] as SizedBox).height == null &&
                  (widget.trailing as Row).children[2] is OutlinedButton &&
                  ((widget.trailing as Row).children[2] as OutlinedButton).child is Icon &&
                  (((widget.trailing as Row).children[2] as OutlinedButton).child as Icon).icon ==
                      Icons.delete_forever_outlined;
            }),
            findsOneWidget,
          );

          await tester.tap(find.byIcon(Icons.restore_from_trash_outlined));
          verify(() => cubit.unDeleteRecipe(recipeTps)).called(1);

          await tester.tap(find.byIcon(Icons.delete_forever_outlined));
          verify(() => cubit.foreverDeleteRecipe(recipeTps)).called(1);
        });
      });

      group("with DeletionStatus.restored", () {
        testWidgets("has ListTile with title and actions/buttons", (WidgetTester tester) async {
          when(() => cubit.state).thenReturn(
            DeletedRecipesState(
              [recipeTps],
              statuses: [DeletionStatus.restored],
            ),
          );

          await tester.pumpWidget(
            BlocProvider.value(
              value: cubit,
              child: MaterialApp(
                home: Scaffold(
                  body: DeletedRecipeListTile(index: 0),
                ),
              ),
            ),
          );
          expect(find.byType(ListTile), findsOneWidget);
          expect(
            find.byWidgetPredicate((widget) {
              return widget is ListTile &&
                  widget.title is Text &&
                  (widget.title as Text).data == "TPS" &&
                  widget.subtitle is Text &&
                  (widget.subtitle as Text).data == "Restored to the main screen" &&
                  widget.trailing is Row &&
                  (widget.trailing as Row).children.length == 3 &&
                  (widget.trailing as Row).children[0] is ElevatedButton &&
                  ((widget.trailing as Row).children[0] as ElevatedButton).child is Icon &&
                  (((widget.trailing as Row).children[0] as ElevatedButton).child as Icon).icon ==
                      Icons.restore_from_trash_outlined &&
                  (widget.trailing as Row).children[1] is SizedBox &&
                  ((widget.trailing as Row).children[1] as SizedBox).width == 8.0 &&
                  ((widget.trailing as Row).children[1] as SizedBox).height == null &&
                  (widget.trailing as Row).children[2] is OutlinedButton &&
                  ((widget.trailing as Row).children[2] as OutlinedButton).child is Icon &&
                  (((widget.trailing as Row).children[2] as OutlinedButton).child as Icon).icon ==
                      Icons.delete_forever_outlined;
            }),
            findsOneWidget,
          );

          await tester.tap(find.byIcon(Icons.restore_from_trash_outlined));
          verify(() => cubit.reDeleteRecipe(recipeTps)).called(1);

          await tester.tap(find.byIcon(Icons.delete_forever_outlined));
          verify(() => cubit.foreverDeleteRecipe(recipeTps)).called(1);
        });
      });

      group("with DeletionStatus.foreverDeleted", () {
        testWidgets("has ListTile with title and actions/buttons", (WidgetTester tester) async {
          when(() => cubit.state).thenReturn(
            DeletedRecipesState(
              [recipeTps],
              statuses: [DeletionStatus.foreverDeleted],
            ),
          );

          await tester.pumpWidget(
            BlocProvider.value(
              value: cubit,
              child: MaterialApp(
                home: Scaffold(
                  body: DeletedRecipeListTile(index: 0),
                ),
              ),
            ),
          );
          expect(find.byType(ListTile), findsOneWidget);
          expect(
            find.byWidgetPredicate((widget) {
              return widget is ListTile &&
                  widget.title is Text &&
                  (widget.title as Text).data == "TPS" &&
                  widget.subtitle is Text &&
                  (widget.subtitle as Text).data == "Deleted forever from your device" &&
                  widget.trailing is Row &&
                  (widget.trailing as Row).children.length == 3 &&
                  (widget.trailing as Row).children[0] is OutlinedButton &&
                  ((widget.trailing as Row).children[0] as OutlinedButton).child is Icon &&
                  (((widget.trailing as Row).children[0] as OutlinedButton).child as Icon).icon ==
                      Icons.restore_from_trash_outlined &&
                  (widget.trailing as Row).children[1] is SizedBox &&
                  ((widget.trailing as Row).children[1] as SizedBox).width == 8.0 &&
                  ((widget.trailing as Row).children[1] as SizedBox).height == null &&
                  (widget.trailing as Row).children[2] is ElevatedButton &&
                  ((widget.trailing as Row).children[2] as ElevatedButton).child is Icon &&
                  (((widget.trailing as Row).children[2] as ElevatedButton).child as Icon).icon ==
                      Icons.delete_forever_outlined;
            }),
            findsOneWidget,
          );

          await tester.tap(find.byIcon(Icons.restore_from_trash_outlined));
          verify(() => cubit.unDeleteRecipe(recipeTps)).called(1);

          await tester.tap(find.byIcon(Icons.delete_forever_outlined));
          verify(() => cubit.unForeverDeleteRecipe(recipeTps)).called(1);
        });
      });
    });
  });
}
