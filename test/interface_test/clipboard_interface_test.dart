import 'package:captainfoodmaker/interfaces/clipboard_interface.dart';
import 'package:test/test.dart';

void main() {
  test('copyTextToClipboard() method exists, takes arg, and is of type', () async {
    const clipboardInterface = ClipboardInterface();
    expect(clipboardInterface.copyTextToClipboard.runtimeType.toString(), "(String) => Future<void>");
  });

  test('getCopiedText() method exists & is of type', () async {
    const clipboardInterface = ClipboardInterface();
    expect(clipboardInterface.getCopiedText.runtimeType.toString(), "() => Future<String?>");
  });
}
