import 'package:captainfoodmaker/interfaces/hive_storage_interface.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:mocktail/mocktail.dart';

// ignore_for_file: prefer_const_constructors
// ignore_for_file: prefer_const_declarations
// ignore_for_file: prefer_const_literals_to_create_immutables

class MockHiveApi extends Mock implements HiveInterface {} // Hive's internal API-interface (not CF's `interface`)

void main() {
  group('HiveInterface', () {
    late HiveInterface _hiveApi;
    late HiveStorageInterface hiveInterface;

    setUp(() {
      _hiveApi = MockHiveApi();
      hiveInterface = HiveStorageInterface(hiveApi: _hiveApi);
    });

    group('init', () {
      test('constructor instantiates with mock Hive-API for testing', () {
        expect(HiveStorageInterface(hiveApi: _hiveApi), isNotNull);
      });

      test('constructor instantiates actual Hive when not injected', () {
        expect(HiveStorageInterface(), isNotNull);
      });
    });

    group('openStorageBox', () {
      test('calls openBox', () async {
        try {
          await hiveInterface.openStorageBox();
        } catch (_) {}
        verify(() => _hiveApi.openBox("_generalStorageBoxName")).called(1);
      });
    });

    // there could be more tests here, but manual testing is good enough
  });
}
