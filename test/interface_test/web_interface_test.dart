import 'package:captainfoodmaker/data_models/recipe.dart';
import 'package:captainfoodmaker/interfaces/web_interface.dart';
import 'package:flutter_test/flutter_test.dart';

part "response_body_test_data.dart";

part "test_recipes.dart";

// ignore_for_file: prefer_const_constructors
// ignore_for_file: prefer_const_declarations
// ignore_for_file: prefer_const_literals_to_create_immutables

void main() {
  group('WebInterface', () {
    late WebInterface webInterface;

    setUp(() {
      webInterface = WebInterface();
    });

    test('`uriFromText` return expected values', () {
      expect(webInterface.uriFromText("https://www.initech.net"), Uri.parse("https://www.initech.net"));
      expect(webInterface.uriFromText("https://initech.net"), Uri.parse("https://initech.net"));
      expect(webInterface.uriFromText("http://initech.net"), Uri.parse("http://initech.net"));
      expect(webInterface.uriFromText("www.initech"), null);
      expect(webInterface.uriFromText("initech.net"), Uri.parse("https://initech.net"));
      expect(webInterface.uriFromText("www.initech.net"), Uri.parse("https://www.initech.net"));
      expect(webInterface.uriFromText("tps"), null);
      expect(webInterface.uriFromText("http:"), null);
      expect(webInterface.uriFromText("https:"), null);
      expect(webInterface.uriFromText("http://"), null);
      expect(webInterface.uriFromText("https://"), null);
      expect(webInterface.uriFromText("http://tps"), null);
      expect(webInterface.uriFromText("https://tps"), null);
      expect(webInterface.uriFromText("http://tps/"), null);
      expect(webInterface.uriFromText("https://tps/"), null);
    });

    group('httpGet', () {
      test('_successful_ response', () async {
        final response = await webInterface.httpGet(Uri.parse("https://example.com"));
        expect(response.statusCode, 200);
        expect(response.reasonPhrase, "OK");
        expect(response.body, exampleDotComResponseBody);
      });

      test('_unsuccessful_ response', () async {
        final response = await webInterface.httpGet(Uri.parse("https://example.com/pc_load_letter"));
        expect(response.statusCode, 404);
        expect(response.reasonPhrase, "Not Found");
      });
    });

    group('parseRecipeFromHttpResponseBody', () {
      test('unable to parse returns `null`', () async {
        expect(await webInterface.parseRecipeFromHttpResponseBody("PC load letter"), null);
      });

      test('sour cream coffee cake', () async {
        final actual = await webInterface.parseRecipeFromHttpResponseBody(sourCreamCoffeeCakeResponseBody);
        final expected = sourCreamCoffeeCake;
        expect(actual!.ingredients.length, expected.ingredients.length);
        for (var i in Iterable<int>.generate(actual.ingredients.length)) {
          final actualIngredient = actual.ingredients[i];
          final expectedIngredient = expected.ingredients[i];
          expect(actualIngredient, expectedIngredient);
        }
        expect(actual.steps.length, expected.steps.length);
        for (var i in Iterable<int>.generate(actual.steps.length)) {
          final actualStep = actual.steps[i];
          final expectedStep = expected.steps[i];
          expect(actualStep, expectedStep);
        }
        expect(actual, expected);
      });

      test('beer damper', () async {
        final actual = await webInterface.parseRecipeFromHttpResponseBody(beerDamperResponseBody);
        expect(actual, beerDamper);
      });

      test('rhubarb coffee cake', () async {
        final actual = await webInterface.parseRecipeFromHttpResponseBody(rhubarbCoffeeCakeResponseBody);
        expect(actual, rhubarbCoffeeCake);
      });

      test('plum almond tart', () async {
        final actual = await webInterface.parseRecipeFromHttpResponseBody(plumAlmondTartResponseBody);
        final expected = plumAlmondTart;
        expect(actual!.ingredients.length, expected.ingredients.length);
        for (var i in Iterable<int>.generate(actual.ingredients.length)) {
          final actualIngredient = actual.ingredients[i];
          final expectedIngredient = expected.ingredients[i];
          expect(actualIngredient, expectedIngredient);
        }
        expect(actual.steps.length, expected.steps.length);
        for (var i in Iterable<int>.generate(actual.steps.length)) {
          final actualStep = actual.steps[i];
          final expectedStep = expected.steps[i];
          expect(actualStep, expectedStep);
        }
        expect(actual, expected);
      });

      test('foolproof shakshuka', () async {
        final actual = await webInterface.parseRecipeFromHttpResponseBody(foolproofShakshukaResponseBody);
        final expected = foolproofShakshuka;
        expect(actual!.ingredients.length, expected.ingredients.length);
        for (var i in Iterable<int>.generate(actual.ingredients.length)) {
          final actualIngredient = actual.ingredients[i];
          final expectedIngredient = expected.ingredients[i];
          expect(actualIngredient, expectedIngredient);
        }
        expect(actual.steps.length, expected.steps.length);
        for (var i in Iterable<int>.generate(actual.steps.length)) {
          final actualStep = actual.steps[i];
          final expectedStep = expected.steps[i];
          expect(actualStep, expectedStep);
        }
        expect(actual, expected);
      });
    });
  });
}
