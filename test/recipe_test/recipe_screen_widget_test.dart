import 'package:bloc_test/bloc_test.dart';
import 'package:captainfoodmaker/data_models/label_colors.dart';
import 'package:captainfoodmaker/data_models/recipe.dart';
import 'package:captainfoodmaker/recipe/recipe_cubit.dart';
import 'package:captainfoodmaker/recipe/recipe_screen.dart';
import 'package:captainfoodmaker/shared_widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

// ignore_for_file: prefer_const_constructors
// ignore_for_file: prefer_const_declarations
// ignore_for_file: prefer_const_literals_to_create_immutables

class MockRecipeCubit extends MockCubit<RecipeState> implements RecipeCubit {}

void main() {
  late RecipeCubit cubit;

  final recipe = Recipe(
    "TPS",
    labels: ["PC", "load", "letter"],
    ingredients: [Ingredient("", "things"), Ingredient("1", "stuff")],
    steps: ["step_0", "step_1"],
    url: "initech.net",
  );
  final editedRecipe = Recipe(
    "edited_TPS",
    labels: ["PC", "load letter"],
    ingredients: [Ingredient("4", "things"), Ingredient("2", "stuff")],
    steps: ["step_00", "step_01"],
    url: "www.initech.net",
  );
  final labelColors = [LabelColor("load", Colors.lime)];
  final stateViewMode = RecipeState(recipe, labelColors: labelColors);
  final stateEditMode = RecipeStateEditMode(recipe: recipe, editedRecipe: editedRecipe, labelColors: labelColors);
  final statePromptMode = RecipeStatePromptForBlankRecipeOrPasteFromClipboard(labelColors: []);
  final stateProcessingClipboardMode = RecipeStateProcessingClipboardData(labelColors: []);

  setUp(() {
    cubit = MockRecipeCubit();
    when(cubit.saveEdits).thenAnswer((_) async => {});
  });

  test("RecipeScreen.withBlankRecipe (factory constructor)", () {
    expect(
      RecipeScreen.withBlankRecipe(labelColors: []).recipe,
      RecipeScreen(Recipe.blank, labelColors: []).recipe,
    );
    expect(
      RecipeScreen.withBlankRecipe(labelColors: []).recipe.toString(),
      RecipeScreen(Recipe.blank, labelColors: []).recipe.toString(),
    );
  });

  group('RecipeView', () {
    test("tabsTexts const has correct value", () {
      expect(RecipeView.tabsTexts, ["Ingredients", "Steps", "Labels", "Website"]);
    });

    group('prompt_mode', () {
      testWidgets('has expected UI', (WidgetTester tester) async {
        when(() => cubit.state).thenReturn(statePromptMode);

        await tester.pumpWidget(
          BlocProvider.value(
            value: cubit,
            child: MaterialApp(
              home: RecipeView(),
            ),
          ),
        );

        expect(find.byType(PromptScaffold), findsOneWidget);
        expect(find.byType(DefaultTabController), findsNothing);
        expect(find.byType(RecipeViewAppBar), findsNothing);
        expect(find.byType(RecipeViewerBody), findsNothing);
      });
    });

    group('view_mode', () {
      testWidgets('_without_ in-progress edit -- has expected UI', (WidgetTester tester) async {
        when(() => cubit.state).thenReturn(stateViewMode);

        await tester.pumpWidget(
          BlocProvider.value(
            value: cubit,
            child: MaterialApp(
              home: RecipeView(),
            ),
          ),
        );

        expect(
          find.byWidgetPredicate((widget) {
            return widget is DefaultTabController && widget.length == RecipeView.tabsTexts.length;
          }),
          findsOneWidget,
        );
        expect(find.byType(RecipeViewAppBar), findsOneWidget);
        expect(find.byType(RecipeViewerBody), findsOneWidget);
      });

      testWidgets('_with_ in-progress edit -- has expected UI', (WidgetTester tester) async {
        whenListen(
          cubit,
          Stream<RecipeState>.value(
            RecipeStateEditModeRestored(recipe: recipe, editedRecipe: editedRecipe, labelColors: labelColors),
          ),
          initialState: stateViewMode,
        );

        await tester.pumpWidget(
          BlocProvider.value(
            value: cubit,
            child: MaterialApp(
              home: RecipeView(),
            ),
          ),
        );

        expect(
          find.byWidgetPredicate((widget) {
            return widget is DefaultTabController && widget.length == RecipeView.tabsTexts.length;
          }),
          findsOneWidget,
        );
        expect(find.byType(RecipeViewAppBar), findsOneWidget);
        expect(find.byType(RecipeViewerBody), findsOneWidget);

        await tester.pump(); // trigger the listen-stream
        expect(
          find.byWidgetPredicate((widget) {
            return widget is SnackBar &&
                (widget.content as Text).data == "Restored previous unsaved edits." &&
                widget.action == null;
          }),
          findsOneWidget,
        );
      });
    });

    group("PromptScaffold", () {
      group("initial state & listening for snack_bars", () {
        group("listening for RecipeStateNoClipboardDataToPaste", () {
          testWidgets('has expected UI', (WidgetTester tester) async {
            whenListen(
              cubit,
              Stream<RecipeState>.value(
                RecipeStateNoClipboardDataToPaste(labelColors: labelColors),
              ),
              initialState: statePromptMode,
            );

            await tester.pumpWidget(
              BlocProvider.value(
                value: cubit,
                child: MaterialApp(
                  home: PromptScaffold(),
                ),
              ),
            );

            expect(find.byType(Scaffold), findsOneWidget);
            expect(
              find.byWidgetPredicate((widget) {
                return widget is AppBar &&
                    widget.title is Text &&
                    (widget.title as Text).data == "Add new recipe" &&
                    widget.actions == null;
              }),
              findsOneWidget,
            );
            expect(find.byType(PromptBody), findsOneWidget);

            await tester.pump(); // trigger the listen-stream
            expect(
              find.byWidgetPredicate((widget) {
                return widget is SnackBar &&
                    (widget.content as Text).data ==
                        "Unable to get anything from the clipboard.\n\nMaybe try copying again?" &&
                    widget.action == null;
              }),
              findsOneWidget,
            );
          });
        });

        group("listening for RecipeStateUnableToParseRecipeFromClipboardText", () {
          testWidgets('has expected UI', (WidgetTester tester) async {
            whenListen(
              cubit,
              Stream<RecipeState>.value(
                RecipeStateUnableToParseRecipeFromClipboardText(labelColors: labelColors),
              ),
              initialState: statePromptMode,
            );

            await tester.pumpWidget(
              BlocProvider.value(
                value: cubit,
                child: MaterialApp(
                  home: PromptScaffold(),
                ),
              ),
            );

            expect(find.byType(Scaffold), findsOneWidget);
            expect(
              find.byWidgetPredicate((widget) {
                return widget is AppBar &&
                    widget.title is Text &&
                    (widget.title as Text).data == "Add new recipe" &&
                    widget.actions == null;
              }),
              findsOneWidget,
            );
            expect(find.byType(PromptBody), findsOneWidget);

            await tester.pump(); // trigger the listen-stream
            expect(
              find.byWidgetPredicate((widget) {
                return widget is SnackBar &&
                    (widget.content as Text).data ==
                        "Unable to parse recipe. 😞\n\n"
                            "This feature is under development.  "
                            "Please email never.ads.info@gmail.com with the text you have copied, and we'll make it work ASAP." &&
                    widget.action == null;
              }),
              findsOneWidget,
            );
          });
        });

        group("listening for RecipeStateFailedHttpGetRequest", () {
          testWidgets('has expected UI', (WidgetTester tester) async {
            final _failure = "PC load letter";

            whenListen(
              cubit,
              Stream<RecipeState>.value(
                RecipeStateFailedHttpGetRequest(labelColors: labelColors, failure: _failure),
              ),
              initialState: statePromptMode,
            );

            await tester.pumpWidget(
              BlocProvider.value(
                value: cubit,
                child: MaterialApp(
                  home: PromptScaffold(),
                ),
              ),
            );

            expect(find.byType(Scaffold), findsOneWidget);
            expect(
              find.byWidgetPredicate((widget) {
                return widget is AppBar &&
                    widget.title is Text &&
                    (widget.title as Text).data == "Add new recipe" &&
                    widget.actions == null;
              }),
              findsOneWidget,
            );
            expect(find.byType(PromptBody), findsOneWidget);

            await tester.pump(); // trigger the listen-stream
            expect(
              find.byWidgetPredicate((widget) {
                return widget is SnackBar &&
                    (widget.content as Text).data ==
                        "Unable to fetch the data from that website.\n\n"
                            "Maybe check your connection?\n\n"
                            "Further error-details:\n"
                            "'$_failure'" &&
                    widget.action == null;
              }),
              findsOneWidget,
            );
          });
        });

        group("listening for RecipeStateUnableToParseRecipeFromHttpResponseBodyError", () {
          testWidgets('has expected UI', (WidgetTester tester) async {
            whenListen(
              cubit,
              Stream<RecipeState>.value(
                RecipeStateUnableToParseRecipeFromHttpResponseBodyError(labelColors: labelColors),
              ),
              initialState: statePromptMode,
            );

            await tester.pumpWidget(
              BlocProvider.value(
                value: cubit,
                child: MaterialApp(
                  home: PromptScaffold(),
                ),
              ),
            );

            expect(find.byType(Scaffold), findsOneWidget);
            expect(
              find.byWidgetPredicate((widget) {
                return widget is AppBar &&
                    widget.title is Text &&
                    (widget.title as Text).data == "Add new recipe" &&
                    widget.actions == null;
              }),
              findsOneWidget,
            );
            expect(find.byType(PromptBody), findsOneWidget);

            await tester.pump(); // trigger the listen-stream
            expect(
              find.byWidgetPredicate((widget) {
                return widget is SnackBar &&
                    (widget.content as Text).data ==
                        "Unable to parse recipe from that website.\n\n"
                            "This feature is under development.  "
                            "Please email never.ads.info@gmail.com with the website's url, and we'll make it work ASAP." &&
                    widget.action == null;
              }),
              findsOneWidget,
            );
          });
        });
      });

      group("PromptBody", () {
        testWidgets('prompt_mode -- has expected UI', (WidgetTester tester) async {
          when(() => cubit.state).thenReturn(statePromptMode);
          await tester.pumpWidget(
            BlocProvider.value(
              value: cubit,
              child: MaterialApp(
                home: PromptBody(),
              ),
            ),
          );

          expect(find.byType(Column), findsOneWidget);
          expect(
            find.byWidgetPredicate((widget) {
              return widget is Column &&
                  widget.children.length == 3 &&
                  widget.children[0] is StartFromScratchButton &&
                  (widget.children[1] as SizedBox).height == 8 &&
                  widget.children[2] is PasteFromClipboardButton;
            }),
            findsOneWidget,
          );
          expect(find.byType(CircularProgressIndicator), findsNothing);
        });

        testWidgets('processing_clipboard_mode -- has expected UI', (WidgetTester tester) async {
          when(() => cubit.state).thenReturn(stateProcessingClipboardMode);
          await tester.pumpWidget(
            BlocProvider.value(
              value: cubit,
              child: MaterialApp(
                home: PromptBody(),
              ),
            ),
          );

          expect(find.byType(CircularProgressIndicator), findsOneWidget);
          expect(find.byType(StartFromScratchButton), findsNothing);
          expect(find.byType(PasteFromClipboardButton), findsNothing);
        });
      });

      group("StartFromScratchButton", () {
        testWidgets('has expected UI', (WidgetTester tester) async {
          await tester.pumpWidget(
            BlocProvider.value(
              value: cubit,
              child: MaterialApp(
                home: Scaffold(
                  body: StartFromScratchButton(),
                ),
              ),
            ),
          );

          expect(
            find.byWidgetPredicate((widget) => widget is TextButton),
            findsOneWidget,
          );
          expect(find.byIcon(Icons.note_add_outlined), findsOneWidget);
          expect(find.text("Start from scratch"), findsOneWidget);

          await tester.tap(find.byType(StartFromScratchButton));
          verify(cubit.startFromScratch).called(1);
        });
      });

      group("PasteFromClipboardButton", () {
        testWidgets('has expected UI', (WidgetTester tester) async {
          when(cubit.pasteFromClipboard).thenAnswer((_) async {});

          await tester.pumpWidget(
            BlocProvider.value(
              value: cubit,
              child: MaterialApp(
                home: PasteFromClipboardButton(),
              ),
            ),
          );

          expect(
            find.byWidgetPredicate((widget) => widget is TextButton),
            findsOneWidget,
          );
          expect(find.byIcon(Icons.paste), findsOneWidget);
          expect(find.text("Paste from clipboard"), findsOneWidget);

          await tester.tap(find.byType(PasteFromClipboardButton));
          verify(cubit.pasteFromClipboard).called(1);
        });
      });
    });

    group("RecipeViewAppBar", () {
      testWidgets('has expected UI', (WidgetTester tester) async {
        when(() => cubit.state).thenReturn(stateViewMode);
        final _indicatorColor = Colors.indigo;

        await tester.pumpWidget(
          BlocProvider.value(
            value: cubit,
            child: DefaultTabController(
              length: RecipeView.tabsTexts.length,
              child: MaterialApp(
                home: RecipeViewAppBar(indicatorColor: _indicatorColor),
              ),
            ),
          ),
        );

        expect(
          find.byWidgetPredicate((widget) {
            return widget is AppBar &&
                widget.title is RecipeTitle &&
                widget.actions!.length == 2 &&
                widget.actions![0] is ToggleEditModeIconButton &&
                widget.actions![1] is RecipeScreenPopupButton &&
                widget.bottom is TabBar &&
                (widget.bottom as TabBar).isScrollable == true &&
                (widget.bottom as TabBar).indicatorColor == _indicatorColor &&
                (widget.bottom as TabBar).tabs.length == RecipeView.tabsTexts.length &&
                (widget.bottom as TabBar).tabs.every((_tab) => _tab is Tab) &&
                ((widget.bottom as TabBar).tabs[0] as Tab).text == RecipeView.tabsTexts[0] &&
                ((widget.bottom as TabBar).tabs[1] as Tab).text == RecipeView.tabsTexts[1] &&
                ((widget.bottom as TabBar).tabs[2] as Tab).text == RecipeView.tabsTexts[2] &&
                ((widget.bottom as TabBar).tabs[3] as Tab).text == RecipeView.tabsTexts[3];
          }),
          findsOneWidget,
        );
      });

      group("RecipeTitle", () {
        testWidgets('view_mode -- has expected UI', (WidgetTester tester) async {
          expect(RecipeTitle.hintText, "recipe title");

          when(() => cubit.state).thenReturn(stateViewMode);
          await tester.pumpWidget(
            BlocProvider.value(
              value: cubit,
              child: MaterialApp(
                home: Scaffold(
                  body: RecipeTitle(),
                ),
              ),
            ),
          );

          expect(find.byType(TextField), findsOneWidget);
          expect(
            find.byWidgetPredicate((widget) {
              return widget is TextField &&
                  widget.controller!.text == cubit.state.recipe.title &&
                  widget.decoration is RecipeTextDecoration &&
                  widget.decoration!.hintText == RecipeTitle.hintText &&
                  widget.style!.color == Colors.white &&
                  widget.style!.fontSize == 16.0 &&
                  widget.style!.fontWeight == FontWeight.w500 &&
                  widget.style!.overflow == TextOverflow.ellipsis &&
                  widget.maxLines == 2 &&
                  widget.minLines == 1 &&
                  widget.enabled == false &&
                  widget.cursorColor!.value == Colors.grey.value;
            }),
            findsOneWidget,
          );
        });

        testWidgets('edit_mode -- has expected UI', (WidgetTester tester) async {
          when(() => cubit.state).thenReturn(stateEditMode);
          await tester.pumpWidget(
            BlocProvider.value(
              value: cubit,
              child: MaterialApp(
                home: Scaffold(
                  body: RecipeTitle(),
                ),
              ),
            ),
          );

          expect(find.text(editedRecipe.title), findsOneWidget);
          expect(
            find.byWidgetPredicate((widget) {
              return widget is TextField && widget.enabled == true;
            }),
            findsOneWidget,
          );

          final newTitle = "15 pieces of flair";
          await tester.enterText(find.byType(TextField), newTitle);
          verify(() => cubit.updateTitle(newTitle)).called(1);
        });
      });

      group("ToggleEditModeIconButton", () {
        group("view_mode", () {
          testWidgets('recipe _is not_ deleted -- has expected UI', (WidgetTester tester) async {
            when(() => cubit.state).thenReturn(stateViewMode);

            await tester.pumpWidget(
              BlocProvider.value(
                value: cubit,
                child: MaterialApp(
                  home: Scaffold(
                    body: ToggleEditModeIconButton(),
                  ),
                ),
              ),
            );

            expect(
              find.byWidgetPredicate((widget) {
                return widget is IconButton && (widget.icon as Icon).icon == Icons.edit;
              }),
              findsOneWidget,
            );
            expect(
              find.byWidgetPredicate((widget) {
                return widget is SizedBox && widget.width == null && widget.height == null;
              }),
              findsNothing,
            );

            await tester.tap(find.byType(IconButton));
            verify(cubit.enableEditMode).called(1);
          });
          testWidgets('recipe _is_ deleted -- has expected UI', (WidgetTester tester) async {
            when(() => cubit.state).thenReturn(
              RecipeState(recipe, labelColors: labelColors, isRecipeDeleted: true),
            );

            await tester.pumpWidget(
              BlocProvider.value(
                value: cubit,
                child: MaterialApp(
                  home: Scaffold(
                    body: ToggleEditModeIconButton(),
                  ),
                ),
              ),
            );

            expect(
              find.byWidgetPredicate((widget) {
                return widget is SizedBox && widget.width == null && widget.height == null;
              }),
              findsOneWidget,
            );
            expect(find.byType(IconButton), findsNothing);
          });
        });

        testWidgets('edit_mode -- has expected UI', (WidgetTester tester) async {
          when(() => cubit.state).thenReturn(stateEditMode);

          await tester.pumpWidget(
            BlocProvider.value(
              value: cubit,
              child: MaterialApp(
                home: Scaffold(
                  body: ToggleEditModeIconButton(),
                ),
              ),
            ),
          );

          expect(
            find.byWidgetPredicate((widget) {
              return widget is SizedBox && widget.width == null && widget.height == null;
            }),
            findsOneWidget,
          );
          expect(find.byType(IconButton), findsNothing);
        });
      });

      group("RecipeScreenPopupButton", () {
        group("view_mode", () {
          testWidgets('isRecipeDeleted as _false_ -- has expected UI', (WidgetTester tester) async {
            when(() => cubit.state).thenReturn(
              RecipeState(recipe, labelColors: labelColors, isRecipeDeleted: false),
            );

            await tester.pumpWidget(
              BlocProvider.value(
                value: cubit,
                child: MaterialApp(
                  home: Scaffold(
                    body: RecipeScreenPopupButton(),
                  ),
                ),
              ),
            );

            expect(find.byType(PopupMenuButton<String>), findsOneWidget);
            await tester.tap(find.byType(PopupMenuButton<String>));
            await tester.pumpAndSettle();
            expect(find.byType(PopupMenuItemExport), findsOneWidget);
            expect(find.byType(PopupMenuItemDeleteRecipe), findsOneWidget);
            expect(find.byType(PopupMenuItemRestoreRecipe), findsNothing);
          });

          testWidgets('isRecipeDeleted as _true_ -- has expected UI', (WidgetTester tester) async {
            when(() => cubit.state).thenReturn(
              RecipeState(recipe, labelColors: labelColors, isRecipeDeleted: true),
            );

            await tester.pumpWidget(
              BlocProvider.value(
                value: cubit,
                child: MaterialApp(
                  home: Scaffold(
                    body: RecipeScreenPopupButton(),
                  ),
                ),
              ),
            );

            expect(find.byType(PopupMenuButton<String>), findsOneWidget);
            await tester.tap(find.byType(PopupMenuButton<String>));
            await tester.pumpAndSettle();
            expect(find.byType(PopupMenuItemRestoreRecipe), findsOneWidget);
            expect(find.byType(PopupMenuItemExport), findsNothing);
            expect(find.byType(PopupMenuItemDeleteRecipe), findsNothing);
          });
        });

        testWidgets('edit_mode -- has expected UI', (WidgetTester tester) async {
          when(() => cubit.state).thenReturn(stateEditMode);

          await tester.pumpWidget(
            BlocProvider.value(
              value: cubit,
              child: MaterialApp(
                home: Scaffold(
                  body: RecipeScreenPopupButton(),
                ),
              ),
            ),
          );

          expect(find.byType(PopupMenuButton<String>), findsNothing);
          expect(
            find.byWidgetPredicate((widget) {
              return widget is SizedBox && widget.width == null && widget.height == null;
            }),
            findsOneWidget,
          );
        });
      });
    });

    group("RecipeViewerBody", () {
      testWidgets('has expected UI', (WidgetTester tester) async {
        when(() => cubit.state).thenReturn(stateViewMode);

        await tester.pumpWidget(
          BlocProvider.value(
            value: cubit,
            child: DefaultTabController(
              length: RecipeView.tabsTexts.length,
              child: MaterialApp(
                home: Scaffold(
                  body: RecipeViewerBody(),
                ),
              ),
            ),
          ),
        );

        expect(
          find.byWidgetPredicate((widget) {
            return widget is Column &&
                widget.children.length == 2 &&
                widget.children[0] is Expanded &&
                (widget.children[0] as Expanded).child is TabBarView &&
                widget.children[1] is DoneEditingRecipeWidget;
          }),
          findsOneWidget,
        );
        expect(
          find.byWidgetPredicate((widget) {
            return widget is TabBarView &&
                widget.children.length == RecipeView.tabsTexts.length &&
                widget.children[0] is IngredientsListView &&
                widget.children[1] is StepsListView &&
                widget.children[2] is LabelsListView &&
                widget.children[3] is WebsiteWidget;
          }),
          findsOneWidget,
        );
      });
    });

    group('IngredientsListView', () {
      testWidgets('view_mode -- has expected UI', (WidgetTester tester) async {
        when(() => cubit.state).thenReturn(stateViewMode);

        await tester.pumpWidget(
          BlocProvider.value(
            value: cubit,
            child: MaterialApp(
              home: Scaffold(
                body: IngredientsListView(),
              ),
            ),
          ),
        );

        expect(
          find.byWidgetPredicate((widget) {
            return widget is PaddedReorderableListView &&
                widget.padding == ScrollViewEdgeInsets() &&
                widget.buildDefaultDragHandles == false;
          }),
          findsOneWidget,
        );
        expect(find.byType(IngredientWidget), findsNWidgets(recipe.ingredients.length));
        expect(find.byType(AddIngredientButton), findsNothing);
      });

      testWidgets('edit_mode -- has correct UI', (WidgetTester tester) async {
        when(() => cubit.state).thenReturn(stateEditMode);

        await tester.pumpWidget(
          BlocProvider.value(
            value: cubit,
            child: MaterialApp(
              home: Scaffold(
                body: IngredientsListView(),
              ),
            ),
          ),
        );

        expect(
          find.byWidgetPredicate((widget) {
            return widget is PaddedReorderableListView &&
                widget.padding == ScrollViewEdgeInsets.onlyTop &&
                widget.buildDefaultDragHandles == true;
          }),
          findsOneWidget,
        );
        expect(find.byType(IngredientWidget), findsNWidgets(recipe.steps.length));
        expect(find.byType(AddIngredientButton), findsOneWidget);

        final Offset startDragLocation = tester.getCenter(find.byType(ReorderIconButton).first);
        final Offset endDragLocation = tester.getCenter(find.byType(ReorderIconButton).last);

        final TestGesture gesture = await tester.startGesture(startDragLocation);
        await tester.pump(Duration(milliseconds: 600)); // simulate a long-press, by waiting while down is held
        await gesture.moveTo(endDragLocation);
        await gesture.up();

        verify(() => cubit.reorderIngredient(0, 1)).called(1);
      });
    });

    group('IngredientWidget', () {
      testWidgets('view_mode w/ _unchecked_ step -- has correct UI & calls cubit-method when tapped',
          (WidgetTester tester) async {
        when(() => cubit.state).thenReturn(stateViewMode);

        final _index = 1;
        final _ingredient = recipe.ingredients[_index];

        await tester.pumpWidget(
          BlocProvider.value(
            value: cubit,
            child: MaterialApp(
              home: Scaffold(
                body: IngredientWidget(_index),
              ),
            ),
          ),
        );

        expect(
          find.byWidgetPredicate((widget) {
            return widget is CheckboxListTile &&
                widget.value == false &&
                widget.title is Opacity &&
                (widget.title as Opacity).opacity == 1.0 &&
                ((widget.title as Opacity).child as Row).children.length == 3 &&
                (((widget.title as Opacity).child as Row).children[0] as Expanded).flex == 4 &&
                ((((widget.title as Opacity).child as Row).children[0] as Expanded).child as TextField)
                        .controller!
                        .text ==
                    _ingredient.amount &&
                ((((widget.title as Opacity).child as Row).children[0] as Expanded).child as TextField).maxLines ==
                    null &&
                ((((widget.title as Opacity).child as Row).children[0] as Expanded).child as TextField).decoration
                    is RecipeTextDecoration &&
                ((((widget.title as Opacity).child as Row).children[0] as Expanded).child as TextField).textAlign ==
                    TextAlign.end &&
                ((((widget.title as Opacity).child as Row).children[0] as Expanded).child as TextField).enabled ==
                    false &&
                (((widget.title as Opacity).child as Row).children[1] as SizedBox).width == 24.0 &&
                (((widget.title as Opacity).child as Row).children[2] as Expanded).flex == 7 &&
                ((((widget.title as Opacity).child as Row).children[2] as Expanded).child as TextField)
                        .controller!
                        .text ==
                    _ingredient.description &&
                ((((widget.title as Opacity).child as Row).children[2] as Expanded).child as TextField).maxLines ==
                    null &&
                ((((widget.title as Opacity).child as Row).children[2] as Expanded).child as TextField).decoration
                    is RecipeTextDecoration &&
                ((((widget.title as Opacity).child as Row).children[2] as Expanded).child as TextField)
                        .decoration!
                        .hintText ==
                    null &&
                ((((widget.title as Opacity).child as Row).children[2] as Expanded).child as TextField).textAlign ==
                    TextAlign.start &&
                ((((widget.title as Opacity).child as Row).children[2] as Expanded).child as TextField).enabled ==
                    false;
          }),
          findsOneWidget,
        );

        await tester.tap(find.byType(CheckboxListTile));
        verify(() => cubit.toggleIngredientCompleted(_ingredient, newValue: true)).called(1);
      });

      testWidgets('view_mode w/ _checked_ step -- has correct UI & calls cubit-method when tapped',
          (WidgetTester tester) async {
        final _index = 1;
        final _ingredient = recipe.ingredients[_index];

        when(() => cubit.state).thenReturn(
          RecipeState(recipe, labelColors: labelColors, completedIngredients: [_ingredient]),
        );

        await tester.pumpWidget(
          BlocProvider.value(
            value: cubit,
            child: MaterialApp(
              home: Scaffold(
                body: IngredientWidget(_index),
              ),
            ),
          ),
        );

        expect(
          find.byWidgetPredicate((widget) {
            return widget is CheckboxListTile &&
                widget.value == true &&
                widget.title is Opacity &&
                (widget.title as Opacity).opacity == 0.5 &&
                ((widget.title as Opacity).child as Row).children.length == 3 &&
                (((widget.title as Opacity).child as Row).children[0] as Expanded).flex == 4 &&
                ((((widget.title as Opacity).child as Row).children[0] as Expanded).child as TextField)
                        .controller!
                        .text ==
                    _ingredient.amount &&
                ((((widget.title as Opacity).child as Row).children[0] as Expanded).child as TextField).maxLines ==
                    null &&
                ((((widget.title as Opacity).child as Row).children[0] as Expanded).child as TextField).decoration
                    is RecipeTextDecoration &&
                ((((widget.title as Opacity).child as Row).children[0] as Expanded).child as TextField).textAlign ==
                    TextAlign.end &&
                ((((widget.title as Opacity).child as Row).children[0] as Expanded).child as TextField).enabled ==
                    false &&
                (((widget.title as Opacity).child as Row).children[1] as SizedBox).width == 24.0 &&
                (((widget.title as Opacity).child as Row).children[2] as Expanded).flex == 7 &&
                ((((widget.title as Opacity).child as Row).children[2] as Expanded).child as TextField)
                        .controller!
                        .text ==
                    _ingredient.description &&
                ((((widget.title as Opacity).child as Row).children[2] as Expanded).child as TextField).maxLines ==
                    null &&
                ((((widget.title as Opacity).child as Row).children[2] as Expanded).child as TextField).decoration
                    is RecipeTextDecoration &&
                ((((widget.title as Opacity).child as Row).children[2] as Expanded).child as TextField)
                        .decoration!
                        .hintText ==
                    null &&
                ((((widget.title as Opacity).child as Row).children[2] as Expanded).child as TextField).textAlign ==
                    TextAlign.start &&
                ((((widget.title as Opacity).child as Row).children[2] as Expanded).child as TextField).enabled ==
                    false;
          }),
          findsOneWidget,
        );

        await tester.tap(find.byType(CheckboxListTile));
        verify(() => cubit.toggleIngredientCompleted(_ingredient, newValue: false)).called(1);
      });

      testWidgets('edit_mode -- has correct UI & calls cubit-method when tapped', (WidgetTester tester) async {
        when(() => cubit.state).thenReturn(stateEditMode);

        final _index = 1;
        final _ingredient = editedRecipe.ingredients[_index];

        await tester.pumpWidget(
          BlocProvider.value(
            value: cubit,
            child: MaterialApp(
              home: Scaffold(
                body: IngredientWidget(_index),
              ),
            ),
          ),
        );

        expect(
          find.byWidgetPredicate((widget) {
            return widget is ListTile &&
                widget.title is Row &&
                (widget.title as Row).children.length == 3 &&
                ((widget.title as Row).children[0] as Expanded).flex == 4 &&
                (((widget.title as Row).children[0] as Expanded).child as TextField).controller!.text ==
                    _ingredient.amount &&
                (((widget.title as Row).children[0] as Expanded).child as TextField).maxLines == null &&
                (((widget.title as Row).children[0] as Expanded).child as TextField).decoration
                    is RecipeTextDecoration &&
                (((widget.title as Row).children[0] as Expanded).child as TextField).decoration!.hintText == "amount" &&
                (((widget.title as Row).children[0] as Expanded).child as TextField).textAlign == TextAlign.end &&
                (((widget.title as Row).children[0] as Expanded).child as TextField).enabled == true &&
                ((widget.title as Row).children[1] as SizedBox).width == 24.0 &&
                ((widget.title as Row).children[2] as Expanded).flex == 7 &&
                (((widget.title as Row).children[2] as Expanded).child as TextField).controller!.text ==
                    _ingredient.description &&
                (((widget.title as Row).children[2] as Expanded).child as TextField).maxLines == null &&
                (((widget.title as Row).children[2] as Expanded).child as TextField).decoration
                    is RecipeTextDecoration &&
                (((widget.title as Row).children[2] as Expanded).child as TextField).decoration!.hintText ==
                    "ingredient" &&
                (((widget.title as Row).children[2] as Expanded).child as TextField).textAlign == TextAlign.start &&
                (((widget.title as Row).children[2] as Expanded).child as TextField).enabled == true &&
                widget.trailing is ReorderIconButton;
          }),
          findsOneWidget,
        );

        await tester.enterText(find.byType(TextField).first, "15 pieces");
        verify(() => cubit.updateIngredient(Ingredient("15 pieces", "stuff"), ingredientIndex: 1)).called(1);

        await tester.enterText(find.byType(TextField).last, "flair");
        verify(() => cubit.updateIngredient(Ingredient("15 pieces", "flair"), ingredientIndex: 1)).called(1);
      });
    });

    group('AddIngredientButton', () {
      testWidgets('has correct UI & UX', (WidgetTester tester) async {
        await tester.pumpWidget(
          BlocProvider.value(
            value: cubit,
            child: MaterialApp(
              home: AddIngredientButton(),
            ),
          ),
        );

        expect(
          find.byWidgetPredicate((widget) {
            return widget is AddRecipeElementButton &&
                widget.text == "Add ingredient" &&
                widget.onPressed == cubit.addIngredient;
          }),
          findsOneWidget,
        );
      });
    });

    group('StepsListView', () {
      testWidgets('view_mode -- has correct UI', (WidgetTester tester) async {
        when(() => cubit.state).thenReturn(stateViewMode);

        await tester.pumpWidget(
          BlocProvider.value(
            value: cubit,
            child: MaterialApp(
              home: Scaffold(
                body: StepsListView(),
              ),
            ),
          ),
        );

        expect(
          find.byWidgetPredicate((widget) {
            return widget is PaddedReorderableListView &&
                widget.padding == ScrollViewEdgeInsets() &&
                widget.buildDefaultDragHandles == false;
          }),
          findsOneWidget,
        );
        expect(find.byType(StepWidget), findsNWidgets(recipe.steps.length));
        expect(find.byType(AddStepButton), findsNothing);
      });

      testWidgets('edit_mode -- has correct UI', (WidgetTester tester) async {
        when(() => cubit.state).thenReturn(stateEditMode);

        await tester.pumpWidget(
          BlocProvider.value(
            value: cubit,
            child: MaterialApp(
              home: Scaffold(
                body: StepsListView(),
              ),
            ),
          ),
        );

        expect(
          find.byWidgetPredicate((widget) {
            return widget is PaddedReorderableListView &&
                widget.padding == ScrollViewEdgeInsets.onlyTop &&
                widget.buildDefaultDragHandles == true;
          }),
          findsOneWidget,
        );
        expect(find.byType(StepWidget), findsNWidgets(recipe.steps.length));
        expect(find.byType(AddStepButton), findsOneWidget);

        final Offset startDragLocation = tester.getCenter(find.byType(ReorderIconButton).first);
        final Offset endDragLocation = tester.getCenter(find.byType(ReorderIconButton).last);

        final TestGesture gesture = await tester.startGesture(startDragLocation);
        await tester.pump(Duration(milliseconds: 600)); // simulate a long-press, by waiting while down is held
        await gesture.moveTo(endDragLocation);
        await gesture.up();

        verify(() => cubit.reorderStep(0, 1)).called(1);
      });
    });

    group('StepWidget', () {
      testWidgets('view_mode w/ _unchecked_ step -- has correct UI & UX', (WidgetTester tester) async {
        when(() => cubit.state).thenReturn(stateViewMode);

        final _index = 0;
        final _step = recipe.steps[_index];

        await tester.pumpWidget(
          BlocProvider.value(
            value: cubit,
            child: MaterialApp(
              home: Scaffold(
                body: StepWidget(_index),
              ),
            ),
          ),
        );

        expect(
          find.byWidgetPredicate((widget) {
            return widget is CheckboxListTile &&
                widget.value == false &&
                widget.title is Opacity &&
                (widget.title as Opacity).opacity == 1.0 &&
                (widget.title as Opacity).child is TextField &&
                ((widget.title as Opacity).child as TextField).controller!.text == _step &&
                ((widget.title as Opacity).child as TextField).decoration is RecipeTextDecoration &&
                ((widget.title as Opacity).child as TextField).decoration!.hintText == "step" &&
                ((widget.title as Opacity).child as TextField).decoration!.prefixText == "${_index + 1}.  " &&
                ((widget.title as Opacity).child as TextField).enabled == false &&
                ((widget.title as Opacity).child as TextField).maxLines == null &&
                ((widget.title as Opacity).child as TextField).style!.height == 1.5;
          }),
          findsOneWidget,
        );

        await tester.tap(find.byType(CheckboxListTile));
        verify(() => cubit.toggleStepCompleted(_step, newValue: true)).called(1);
      });

      testWidgets('view_mode w/ _checked_ step -- has correct UI & UX', (WidgetTester tester) async {
        when(() => cubit.state).thenReturn(
          RecipeState(recipe, labelColors: labelColors, completedSteps: [recipe.steps[0]]),
        );

        final _index = 0;
        final _step = recipe.steps[_index];

        await tester.pumpWidget(
          BlocProvider.value(
            value: cubit,
            child: MaterialApp(
              home: Scaffold(
                body: StepWidget(_index),
              ),
            ),
          ),
        );

        expect(
          find.byWidgetPredicate((widget) {
            return widget is CheckboxListTile &&
                widget.value == true &&
                widget.title is Opacity &&
                (widget.title as Opacity).opacity == 0.5 &&
                (widget.title as Opacity).child is TextField &&
                ((widget.title as Opacity).child as TextField).controller!.text == _step &&
                ((widget.title as Opacity).child as TextField).decoration is RecipeTextDecoration &&
                ((widget.title as Opacity).child as TextField).decoration!.hintText == "step" &&
                ((widget.title as Opacity).child as TextField).decoration!.prefixText == "${_index + 1}.  " &&
                ((widget.title as Opacity).child as TextField).enabled == false &&
                ((widget.title as Opacity).child as TextField).maxLines == null &&
                ((widget.title as Opacity).child as TextField).style!.height == 1.5;
          }),
          findsOneWidget,
        );

        await tester.tap(find.byType(CheckboxListTile));
        verify(() => cubit.toggleStepCompleted(_step, newValue: false)).called(1);
      });

      testWidgets('edit_mode -- has correct UI & UX', (WidgetTester tester) async {
        when(() => cubit.state).thenReturn(stateEditMode);

        final _index = 0;
        final _step = editedRecipe.steps[_index];

        await tester.pumpWidget(
          BlocProvider.value(
            value: cubit,
            child: MaterialApp(
              home: Scaffold(
                body: StepWidget(_index),
              ),
            ),
          ),
        );

        expect(
          find.byWidgetPredicate((widget) {
            return widget is ListTile &&
                widget.title is TextField &&
                (widget.title as TextField).controller!.text == _step &&
                (widget.title as TextField).decoration is RecipeTextDecoration &&
                (widget.title as TextField).decoration!.hintText == "step" &&
                (widget.title as TextField).decoration!.prefixText == "${_index + 1}.  " &&
                (widget.title as TextField).enabled == true &&
                (widget.title as TextField).maxLines == null &&
                (widget.title as TextField).style!.height == 1.5 &&
                widget.trailing is ReorderIconButton;
          }),
          findsOneWidget,
        );

        await tester.enterText(find.byType(TextField), "wear flair");
        verify(() => cubit.updateStep("wear flair", stepIndex: _index)).called(1);
      });
    });

    group('AddStepButton', () {
      testWidgets('has correct UI & UX', (WidgetTester tester) async {
        await tester.pumpWidget(
          BlocProvider.value(
            value: cubit,
            child: MaterialApp(
              home: AddStepButton(),
            ),
          ),
        );

        expect(
          find.byWidgetPredicate((widget) {
            return widget is AddRecipeElementButton && widget.text == "Add step" && widget.onPressed == cubit.addStep;
          }),
          findsOneWidget,
        );
      });
    });

    group('LabelsListView', () {
      testWidgets('view_mode -- has correct UI', (WidgetTester tester) async {
        when(() => cubit.state).thenReturn(stateViewMode);

        await tester.pumpWidget(
          BlocProvider.value(
            value: cubit,
            child: MaterialApp(
              home: Scaffold(
                body: LabelsListView(),
              ),
            ),
          ),
        );

        expect(
          find.byWidgetPredicate((widget) {
            return widget is PaddedReorderableListView &&
                widget.padding == ScrollViewEdgeInsets.onlyBottom &&
                widget.buildDefaultDragHandles == false;
          }),
          findsOneWidget,
        );
        expect(find.byType(LabelWidget), findsNWidgets(cubit.state.recipe.labels.length));
        expect(find.byType(AddLabelButton), findsNothing);
      });

      testWidgets('edit_mode -- has correct UI', (WidgetTester tester) async {
        when(() => cubit.state).thenReturn(stateEditMode);

        await tester.pumpWidget(
          BlocProvider.value(
            value: cubit,
            child: MaterialApp(
              home: Scaffold(
                body: LabelsListView(),
              ),
            ),
          ),
        );

        expect(
          find.byWidgetPredicate((widget) {
            return widget is PaddedReorderableListView &&
                widget.padding == ScrollViewEdgeInsets.none &&
                widget.buildDefaultDragHandles == true;
          }),
          findsOneWidget,
        );
        expect(find.byType(LabelWidget), findsNWidgets(cubit.state.editedRecipe!.labels.length));
        expect(find.byType(AddLabelButton), findsOneWidget);

        final Offset startDragLocation = tester.getCenter(find.byType(ReorderIconButton).first);
        final Offset endDragLocation = tester.getCenter(find.byType(ReorderIconButton).last);

        final TestGesture gesture = await tester.startGesture(startDragLocation);
        await tester.pump(Duration(milliseconds: 600)); // simulate a long-press, by waiting while down is held
        await gesture.moveTo(endDragLocation);
        await gesture.up();

        verify(() => cubit.reorderLabel(0, 1)).called(1);
      });
    });

    group('LabelWidget', () {
      testWidgets('view_mode -- has correct UI', (WidgetTester tester) async {
        when(() => cubit.state).thenReturn(stateViewMode);
        final _index = 1;
        final _label = recipe.labels[_index];

        await tester.pumpWidget(
          BlocProvider.value(
            value: cubit,
            child: MaterialApp(
              home: Scaffold(
                body: LabelWidget(_index),
              ),
            ),
          ),
        );

        expect(
          find.byWidgetPredicate((widget) {
            return widget is Container &&
                widget.decoration is LabelColorGradient &&
                widget.child is ListTile &&
                (widget.child as ListTile).leading is ColorSelector &&
                (widget.child as ListTile).title is LabelTextField &&
                (widget.child as ListTile).trailing == null;
          }),
          findsOneWidget,
        );
        expect(
          find.byWidgetPredicate((widget) {
            return widget is LabelTextField &&
                widget.label == _label &&
                widget.labelIndex == _index &&
                widget.isInEditMode == false;
          }),
          findsOneWidget,
        );
        expect(
          find.byWidgetPredicate((widget) {
            return widget is TextField &&
                widget.controller!.text == _label &&
                widget.decoration is RecipeTextDecoration &&
                widget.decoration!.hintText == "label" &&
                widget.enabled == false;
          }),
          findsOneWidget,
        );
        expect(find.text(_label), findsOneWidget);
      });

      testWidgets('edit_mode -- has correct UI', (WidgetTester tester) async {
        when(() => cubit.state).thenReturn(stateEditMode);
        final _index = 1;
        final _label = editedRecipe.labels[_index];

        await tester.pumpWidget(
          BlocProvider.value(
            value: cubit,
            child: MaterialApp(
              home: Scaffold(
                body: LabelWidget(_index),
              ),
            ),
          ),
        );
        expect(
          find.byWidgetPredicate((widget) {
            return widget is Container &&
                widget.decoration == null &&
                widget.child is ListTile &&
                (widget.child as ListTile).leading is ColorSelector &&
                ((widget.child as ListTile).title as Row).children.length == 2 &&
                (((widget.child as ListTile).title as Row).children[0] as Expanded).child is LabelTextField &&
                ((widget.child as ListTile).title as Row).children[1] is DeleteLabelButton &&
                (widget.child as ListTile).trailing is ReorderIconButton;
          }),
          findsOneWidget,
        );

        expect(
          find.byWidgetPredicate((widget) {
            return widget is LabelTextField &&
                widget.label == _label &&
                widget.labelIndex == _index &&
                widget.isInEditMode == true;
          }),
          findsOneWidget,
        );
        expect(
          find.byWidgetPredicate((widget) {
            return widget is TextField &&
                widget.controller!.text == _label &&
                widget.decoration is RecipeTextDecoration &&
                widget.decoration!.hintText == "label" &&
                widget.enabled == true;
          }),
          findsOneWidget,
        );
        expect(find.text(_label), findsOneWidget);

        await tester.enterText(find.byType(TextField), "TPS");
        verify(() => cubit.updateLabelText("TPS", labelAtIndex: _index)).called(1);
      });
    });

    testWidgets('DeleteLabelButton -- has correct UI', (WidgetTester tester) async {
      when(() => cubit.state).thenReturn(stateEditMode);
      final _labelIndex = 0;

      await tester.pumpWidget(
        BlocProvider.value(
          value: cubit,
          child: MaterialApp(
            home: Scaffold(
              body: DeleteLabelButton(labelIndex: _labelIndex),
            ),
          ),
        ),
      );
      expect(
        find.byWidgetPredicate((widget) {
          return widget is InkWell && ((widget.child as IconButton).icon as Icon).icon == Icons.delete_outline;
        }),
        findsOneWidget,
      );

      await tester.tap(find.byIcon(Icons.delete_outline));
      await tester.pump();
      expect(
        find.byWidgetPredicate((widget) {
          return widget is SnackBar && (widget.content as Text).data == "Long-press to remove label.";
        }),
        findsOneWidget,
      );

      await tester.longPress(find.byIcon(Icons.delete_outline));
      await tester.pumpAndSettle(Duration(seconds: 4)); // allow previous snack_bar to dismiss
      expect(
        find.byWidgetPredicate((widget) {
          return widget is SnackBar && (widget.content as Text).data == 'Label removed.';
        }),
        findsOneWidget,
      );
      verify(() => cubit.deleteLabel(_labelIndex)).called(1);
    });

    group('AddLabelButton', () {
      testWidgets('has correct UI & UX', (WidgetTester tester) async {
        await tester.pumpWidget(
          BlocProvider.value(
            value: cubit,
            child: MaterialApp(
              home: AddLabelButton(),
            ),
          ),
        );

        expect(
          find.byWidgetPredicate((widget) {
            return widget is AddRecipeElementButton && widget.text == "Add label" && widget.onPressed == cubit.addLabel;
          }),
          findsOneWidget,
        );
      });
    });

    group('WebsiteWidget', () {
      final _recipe = Recipe("", labels: [], ingredients: [], steps: [], url: "initech.net");

      testWidgets('view_mode -- has expected UI', (WidgetTester tester) async {
        when(() => cubit.state).thenReturn(RecipeState(_recipe, labelColors: []));

        await tester.pumpWidget(
          BlocProvider.value(
            value: cubit,
            child: MaterialApp(
              home: Scaffold(
                body: WebsiteWidget(),
              ),
            ),
          ),
        );

        expect(
          find.byWidgetPredicate((widget) {
            return widget is Padding &&
                widget.padding == ScrollViewEdgeInsets.onlyTop &&
                (widget.child as ListTile).leading is CopyButton &&
                ((widget.child as ListTile).leading as CopyButton).isEnabled == true &&
                ((widget.child as ListTile).title as TextField).controller!.text == "initech.net" &&
                ((widget.child as ListTile).title as TextField).decoration is RecipeTextDecoration &&
                ((widget.child as ListTile).title as TextField).decoration!.labelText == "URL for this recipe" &&
                ((widget.child as ListTile).title as TextField).enabled == false;
          }),
          findsOneWidget,
        );
      });

      testWidgets('edit_mode -- has expected UI', (WidgetTester tester) async {
        when(() => cubit.state).thenReturn(
          RecipeStateEditMode(recipe: _recipe, editedRecipe: _recipe, labelColors: []),
        );

        await tester.pumpWidget(
          BlocProvider.value(
            value: cubit,
            child: MaterialApp(
              home: Scaffold(
                body: WebsiteWidget(),
              ),
            ),
          ),
        );

        expect(
          find.byWidgetPredicate((widget) {
            return widget is Padding &&
                widget.padding == ScrollViewEdgeInsets.onlyTop &&
                (widget.child as ListTile).leading is CopyButton &&
                ((widget.child as ListTile).leading as CopyButton).isEnabled == false &&
                ((widget.child as ListTile).title as TextField).controller!.text == "initech.net" &&
                ((widget.child as ListTile).title as TextField).decoration is RecipeTextDecoration &&
                ((widget.child as ListTile).title as TextField).decoration!.labelText == "URL for this recipe" &&
                ((widget.child as ListTile).title as TextField).enabled == true;
          }),
          findsOneWidget,
        );

        await tester.enterText(find.byType(TextField), "www.initech.net");
        verify(() => cubit.updateUrl("www.initech.net")).called(1);
      });
    });

    group('CopyButton', () {
      group('enabled', () {
        testWidgets('with _typical_ url -- has expected UI/UX', (WidgetTester tester) async {
          when(() => cubit.state).thenReturn(
            RecipeState(
              Recipe("", labels: [], ingredients: [], steps: [], url: "initech.net"),
              labelColors: [],
            ),
          );

          await tester.pumpWidget(
            BlocProvider.value(
              value: cubit,
              child: MaterialApp(
                home: Scaffold(
                  body: CopyButton(isEnabled: true),
                ),
              ),
            ),
          );
          expect(
            find.byWidgetPredicate((widget) {
              return widget is IconButton &&
                  (widget.icon as Icon).icon == Icons.copy &&
                  (widget.icon as Icon).color != Colors.transparent &&
                  widget.onPressed != null;
            }),
            findsOneWidget,
          );

          when(cubit.copyUrlToClipboard).thenAnswer((_) async {});
          await tester.tap(find.byIcon(Icons.copy));
          verify(cubit.copyUrlToClipboard).called(1);

          await tester.pump();
          expect(
            find.byWidgetPredicate((widget) {
              return widget is SnackBar &&
                  widget.content is Text &&
                  (widget.content as Text).data == 'Copied to clipboard:\n\n"initech.net"';
            }),
            findsOneWidget,
          );
        });

        testWidgets('with _blank_ url -- has expected UI/UX', (WidgetTester tester) async {
          when(() => cubit.state).thenReturn(
            RecipeState(
              Recipe("", labels: [], ingredients: [], steps: [], url: ""),
              labelColors: [],
            ),
          );

          await tester.pumpWidget(
            BlocProvider.value(
              value: cubit,
              child: MaterialApp(
                home: Scaffold(
                  body: CopyButton(isEnabled: true),
                ),
              ),
            ),
          );
          expect(
            find.byWidgetPredicate((widget) {
              return widget is IconButton &&
                  (widget.icon as Icon).icon == Icons.copy &&
                  (widget.icon as Icon).color != Colors.transparent &&
                  widget.onPressed != null;
            }),
            findsOneWidget,
          );

          await tester.tap(find.byIcon(Icons.copy));
          verifyNever(cubit.copyUrlToClipboard);

          await tester.pump();
          expect(
            find.byWidgetPredicate((widget) {
              return widget is SnackBar &&
                  widget.content is Text &&
                  (widget.content as Text).data == 'Nothing to copy.';
            }),
            findsOneWidget,
          );
        });

        testWidgets('with _null_ url -- has expected UI/UX', (WidgetTester tester) async {
          when(() => cubit.state).thenReturn(
            RecipeState(
              Recipe("", labels: [], ingredients: [], steps: [], url: null),
              labelColors: [],
            ),
          );

          await tester.pumpWidget(
            BlocProvider.value(
              value: cubit,
              child: MaterialApp(
                home: Scaffold(
                  body: CopyButton(isEnabled: true),
                ),
              ),
            ),
          );
          expect(
            find.byWidgetPredicate((widget) {
              return widget is IconButton &&
                  (widget.icon as Icon).icon == Icons.copy &&
                  (widget.icon as Icon).color != Colors.transparent &&
                  widget.onPressed != null;
            }),
            findsOneWidget,
          );

          await tester.tap(find.byIcon(Icons.copy));
          verifyNever(cubit.copyUrlToClipboard);

          await tester.pump();
          expect(
            find.byWidgetPredicate((widget) {
              return widget is SnackBar &&
                  widget.content is Text &&
                  (widget.content as Text).data == 'Nothing to copy.';
            }),
            findsOneWidget,
          );
        });
      });

      testWidgets('disabled -- has expected UI/UX', (WidgetTester tester) async {
        await tester.pumpWidget(
          BlocProvider.value(
            value: cubit,
            child: MaterialApp(
              home: Scaffold(
                body: CopyButton(isEnabled: false),
              ),
            ),
          ),
        );
        expect(
          find.byWidgetPredicate((widget) {
            return widget is IconButton &&
                (widget.icon as Icon).icon == Icons.copy &&
                (widget.icon as Icon).color == Colors.transparent &&
                widget.onPressed == null;
          }),
          findsOneWidget,
        );
      });
    });

    group('AddRecipeElementButton', () {
      testWidgets('has correct UI & UX', (WidgetTester tester) async {
        final _buttonText = "PC load letter";

        await tester.pumpWidget(
          BlocProvider.value(
            value: cubit,
            child: MaterialApp(
              home: AddRecipeElementButton(
                _buttonText,
                onPressed: cubit.saveEdits,
              ),
            ),
          ),
        );

        expect(
          find.byWidgetPredicate((widget) {
            return widget is Padding &&
                widget.padding == EdgeInsets.symmetric(vertical: 8, horizontal: 16) &&
                widget.child is Row &&
                (widget.child as Row).mainAxisAlignment == MainAxisAlignment.start &&
                (widget.child as Row).children.length == 1 &&
                (widget.child as Row).children[0] is OutlinedButton;
          }),
          findsOneWidget,
        );
        expect(find.byIcon(Icons.add), findsOneWidget);
        expect(find.text(_buttonText), findsOneWidget);

        await tester.tap(find.byIcon(Icons.add));
        verify(cubit.saveEdits).called(1);
      });
    });

    group('DoneEditingRecipeWidget', () {
      group('view_mode', () {
        testWidgets('has expected UI & UX', (WidgetTester tester) async {
          when(() => cubit.state).thenReturn(stateViewMode);

          await tester.pumpWidget(
            MaterialApp(
              home: BlocProvider.value(
                value: cubit,
                child: DoneEditingRecipeWidget(),
              ),
            ),
          );

          expect(
            find.byWidgetPredicate((widget) {
              return widget is SizedBox && widget.width == null && widget.height == null;
            }),
            findsOneWidget,
          );
          expect(find.byType(Padding), findsNothing);
          expect(find.byType(ElevatedButton), findsNothing);
          expect(find.byIcon(Icons.done), findsNothing);
          expect(find.text("Save changes"), findsNothing);
        });
      });

      group('edit_mode', () {
        testWidgets('recipe has a title -- has expected UI & UX', (WidgetTester tester) async {
          when(() => cubit.state).thenReturn(stateEditMode);

          await tester.pumpWidget(
            BlocProvider.value(
              value: cubit,
              child: MaterialApp(
                home: Scaffold(
                  body: DoneEditingRecipeWidget(),
                ),
              ),
            ),
          );

          expect(
            find.byWidgetPredicate((widget) {
              return widget is Padding &&
                  widget.padding == EdgeInsets.symmetric(vertical: 24) &&
                  widget.child is ElevatedButton;
            }),
            findsOneWidget,
          );
          expect(find.byIcon(Icons.done), findsOneWidget);
          expect(find.text("Save changes"), findsOneWidget);

          await tester.tap(find.byType(DoneEditingRecipeWidget));
          await tester.pump();
          expect(find.byType(SnackBar), findsNothing);
          verify(cubit.saveEdits).called(1);
        });

        testWidgets('recipe has blank title -- has expected UI & UX', (WidgetTester tester) async {
          when(() => cubit.state).thenReturn(
            RecipeStateEditMode(
              recipe: recipe,
              editedRecipe: Recipe(" ", labels: [], ingredients: [], steps: []),
              labelColors: labelColors,
            ),
          );

          await tester.pumpWidget(
            BlocProvider.value(
              value: cubit,
              child: MaterialApp(
                home: Scaffold(
                  body: DoneEditingRecipeWidget(),
                ),
              ),
            ),
          );

          await tester.tap(find.byType(DoneEditingRecipeWidget));
          await tester.pumpAndSettle();
          expect(find.byType(SnackBar), findsOneWidget);
          expect(
            find.byWidgetPredicate((widget) {
              return widget is SnackBar &&
                  (widget.content as Text).data == "Recipe title cannot be blank." &&
                  widget.action == null;
            }),
            findsOneWidget,
          );
          verifyNever(cubit.saveEdits);
        });
      });
    });

    test("RecipeTextDecoration", () {
      var textDecoration = RecipeTextDecoration(hasBorder: false);
      expect(textDecoration.labelText, null);
      expect(textDecoration.hintText, null);
      expect(textDecoration.hintStyle, null);
      expect(textDecoration.prefixText, null);
      expect(textDecoration.border, InputBorder.none);

      textDecoration = RecipeTextDecoration(hasBorder: false, hintText: RecipeTitle.hintText);
      expect(textDecoration.labelText, null);
      expect(textDecoration.hintText, "recipe title");
      expect(textDecoration.hintStyle, TextStyle(color: Colors.white70));
      expect(textDecoration.prefixText, null);
      expect(textDecoration.border, InputBorder.none);

      textDecoration = RecipeTextDecoration(hasBorder: true, labelText: "PC", hintText: "load", prefixText: "letter");
      expect(textDecoration.labelText, "PC");
      expect(textDecoration.hintText, "load");
      expect(textDecoration.hintStyle, null);
      expect(textDecoration.prefixText, "letter");
      expect(textDecoration.border, null);
    });
  });
}
