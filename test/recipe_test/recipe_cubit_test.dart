import 'dart:io';

import 'package:bloc_test/bloc_test.dart';
import 'package:captainfoodmaker/data_models/label_colors.dart';
import 'package:captainfoodmaker/data_models/recipe.dart';
import 'package:captainfoodmaker/interfaces/clipboard_interface.dart';
import 'package:captainfoodmaker/interfaces/hive_storage_interface.dart';
import 'package:captainfoodmaker/interfaces/web_interface.dart';
import 'package:captainfoodmaker/recipe/recipe_cubit.dart';
import 'package:captainfoodmaker/shared_getters.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart';
import 'package:mocktail/mocktail.dart';

// ignore_for_file: prefer_const_constructors
// ignore_for_file: prefer_const_declarations
// ignore_for_file: prefer_const_literals_to_create_immutables

class MockClipboardInterface extends Mock implements ClipboardInterface {}

class MockHiveStorageInterface extends Mock implements HiveStorageInterface {}

class MockWebInterface extends Mock implements WebInterface {}

void main() {
  final stuffIngredient = Ingredient("", "stuff");
  final thingsIngredient = Ingredient("", "things");
  final step0 = "step_0";
  final step1 = "step_1";
  final initechDotCom = "initech.com";

  final Recipe recipe = Recipe(
    "TPS",
    labels: ["PC", "load letter"],
    ingredients: [stuffIngredient, thingsIngredient],
    steps: [step0, step1],
    url: initechDotCom,
  );
  final List<LabelColor> labelColors = [LabelColor("load letter", Colors.red)];
  late RecipeCubit cubit;
  late ClipboardInterface clipboardInterface;
  late HiveStorageInterface storageInterface;
  late WebInterface webInterface;

  setUp(() {
    clipboardInterface = MockClipboardInterface();
    storageInterface = MockHiveStorageInterface();
    webInterface = MockWebInterface();
    cubit = RecipeCubit(
      recipe,
      labelColors,
      clipboardInterface,
      storageInterface,
      webInterface,
    );
  });

  tearDown(() {
    cubit.close();
  });

  group("on init with _non-blank_ recipe", () {
    blocTest<RecipeCubit, RecipeState>(
      "triggers initial state (for viewing a previously added recipe)",
      build: () => cubit,
      expect: () => [],
      verify: (_) {
        expect(
          cubit.state,
          RecipeStateInitial(recipe, labelColors: labelColors),
        );
      },
    );
  });

  group("on init with _blank_ recipe", () {
    late RecipeCubit cubitWithBlankRecipe;

    setUp(() {
      cubitWithBlankRecipe = RecipeCubit(Recipe.blank, labelColors, clipboardInterface, storageInterface, webInterface);
    });

    blocTest<RecipeCubit, RecipeState>(
      "triggers prompt_mode state (for asking user how they want to proceed)",
      build: () => cubitWithBlankRecipe,
      expect: () => [],
      verify: (_) {
        expect(
          cubitWithBlankRecipe.state,
          RecipeStatePromptForBlankRecipeOrPasteFromClipboard(
            labelColors: labelColors,
          ),
        );
      },
    );
  });

  group('exportRecipe', () {
    blocTest<RecipeCubit, RecipeState>(
      'calls copyTextToClipboard()',
      build: () {
        when(() => clipboardInterface.copyTextToClipboard(recipe.toString())).thenAnswer((_) async => Future<void>);
        return cubit;
      },
      act: (cubit) async => await cubit.exportRecipe(),
      expect: () => [],
      verify: (_) {
        verify(() => clipboardInterface.copyTextToClipboard(recipe.toString())).called(1);
      },
    );
  });

  group("delete/restore", () {
    group("deleteRecipe", () {
      blocTest<RecipeCubit, RecipeState>(
        "updates list of deleted recipes & emits new state",
        build: () {
          when(() => storageInterface.setDeletionStatusForRecipe(recipe, newDeletionStatus: true))
              .thenAnswer((_) async => {});
          return cubit;
        },
        act: (cubit) => cubit.deleteRecipe(),
        expect: () => [RecipeState(recipe, labelColors: labelColors, isRecipeDeleted: true)],
        verify: (_) {
          verify(() => storageInterface.setDeletionStatusForRecipe(recipe, newDeletionStatus: true)).called(1);
        },
      );
    });

    group("restoreRecipe", () {
      blocTest<RecipeCubit, RecipeState>(
        "updates list of deleted recipes & emits new state",
        build: () {
          when(() => storageInterface.setDeletionStatusForRecipe(recipe, newDeletionStatus: false))
              .thenAnswer((_) async => {});
          return cubit;
        },
        act: (cubit) => cubit.restoreRecipe(),
        expect: () => [RecipeState(recipe, labelColors: labelColors, isRecipeDeleted: false)],
        verify: (_) {
          verify(() => storageInterface.setDeletionStatusForRecipe(recipe, newDeletionStatus: false)).called(1);
        },
      );
    });
  });

  group("toggleEditMode", () {
    group("_without_ an edit in-progress", () {
      blocTest<RecipeCubit, RecipeState>(
        "toggling edit_mode _on_ emits RecipeStateEditMode",
        build: () {
          editedRecipes = {};
          return cubit;
        },
        act: (cubit) => cubit.enableEditMode(),
        expect: () => [
          RecipeStateEditMode(
            recipe: recipe,
            editedRecipe: recipe,
            labelColors: labelColors,
          ),
        ],
      );
    });

    group("_with_ an edit in-progress", () {
      var editInProgress = Recipe("in_progress", labels: [], ingredients: [], steps: []);
      blocTest<RecipeCubit, RecipeState>(
        "toggling edit_mode _on_ emits RecipeStateEditMode",
        build: () {
          editedRecipes = {recipe.title: editInProgress};
          return cubit;
        },
        act: (cubit) => cubit.enableEditMode(),
        expect: () => [
          RecipeStateEditModeRestored(
            recipe: recipe,
            editedRecipe: editInProgress,
            labelColors: labelColors,
          ),
        ],
      );
    });
  });

  group("saveEdits", () {
    final editedRecipe = Recipe("edited recipe", labels: [], ingredients: [], steps: []);

    setUp(() {
      cubit.emit(
        RecipeStateEditMode(
          recipe: recipe,
          editedRecipe: editedRecipe,
          labelColors: labelColors,
          completedIngredients: [stuffIngredient],
        ),
      );
    });

    group("_without_ an edit in-progress", () {
      group("_with_ whitespace", () {
        final _title = "\nedited recipe ";
        final _label = "  a_recipe";
        final _amount = "1\t";
        final _description = "thing ";
        final _step = "do stuff\n";
        final _url = " initech.net  ";
        final editedRecipeWithWhitespace = Recipe(
          _title,
          labels: [_label],
          ingredients: [Ingredient(_amount, _description)],
          steps: [_step],
          url: _url,
        );
        final editedRecipeTrimmed = Recipe(
          _title.trim(),
          labels: [_label.trim()],
          ingredients: [Ingredient(_amount.trim(), _description.trim())],
          steps: [_step.trim()],
          url: _url.trim(),
        );

        setUp(() {
          cubit.emit(
            RecipeStateEditMode(
              recipe: recipe,
              editedRecipe: editedRecipeWithWhitespace,
              labelColors: labelColors,
            ),
          );
        });

        blocTest<RecipeCubit, RecipeState>(
          "emits state",
          build: () {
            editedRecipes = {};
            when(() => storageInterface.removeFromRecipes(recipe)).thenAnswer((_) async => {});
            when(() => storageInterface.addToRecipes(editedRecipeTrimmed)).thenAnswer((_) async => {});
            return cubit;
          },
          act: (cubit) => cubit.saveEdits(),
          expect: () => [
            RecipeState(
              editedRecipeTrimmed,
              editedRecipe: null,
              labelColors: labelColors,
            ),
          ],
          verify: (_) {
            verify(() => storageInterface.removeFromRecipes(recipe)).called(1);
            verify(() => storageInterface.addToRecipes(editedRecipeTrimmed)).called(1);
            expect(editedRecipes.length, 0);
          },
        );
      });

      group("_without_ whitespace", () {
        blocTest<RecipeCubit, RecipeState>(
          "emits state",
          build: () {
            editedRecipes = {};
            when(() => storageInterface.removeFromRecipes(recipe)).thenAnswer((_) async => {});
            when(() => storageInterface.addToRecipes(editedRecipe)).thenAnswer((_) async => {});
            return cubit;
          },
          act: (cubit) => cubit.saveEdits(),
          expect: () => [
            RecipeState(
              editedRecipe,
              editedRecipe: null,
              labelColors: labelColors,
              completedIngredients: [stuffIngredient],
            ),
          ],
          verify: (_) {
            verify(() => storageInterface.removeFromRecipes(recipe)).called(1);
            verify(() => storageInterface.addToRecipes(editedRecipe)).called(1);
            expect(editedRecipes.length, 0);
          },
        );
      });
    });

    group("_with_ an edit in-progress", () {
      blocTest<RecipeCubit, RecipeState>(
        "emits state",
        build: () {
          editedRecipes = {
            recipe.title: editedRecipe,
            "_placeholder": recipe,
          };
          when(() => storageInterface.removeFromRecipes(recipe)).thenAnswer((_) async => {});
          when(() => storageInterface.addToRecipes(editedRecipe)).thenAnswer((_) async => {});
          return cubit;
        },
        act: (cubit) => cubit.saveEdits(),
        expect: () => [
          RecipeState(
            editedRecipe,
            editedRecipe: null,
            labelColors: labelColors,
            completedIngredients: [stuffIngredient],
          ),
        ],
        verify: (_) {
          verify(() => storageInterface.removeFromRecipes(recipe)).called(1);
          verify(() => storageInterface.addToRecipes(editedRecipe)).called(1);
          expect(editedRecipes.length, 1);
          expect(editedRecipes["_placeholder"], recipe);
        },
      );
    });
  });

  group("toggleIngredientCompleted", () {
    blocTest<RecipeCubit, RecipeState>(
      "toggling ingredient emits new state",
      build: () => cubit,
      act: (cubit) {
        cubit.toggleIngredientCompleted(thingsIngredient, newValue: true);
        cubit.toggleIngredientCompleted(thingsIngredient, newValue: false);
      },
      expect: () => [
        RecipeState(recipe, labelColors: labelColors, completedIngredients: [thingsIngredient]),
        RecipeState(recipe, labelColors: labelColors, completedIngredients: []),
      ],
    );
  });

  group("toggleStepCompleted", () {
    final step = "PC load letter";
    blocTest<RecipeCubit, RecipeState>(
      "toggling step emits new state",
      build: () => cubit,
      act: (cubit) {
        cubit.toggleStepCompleted(step, newValue: true);
        cubit.toggleStepCompleted(step, newValue: false);
      },
      expect: () => [
        RecipeState(recipe, labelColors: labelColors, completedSteps: [step]),
        RecipeState(recipe, labelColors: labelColors, completedSteps: []),
      ],
    );
  });

  group("reorderIngredient", () {
    setUp(() {
      cubit.emit(
        RecipeStateEditMode(
          recipe: recipe,
          editedRecipe: recipe,
          labelColors: labelColors,
        ),
      );
    });

    blocTest<RecipeCubit, RecipeState>(
      "moving _later_ in list emits new state with updated recipe",
      build: () {
        editedRecipes = {};
        return cubit;
      },
      act: (RecipeCubit cubit) => cubit.reorderIngredient(0, 2),
      expect: () {
        final _editedRecipe = Recipe(
          recipe.title,
          labels: recipe.labels,
          ingredients: [recipe.ingredients[1], recipe.ingredients[0]],
          steps: recipe.steps,
          url: recipe.url,
        );
        return [
          RecipeStateEditMode(
            recipe: recipe,
            editedRecipe: _editedRecipe,
            labelColors: labelColors,
          ),
        ];
      },
      verify: (_) {
        expect(editedRecipes.length, 1);
        expect(
          getEditedRecipe(recipe.title),
          Recipe(
            recipe.title,
            labels: recipe.labels,
            ingredients: [recipe.ingredients[1], recipe.ingredients[0]],
            steps: recipe.steps,
            url: recipe.url,
          ),
        );
      },
    );

    blocTest<RecipeCubit, RecipeState>(
      "moving _earlier_ in list emits new state with updated recipe",
      build: () => cubit,
      act: (RecipeCubit cubit) => cubit.reorderIngredient(1, 0),
      expect: () {
        final _editedRecipe = Recipe(
          recipe.title,
          labels: recipe.labels,
          ingredients: [recipe.ingredients[1], recipe.ingredients[0]],
          steps: recipe.steps,
          url: recipe.url,
        );
        return [
          RecipeStateEditMode(
            recipe: recipe,
            editedRecipe: _editedRecipe,
            labelColors: labelColors,
          ),
        ];
      },
    );
  });

  group("reorderStep", () {
    setUp(() {
      cubit.emit(
        RecipeStateEditMode(
          recipe: recipe,
          editedRecipe: recipe,
          labelColors: labelColors,
        ),
      );
    });

    blocTest<RecipeCubit, RecipeState>(
      "moving _later_ in list emits new state with updated recipe",
      build: () => cubit,
      act: (RecipeCubit cubit) => cubit.reorderStep(0, 2),
      expect: () {
        final _editedRecipe = Recipe(
          recipe.title,
          labels: recipe.labels,
          ingredients: recipe.ingredients,
          steps: [recipe.steps[1], recipe.steps[0]],
          url: recipe.url,
        );
        return [
          RecipeStateEditMode(
            recipe: recipe,
            editedRecipe: _editedRecipe,
            labelColors: labelColors,
          ),
        ];
      },
    );

    blocTest<RecipeCubit, RecipeState>(
      "moving _earlier_ in list emits new state with updated recipe",
      build: () => cubit,
      act: (RecipeCubit cubit) => cubit.reorderStep(1, 0),
      expect: () {
        final _editedRecipe = Recipe(
          recipe.title,
          labels: recipe.labels,
          ingredients: recipe.ingredients,
          steps: [recipe.steps[1], recipe.steps[0]],
          url: recipe.url,
        );
        return [
          RecipeStateEditMode(
            recipe: recipe,
            editedRecipe: _editedRecipe,
            labelColors: labelColors,
          ),
        ];
      },
      verify: (_) {
        expect(editedRecipes.length, 1);
        expect(
          getEditedRecipe(recipe.title),
          Recipe(
            recipe.title,
            labels: recipe.labels,
            ingredients: recipe.ingredients,
            steps: [recipe.steps[1], recipe.steps[0]],
            url: recipe.url,
          ),
        );
      },
    );
  });

  group("reorderLabel", () {
    setUp(() {
      cubit.emit(
        RecipeStateEditMode(
          recipe: recipe,
          editedRecipe: recipe,
          labelColors: labelColors,
        ),
      );
    });

    blocTest<RecipeCubit, RecipeState>(
      "moving _later_ in list emits new state with updated recipe",
      build: () => cubit,
      act: (RecipeCubit cubit) => cubit.reorderLabel(0, 2),
      expect: () {
        final _editedRecipe = Recipe(
          recipe.title,
          labels: [recipe.labels[1], recipe.labels[0]],
          ingredients: recipe.ingredients,
          steps: recipe.steps,
          url: recipe.url,
        );
        return [
          RecipeStateEditMode(
            recipe: recipe,
            editedRecipe: _editedRecipe,
            labelColors: labelColors,
          ),
        ];
      },
      verify: (_) {
        expect(editedRecipes.length, 1);
        expect(
          getEditedRecipe(recipe.title),
          Recipe(
            recipe.title,
            labels: [recipe.labels[1], recipe.labels[0]],
            ingredients: recipe.ingredients,
            steps: recipe.steps,
            url: recipe.url,
          ),
        );
      },
    );

    blocTest<RecipeCubit, RecipeState>(
      "moving _earlier_ in list emits new state with updated recipe",
      build: () => cubit,
      act: (RecipeCubit cubit) => cubit.reorderLabel(1, 0),
      expect: () {
        final _editedRecipe = Recipe(
          recipe.title,
          labels: [recipe.labels[1], recipe.labels[0]],
          ingredients: recipe.ingredients,
          steps: recipe.steps,
          url: recipe.url,
        );
        return [
          RecipeStateEditMode(
            recipe: recipe,
            editedRecipe: _editedRecipe,
            labelColors: labelColors,
          ),
        ];
      },
    );
  });

  group("updateUrl", () {
    setUp(() {
      cubit.emit(
        RecipeStateEditMode(
          recipe: recipe,
          editedRecipe: recipe,
          labelColors: labelColors,
        ),
      );
    });

    blocTest<RecipeCubit, RecipeState>(
      "updates url & emits new state",
      build: () => cubit,
      act: (RecipeCubit cubit) => cubit.updateUrl("initech.net"),
      expect: () {
        final _editedRecipe = Recipe(
          recipe.title,
          labels: recipe.labels,
          ingredients: recipe.ingredients,
          steps: recipe.steps,
          url: "initech.net",
        );
        return [
          RecipeStateEditMode(
            recipe: recipe,
            editedRecipe: _editedRecipe,
            labelColors: labelColors,
          ),
        ];
      },
      verify: (_) {
        expect(editedRecipes.length, 1);
        expect(
          getEditedRecipe(recipe.title),
          Recipe(
            recipe.title,
            labels: recipe.labels,
            ingredients: recipe.ingredients,
            steps: recipe.steps,
            url: "initech.net",
          ),
        );
      },
    );
  });

  group("copyUrlToClipboard", () {
    setUp(() {
      cubit.emit(
        RecipeState(recipe, labelColors: labelColors),
      );
    });

    blocTest<RecipeCubit, RecipeState>(
      "calls ClipboardInterface method",
      build: () {
        when(() => clipboardInterface.copyTextToClipboard(initechDotCom)).thenAnswer((_) async {});
        return cubit;
      },
      act: (RecipeCubit cubit) => cubit.copyUrlToClipboard(),
      verify: (_) {
        verify(() => clipboardInterface.copyTextToClipboard(initechDotCom)).called(1);
      },
    );
  });

  group("updateTitle", () {
    setUp(() {
      cubit.emit(
        RecipeStateEditMode(
          recipe: recipe,
          editedRecipe: recipe,
          labelColors: labelColors,
        ),
      );
    });

    blocTest<RecipeCubit, RecipeState>(
      "calls ClipboardInterface method",
      build: () {
        when(() => clipboardInterface.copyTextToClipboard(initechDotCom)).thenAnswer((_) async {});
        return cubit;
      },
      act: (RecipeCubit cubit) => cubit.updateTitle("15 pieces of flair"),
      expect: () {
        final _editedRecipe = Recipe(
          "15 pieces of flair",
          labels: recipe.labels,
          ingredients: recipe.ingredients,
          steps: recipe.steps,
          url: recipe.url,
        );
        return [
          RecipeStateEditMode(
            recipe: recipe,
            editedRecipe: _editedRecipe,
            labelColors: labelColors,
          ),
        ];
      },
      verify: (_) {
        expect(editedRecipes.length, 1);
        expect(
          getEditedRecipe(recipe.title),
          Recipe(
            "15 pieces of flair",
            labels: recipe.labels,
            ingredients: recipe.ingredients,
            steps: recipe.steps,
            url: recipe.url,
          ),
        );
      },
    );
  });

  group("updateIngredient", () {
    group("no completed ingredients edited", () {
      setUp(() {
        cubit.emit(
          RecipeStateEditMode(
            recipe: recipe,
            editedRecipe: recipe,
            labelColors: labelColors,
          ),
        );
      });

      blocTest<RecipeCubit, RecipeState>(
        "updates recipe & emits state",
        build: () => cubit,
        act: (RecipeCubit cubit) => cubit.updateIngredient(Ingredient("PC", "load letter"), ingredientIndex: 0),
        expect: () {
          final _editedRecipe = Recipe(
            recipe.title,
            labels: recipe.labels,
            ingredients: [Ingredient("PC", "load letter"), recipe.ingredients[1]],
            steps: recipe.steps,
            url: recipe.url,
          );
          return [
            RecipeStateEditMode(
              recipe: recipe,
              editedRecipe: _editedRecipe,
              labelColors: labelColors,
            ),
          ];
        },
        verify: (_) {
          expect(editedRecipes.length, 1);
          expect(
            getEditedRecipe(recipe.title),
            Recipe(
              recipe.title,
              labels: recipe.labels,
              ingredients: [Ingredient("PC", "load letter"), recipe.ingredients[1]],
              steps: recipe.steps,
              url: recipe.url,
            ),
          );
        },
      );
    });

    group("1 completed ingredient edited", () {
      setUp(() {
        cubit.emit(
          RecipeStateEditMode(
            recipe: recipe,
            editedRecipe: recipe,
            labelColors: labelColors,
            completedIngredients: [recipe.ingredients[0]],
          ),
        );
      });

      blocTest<RecipeCubit, RecipeState>(
        "updates recipe & emits state",
        build: () => cubit,
        act: (RecipeCubit cubit) => cubit.updateIngredient(Ingredient("PC", "load letter"), ingredientIndex: 0),
        expect: () {
          final _editedRecipe = Recipe(
            recipe.title,
            labels: recipe.labels,
            ingredients: [Ingredient("PC", "load letter"), recipe.ingredients[1]],
            steps: recipe.steps,
            url: recipe.url,
          );
          return [
            RecipeStateEditMode(
              recipe: recipe,
              editedRecipe: _editedRecipe,
              labelColors: labelColors,
              completedIngredients: [Ingredient("PC", "load letter")],
            ),
          ];
        },
      );
    });
  });

  group("updateStep", () {
    group("no completed steps edited", () {
      setUp(() {
        cubit.emit(
          RecipeStateEditMode(
            recipe: recipe,
            editedRecipe: recipe,
            labelColors: labelColors,
          ),
        );
      });

      blocTest<RecipeCubit, RecipeState>(
        "updates recipe & emits state",
        build: () => cubit,
        act: (RecipeCubit cubit) => cubit.updateStep("PC load letter", stepIndex: 0),
        expect: () {
          final _editedRecipe = Recipe(
            recipe.title,
            labels: recipe.labels,
            ingredients: recipe.ingredients,
            steps: ["PC load letter", recipe.steps[1]],
            url: recipe.url,
          );
          return [
            RecipeStateEditMode(
              recipe: recipe,
              editedRecipe: _editedRecipe,
              labelColors: labelColors,
            ),
          ];
        },
        verify: (_) {
          expect(editedRecipes.length, 1);
          expect(
            getEditedRecipe(recipe.title),
            Recipe(
              recipe.title,
              labels: recipe.labels,
              ingredients: recipe.ingredients,
              steps: ["PC load letter", recipe.steps[1]],
              url: recipe.url,
            ),
          );
        },
      );
    });

    group("1 completed step edited", () {
      setUp(() {
        cubit.emit(
          RecipeStateEditMode(
            recipe: recipe,
            editedRecipe: recipe,
            labelColors: labelColors,
            completedSteps: [recipe.steps[0]],
          ),
        );
      });

      blocTest<RecipeCubit, RecipeState>(
        "updates recipe & emits state",
        build: () => cubit,
        act: (RecipeCubit cubit) => cubit.updateStep("PC load letter", stepIndex: 0),
        expect: () {
          final _editedRecipe = Recipe(
            recipe.title,
            labels: recipe.labels,
            ingredients: recipe.ingredients,
            steps: ["PC load letter", recipe.steps[1]],
            url: recipe.url,
          );
          return [
            RecipeStateEditMode(
              recipe: recipe,
              editedRecipe: _editedRecipe,
              labelColors: labelColors,
              completedSteps: ["PC load letter"],
            ),
          ];
        },
      );
    });
  });

  group("updateLabelColor", () {
    setUp(() {
      cubit.emit(
        RecipeStateEditMode(
          recipe: recipe,
          editedRecipe: recipe,
          labelColors: labelColors,
        ),
      );
    });

    blocTest<RecipeCubit, RecipeState>(
      "updates color and emits new state",
      build: () {
        when(storageInterface.getLabelColors).thenReturn([LabelColor("load letter", Colors.amber)]);
        when(() => storageInterface.storeLabelColors(any())).thenAnswer((_) async {});
        return cubit;
      },
      act: (RecipeCubit cubit) => cubit.updateLabelColor("load letter", Colors.cyan.value),
      expect: () {
        return [
          RecipeStateEditMode(
            recipe: recipe,
            editedRecipe: recipe,
            labelColors: [LabelColor("load letter", Colors.cyan)],
          ),
        ];
      },
      verify: (_) {
        verify(storageInterface.getLabelColors).called(1);
        verify(() => storageInterface.storeLabelColors([LabelColor("load letter", Colors.cyan)])).called(1);
      },
    );
  });

  group("updateLabelText", () {
    setUp(() {
      cubit.emit(
        RecipeStateEditMode(
          recipe: recipe,
          editedRecipe: recipe,
          labelColors: labelColors,
        ),
      );
    });

    blocTest<RecipeCubit, RecipeState>(
      "updates label and emits new state",
      build: () {
        when(() => clipboardInterface.copyTextToClipboard(initechDotCom)).thenAnswer((_) async {});
        return cubit;
      },
      act: (RecipeCubit cubit) => cubit.updateLabelText("stapler", labelAtIndex: 1),
      expect: () {
        final _editedRecipe = Recipe(
          recipe.title,
          labels: [recipe.labels[0], "stapler"],
          ingredients: recipe.ingredients,
          steps: recipe.steps,
          url: recipe.url,
        );
        return [
          RecipeStateEditMode(
            recipe: recipe,
            editedRecipe: _editedRecipe,
            labelColors: [LabelColor("load letter", Colors.red), LabelColor("stapler", Colors.red)],
          ),
        ];
      },
      verify: (_) {
        expect(editedRecipes.length, 1);
        expect(
          getEditedRecipe(recipe.title),
          Recipe(
            recipe.title,
            labels: [recipe.labels[0], "stapler"],
            ingredients: recipe.ingredients,
            steps: recipe.steps,
            url: recipe.url,
          ),
        );
      },
    );
  });

  group("addIngredient", () {
    setUp(() {
      cubit.emit(
        RecipeStateEditMode(
          recipe: recipe,
          editedRecipe: recipe,
          labelColors: labelColors,
        ),
      );
    });

    blocTest<RecipeCubit, RecipeState>(
      "emits new state",
      build: () => cubit,
      act: (cubit) => cubit.addIngredient(),
      expect: () {
        return [
          RecipeStateEditModeAddIngredient(
            recipe: recipe,
            editedRecipe: recipe,
            labelColors: labelColors,
          ),
        ];
      },
      verify: (_) {
        expect(cubit.state.editedRecipe!.ingredients.last, Ingredient.empty);
      },
    );
  });

  group("addStep", () {
    setUp(() {
      cubit.emit(
        RecipeStateEditMode(
          recipe: recipe,
          editedRecipe: recipe,
          labelColors: labelColors,
        ),
      );
    });

    blocTest<RecipeCubit, RecipeState>(
      "emits new state",
      build: () => cubit,
      act: (cubit) => cubit.addStep(),
      expect: () {
        return [
          RecipeStateEditModeAddStep(
            recipe: recipe,
            editedRecipe: recipe,
            labelColors: labelColors,
          ),
        ];
      },
      verify: (_) {
        expect(cubit.state.editedRecipe!.steps.last, "");
      },
    );
  });

  group("addLabel", () {
    setUp(() {
      cubit.emit(
        RecipeStateEditMode(
          recipe: recipe,
          editedRecipe: recipe,
          labelColors: labelColors,
        ),
      );
    });

    blocTest<RecipeCubit, RecipeState>(
      "emits new state",
      build: () => cubit,
      act: (cubit) => cubit.addLabel(),
      expect: () {
        return [
          RecipeStateEditModeAddLabel(
            recipe: recipe,
            editedRecipe: recipe,
            labelColors: labelColors,
          ),
        ];
      },
      verify: (_) {
        expect(cubit.state.editedRecipe!.labels.last, "");
      },
    );
  });

  group("deleteLabel", () {
    setUp(() {
      cubit.emit(
        RecipeStateEditMode(
          recipe: recipe,
          editedRecipe: recipe,
          labelColors: labelColors,
        ),
      );
    });

    blocTest<RecipeCubit, RecipeState>(
      "emits new state",
      build: () => cubit,
      act: (cubit) => cubit.deleteLabel(1),
      expect: () {
        return [
          RecipeStateEditModeDeletedLabel(
            recipe: recipe,
            editedRecipe: Recipe(
              "TPS",
              labels: ["PC"],
              ingredients: [stuffIngredient, thingsIngredient],
              steps: [step0, step1],
              url: initechDotCom,
            ),
            labelColors: [],
          ),
        ];
      },
    );
  });

  group("startFromScratch", () {
    blocTest<RecipeCubit, RecipeState>(
      "emits new state",
      build: () => cubit,
      act: (cubit) => cubit.startFromScratch(),
      expect: () {
        return [
          RecipeStateEditMode(
            recipe: Recipe.blank,
            editedRecipe: Recipe("", labels: [""], ingredients: [Ingredient.empty], steps: [""]),
            labelColors: labelColors,
          ),
        ];
      },
    );
  });

  group("pasteFromClipboard", () {
    final _tpsResponse = Response("TPS", 200);

    group("clipboard has some text to paste", () {
      group("_successful_ in parsing URI from text", () {
        final _text = "PC load letter";
        final _initechDotNet = Uri.parse("https://initech.net");
        final _recipe = Recipe(
          _tpsResponse.body,
          labels: [],
          ingredients: [],
          steps: [],
        );

        group("http-get is successful", () {
          group("_can_ parse recipe from response-body (returns recipe)", () {
            blocTest<RecipeCubit, RecipeState>(
              "emits states & calls interface-methods",
              build: () {
                when(clipboardInterface.getCopiedText).thenAnswer((_) async => _text);
                when(() => webInterface.uriFromText(_text)).thenReturn(_initechDotNet);
                when(() => webInterface.httpGet(_initechDotNet)).thenAnswer((_) async => _tpsResponse);
                when(() => webInterface.parseRecipeFromHttpResponseBody(_tpsResponse.body))
                    .thenAnswer((_) async => _recipe);
                return cubit;
              },
              act: (cubit) => cubit.pasteFromClipboard(),
              expect: () {
                return [
                  RecipeStateProcessingClipboardData(
                    labelColors: labelColors,
                  ),
                  RecipeStateEditMode(
                    recipe: Recipe.blank,
                    editedRecipe: _recipe,
                    labelColors: labelColors,
                  ),
                ];
              },
              verify: (_) {
                verify(clipboardInterface.getCopiedText).called(1);
                verify(() => webInterface.uriFromText(_text)).called(1);
                verify(() => webInterface.httpGet(_initechDotNet)).called(1);
                verify(() => webInterface.parseRecipeFromHttpResponseBody(_tpsResponse.body)).called(1);
              },
            );
          });

          group("_cannot_ parse recipe from response-body (returns null)", () {
            blocTest<RecipeCubit, RecipeState>(
              "emits states & calls interface-methods",
              build: () {
                when(clipboardInterface.getCopiedText).thenAnswer((_) async => _text);
                when(() => webInterface.uriFromText(_text)).thenReturn(_initechDotNet);
                when(() => webInterface.httpGet(_initechDotNet)).thenAnswer((_) async => _tpsResponse);
                when(() => webInterface.parseRecipeFromHttpResponseBody(_tpsResponse.body))
                    .thenAnswer((_) async => null);
                return cubit;
              },
              act: (cubit) async => await cubit.pasteFromClipboard(),
              expect: () {
                return [
                  RecipeStateProcessingClipboardData(
                    labelColors: labelColors,
                  ),
                  RecipeStateUnableToParseRecipeFromHttpResponseBodyError(
                    labelColors: labelColors,
                  ),
                ];
              },
              verify: (_) {
                verify(clipboardInterface.getCopiedText).called(1);
                verify(() => webInterface.uriFromText(_text)).called(1);
                verify(() => webInterface.httpGet(_initechDotNet)).called(1);
                verify(() => webInterface.parseRecipeFromHttpResponseBody(_tpsResponse.body)).called(1);
              },
            );
          });
        });

        group("http-get is _unsuccessful_", () {
          blocTest<RecipeCubit, RecipeState>(
            "emits states & calls interface-methods",
            build: () {
              when(clipboardInterface.getCopiedText).thenAnswer((_) async => _text);
              when(() => webInterface.uriFromText(_text)).thenReturn(_initechDotNet);
              when(() => webInterface.httpGet(_initechDotNet)).thenThrow(HttpException("PC load letter"));
              return cubit;
            },
            act: (cubit) => cubit.pasteFromClipboard(),
            expect: () {
              return [
                RecipeStateProcessingClipboardData(
                  labelColors: labelColors,
                ),
                RecipeStateFailedHttpGetRequest(
                  labelColors: labelColors,
                  failure: "HttpException: PC load letter",
                ),
              ];
            },
            verify: (_) {
              verify(clipboardInterface.getCopiedText).called(1);
              verify(() => webInterface.uriFromText(_text)).called(1);
              verify(() => webInterface.httpGet(_initechDotNet)).called(1);
              verifyNever(() => webInterface.parseRecipeFromHttpResponseBody(any()));
            },
          );
        });
      });

      group("text _is not_ a URI (the mock says so)", () {
        setUpAll(() {
          registerFallbackValue(Uri.parse("http://initech.net"));
        });

        group("text _can_ be parsed for a recipe", () {
          final _recipe =
              "shortbread\n\nLabels:\nsweet\n\nIngredients:\nflour\nsugar\netc\n\nSteps:\nmix\nbake\n\nWeb-link:\nnull";

          blocTest<RecipeCubit, RecipeState>(
            "emits states & calls interface-methods",
            build: () {
              when(clipboardInterface.getCopiedText).thenAnswer((_) async => _recipe);
              return cubit;
            },
            act: (cubit) => cubit.pasteFromClipboard(),
            expect: () {
              return [
                RecipeStateProcessingClipboardData(
                  labelColors: labelColors,
                ),
                RecipeStateEditMode(
                  recipe: Recipe.blank,
                  editedRecipe: Recipe.fromString(_recipe),
                  labelColors: labelColors,
                ),
              ];
            },
            verify: (_) {
              verify(clipboardInterface.getCopiedText).called(1);
              verify(() => webInterface.uriFromText(_recipe)).called(1);
              verifyNever(() => webInterface.httpGet(any()));
              verifyNever(() => webInterface.parseRecipeFromHttpResponseBody(any()));
            },
          );
        });

        group("text _cannot_ be parsed for a recipe", () {
          final _text = "PC load letter";

          blocTest<RecipeCubit, RecipeState>(
            "emits states & calls interface-methods",
            build: () {
              when(clipboardInterface.getCopiedText).thenAnswer((_) async => _text);
              return cubit;
            },
            act: (cubit) => cubit.pasteFromClipboard(),
            expect: () {
              return [
                RecipeStateProcessingClipboardData(
                  labelColors: labelColors,
                ),
                RecipeStateUnableToParseRecipeFromClipboardText(
                  labelColors: labelColors,
                ),
              ];
            },
            verify: (_) {
              verify(clipboardInterface.getCopiedText).called(1);
              verify(() => webInterface.uriFromText(_text)).called(1);
              verifyNever(() => webInterface.httpGet(any()));
              verifyNever(() => webInterface.parseRecipeFromHttpResponseBody(any()));
            },
          );
        });
      });
    });

    group("clipboard has no text to paste -- `null`", () {
      blocTest<RecipeCubit, RecipeState>(
        "emits states & calls interface-methods",
        build: () {
          when(clipboardInterface.getCopiedText).thenAnswer((_) async => null);
          when(() => webInterface.httpGet(any())).thenAnswer((_) async => _tpsResponse);
          return cubit;
        },
        act: (cubit) => cubit.pasteFromClipboard(),
        expect: () {
          return [
            RecipeStateProcessingClipboardData(
              labelColors: labelColors,
            ),
            RecipeStateNoClipboardDataToPaste(
              labelColors: labelColors,
            ),
          ];
        },
        verify: (_) {
          verify(clipboardInterface.getCopiedText).called(1);
          verifyNever(() => webInterface.uriFromText(any()));
          verifyNever(() => webInterface.httpGet(any()));
          verifyNever(() => webInterface.parseRecipeFromHttpResponseBody(any()));
        },
      );
    });
  });

  group('getMatchingLabels', () {
    final _labelColors = [
      LabelColor("PC", Colors.red),
      LabelColor("load", Colors.lime),
      LabelColor("letter", Colors.cyan),
    ];
    late final Iterable<String> matches;

    blocTest<RecipeCubit, RecipeState>(
      "calls interface-methods & returns subset of labels",
      build: () {
        when(storageInterface.getLabelColors).thenReturn(_labelColors);
        return cubit;
      },
      act: (cubit) => matches = cubit.getMatchingLabels("L"),
      expect: () => [],
      verify: (_) {
        verify(storageInterface.getLabelColors).called(1);
        expect(matches, ["load", "letter"]);
      },
    );
  });
}
