import 'package:captainfoodmaker/data_models/recipe.dart';
import 'package:captainfoodmaker/recipe/recipe_cubit.dart';
import 'package:flutter_test/flutter_test.dart';

// ignore_for_file: prefer_const_constructors
// ignore_for_file: prefer_const_declarations
// ignore_for_file: prefer_const_literals_to_create_immutables

void main() {
  test("RecipeStateInitial auto-removes empty/blank elements (from possible editing)", () {
    final state = RecipeStateInitial(
      Recipe(
        "Chotchkie's",
        labels: ["", "has coffee"],
        ingredients: [Ingredient("15 pieces", "flair"), Ingredient.empty],
        steps: ["  ", "order extreme fajitas", "\t\n\r"],
        url: "chotchkies.net",
      ),
      labelColors: [],
    );

    expect(state.recipe.labels.length, 1);
    expect(state.recipe.labels[0], "has coffee");

    expect(state.recipe.ingredients.length, 1);
    expect(state.recipe.ingredients[0], Ingredient("15 pieces", "flair"));

    expect(state.recipe.steps.length, 1);
    expect(state.recipe.steps[0], "order extreme fajitas");
  });
}
