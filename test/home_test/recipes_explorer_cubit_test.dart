import 'package:bloc_test/bloc_test.dart';
import 'package:captainfoodmaker/data_models/label_colors.dart';
import 'package:captainfoodmaker/data_models/recipe.dart';
import 'package:captainfoodmaker/home/recipes_explorer_cubit.dart';
import 'package:captainfoodmaker/interfaces/hive_storage_interface.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

// ignore_for_file: prefer_const_constructors
// ignore_for_file: prefer_const_declarations
// ignore_for_file: prefer_const_literals_to_create_immutables

class MockHiveStorageInterface extends Mock implements HiveStorageInterface {}

void main() {
  group('RecipesExplorerCubit', () {
    late HiveStorageInterface hiveStorageInterface;
    late RecipesExplorerCubit cubit;
    final recipes = [
      Recipe("PC", labels: [], ingredients: [], steps: []),
      Recipe("load letter", labels: [], ingredients: [], steps: []),
    ];
    final labelColors = [
      LabelColor("TPS", Colors.red),
      LabelColor("report", Colors.cyan),
    ];

    setUp(() {
      hiveStorageInterface = MockHiveStorageInterface();
      when(() => hiveStorageInterface.getRecipes(excludeDeletedRecipes: true)).thenReturn(recipes);
      when(hiveStorageInterface.getLabelColors).thenReturn(labelColors);
      cubit = RecipesExplorerCubit(hiveStorageInterface);

      // called every time cubit is set-up -- verified here to avoid confusion in tests
      verifyNever(hiveStorageInterface.getRecipes);
      verify(() => hiveStorageInterface.getRecipes(excludeDeletedRecipes: true)).called(1);
      verify(hiveStorageInterface.getLabelColors).called(1);
    });

    tearDown(() {
      cubit.close();
    });

    group('initial state', () {
      blocTest<RecipesExplorerCubit, RecipesExplorerState>(
        'no actions',
        build: () => cubit,
        expect: () => [],
        verify: (_) {
          expect(cubit.state.recipes, recipes);
          expect(cubit.state.labels, labelColors.labels);
          expect(cubit.state.labelColors, labelColors);
          expect(cubit.state.searchText, null);
          expect(
            cubit.state.toString(),
            RecipesExplorerState(
              recipes: recipes,
              labelColors: labelColors,
              searchText: null,
            ).toString(),
          );
          expect(
            cubit.state.props,
            [
              Recipe("PC", labels: [], ingredients: [], steps: []),
              Recipe("load letter", labels: [], ingredients: [], steps: []),
              LabelColor("TPS", Colors.red),
              LabelColor("report", Colors.cyan),
              null,
            ],
          );
        },
      );
    });

    group('search', () {
      group('enableSearch', () {
        blocTest<RecipesExplorerCubit, RecipesExplorerState>(
          "calls getsRecipes and emits them with blank searchText",
          build: () => cubit,
          act: (cubit) => cubit.enableSearch(),
          expect: () => [
            RecipesExplorerState(
              recipes: recipes,
              labelColors: labelColors,
              searchText: "",
            ),
          ],
        );
      });

      group("disableSearch", () {
        final recipes = [
          Recipe("PC", labels: [], ingredients: [], steps: []),
          Recipe("load letter", labels: [], ingredients: [], steps: []),
        ];

        blocTest<RecipesExplorerCubit, RecipesExplorerState>(
          "calls getsRecipes and emits them with `null` searchText",
          build: () {
            when(hiveStorageInterface.getRecipes).thenReturn(recipes);
            return cubit;
          },
          act: (cubit) => cubit.disableSearch(),
          expect: () => [
            RecipesExplorerState(
              recipes: recipes,
              labelColors: labelColors,
              searchText: null,
            ),
          ],
          verify: (_) {
            verify(() => hiveStorageInterface.getRecipes(excludeDeletedRecipes: true)).called(1);
          },
        );
      });

      group("updateSearch", () {
        blocTest<RecipesExplorerCubit, RecipesExplorerState>(
          "calls getsRecipes and emits them with updated searchText",
          build: () {
            when(hiveStorageInterface.getRecipes).thenReturn([
              Recipe("PC", labels: [], ingredients: [], steps: []),
              Recipe("load letter", labels: [], ingredients: [], steps: []),
            ]);
            return cubit;
          },
          act: (cubit) => cubit.updateSearch("PC"),
          expect: () => [
            RecipesExplorerState(
              recipes: [Recipe("PC", labels: [], ingredients: [], steps: [])],
              labelColors: labelColors,
              searchText: "PC", // upper-case
            ),
          ],
          verify: (_) {
            verify(() => hiveStorageInterface.getRecipes(excludeDeletedRecipes: true)).called(1);
          },
        );

        blocTest<RecipesExplorerCubit, RecipesExplorerState>(
          "searchText ignores case when searching lower for upper recipe-title",
          build: () {
            when(hiveStorageInterface.getRecipes).thenReturn([
              Recipe("PC", labels: [], ingredients: [], steps: []),
              Recipe("load letter", labels: [], ingredients: [], steps: []),
            ]);
            return cubit;
          },
          act: (cubit) => cubit.updateSearch("pc"),
          expect: () => [
            RecipesExplorerState(
              recipes: [Recipe("PC", labels: [], ingredients: [], steps: [])],
              labelColors: labelColors,
              searchText: "pc", // lower-case
            ),
          ],
          verify: (_) {
            verify(() => hiveStorageInterface.getRecipes(excludeDeletedRecipes: true)).called(1);
          },
        );

        blocTest<RecipesExplorerCubit, RecipesExplorerState>(
          "searchText ignores case when searching upper for lower recipe-title",
          build: () {
            when(hiveStorageInterface.getRecipes).thenReturn([
              Recipe("PC", labels: [], ingredients: [], steps: []),
              Recipe("load letter", labels: [], ingredients: [], steps: []),
            ]);
            return cubit;
          },
          act: (cubit) => cubit.updateSearch("OAD"),
          expect: () => [
            RecipesExplorerState(
              recipes: [Recipe("load letter", labels: [], ingredients: [], steps: [])],
              labelColors: labelColors,
              searchText: "OAD",
            ),
          ],
          verify: (_) {
            verify(() => hiveStorageInterface.getRecipes(excludeDeletedRecipes: true)).called(1);
          },
        );
      });
    });

    group('updateWithFreshLabelsAndColors', () {
      group('when labels were reordered', () {
        blocTest<RecipesExplorerCubit, RecipesExplorerState>(
          'calls interface-method & emits state',
          build: () {
            when(hiveStorageInterface.getLabelColors).thenReturn(cubit.state.labelColors);
            return cubit;
          },
          act: (cubit) => cubit.updateWithFreshLabelsAndColors(),
          expect: () => [
            RecipesExplorerState(
              recipes: [
                Recipe("PC", labels: [], ingredients: [], steps: []),
                Recipe("load letter", labels: [], ingredients: [], steps: []),
              ],
              labelColors: labelColors,
              searchText: null,
            ),
          ],
          verify: (_) {
            verify(hiveStorageInterface.getLabelColors).called(1);
          },
        );
      });

      group('when label-colors were changed', () {
        final newLabelColors = [LabelColor("TPS", Colors.lime)];
        blocTest<RecipesExplorerCubit, RecipesExplorerState>(
          'calls interface-method & emits state',
          build: () {
            when(hiveStorageInterface.getLabelColors).thenReturn(newLabelColors);
            return cubit;
          },
          act: (cubit) => cubit.updateWithFreshLabelsAndColors(),
          expect: () => [
            RecipesExplorerState(
              recipes: [
                Recipe("PC", labels: [], ingredients: [], steps: []),
                Recipe("load letter", labels: [], ingredients: [], steps: []),
              ],
              labelColors: newLabelColors,
              searchText: null,
            ),
          ],
          verify: (_) {
            verify(hiveStorageInterface.getLabelColors).called(1);
          },
        );
      });
    });

    group('updateWithFreshLabelsAndColors', () {
      group('when labels were reordered', () {
        blocTest<RecipesExplorerCubit, RecipesExplorerState>(
          'calls interface-method & emits state',
          build: () {
            when(hiveStorageInterface.getLabelColors).thenReturn(cubit.state.labelColors);
            return cubit;
          },
          act: (cubit) => cubit.updateWithFreshLabelsAndColors(),
          expect: () => [
            RecipesExplorerState(
              recipes: [
                Recipe("PC", labels: [], ingredients: [], steps: []),
                Recipe("load letter", labels: [], ingredients: [], steps: []),
              ],
              labelColors: labelColors,
              searchText: null,
            ),
          ],
          verify: (_) {
            verify(hiveStorageInterface.getLabelColors).called(1);
          },
        );
      });
    });

    group("updateIfRecipesWereUpdated", () {
      late bool result;
      group("when state matches storage", () {
        blocTest<RecipesExplorerCubit, RecipesExplorerState>(
          "does not emit new state; returns false",
          build: () {
            when(hiveStorageInterface.getRecipes).thenReturn(recipes);
            when(hiveStorageInterface.getLabelColors).thenReturn([]);
            return cubit;
          },
          act: (cubit) => result = cubit.updateIfRecipesWereUpdated(),
          expect: () => [],
          verify: (_) {
            verify(() => hiveStorageInterface.getRecipes(excludeDeletedRecipes: true)).called(1);
            verifyNever(hiveStorageInterface.getLabelColors);
            expect(result, false);
          },
        );
      });

      group("when state does not match storage", () {
        final differentRecipes = [
          Recipe("Chotchkie's", labels: [], ingredients: [], steps: []),
          Recipe("initech", labels: [], ingredients: [], steps: []),
        ];
        final labelColors = [LabelColor("flair", Colors.blue)];
        blocTest<RecipesExplorerCubit, RecipesExplorerState>(
          "emits new state; returns true",
          build: () {
            when(() => hiveStorageInterface.getRecipes(excludeDeletedRecipes: true)).thenReturn(differentRecipes);
            when(hiveStorageInterface.getLabelColors).thenReturn(labelColors);
            return cubit;
          },
          act: (cubit) => result = cubit.updateIfRecipesWereUpdated(),
          expect: () => [
            RecipesExplorerState(
              recipes: differentRecipes,
              labelColors: labelColors,
              searchText: null,
            ),
          ],
          verify: (_) {
            verify(() => hiveStorageInterface.getRecipes(excludeDeletedRecipes: true)).called(1);
            verify(hiveStorageInterface.getLabelColors).called(1);
            expect(result, true);
          },
        );
      });
    });

    group("navigateToDeletedRecipesScreen", () {
      blocTest<RecipesExplorerCubit, RecipesExplorerState>(
        "emits new nav-state",
        build: () => cubit,
        act: (cubit) => cubit.navigateToDeletedRecipesScreen(),
        expect: () => [
          RecipesExplorerStateNavigateToDeletedRecipesScreen.fromState(cubit.state),
        ],
      );
    });

    group("navigateToRecipeScreenToAddNewRecipe", () {
      blocTest<RecipesExplorerCubit, RecipesExplorerState>(
        "emits new nav-state",
        build: () => cubit,
        act: (cubit) => cubit.navigateToRecipeScreenToAddNewRecipe(),
        expect: () => [
          RecipesExplorerStateNavigateToScreenAddNewRecipe.fromState(cubit.state),
        ],
      );
    });
  });
}
