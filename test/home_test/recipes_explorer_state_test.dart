import 'package:captainfoodmaker/data_models/label_colors.dart';
import 'package:captainfoodmaker/data_models/recipe.dart';
import 'package:captainfoodmaker/home/recipes_explorer_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

// ignore_for_file: prefer_const_constructors
// ignore_for_file: prefer_const_declarations
// ignore_for_file: prefer_const_literals_to_create_immutables

void main() {
  group('RecipeExplorerState', () {
    group('on init', () {
      test('has attributes', () {
        final state = RecipesExplorerState(
          recipes: [],
          labelColors: [],
          searchText: null,
        );
        expect(state.recipes, []);
        expect(state.labels, []);
        expect(state.labelColors, []);
        expect(state.searchText, null);
        expect(state.props, [null]);
      });

      test('has a few recipes', () {
        final state = RecipesExplorerState(
          recipes: [
            Recipe(
              "recipe_0",
              labels: ["label_0", "label_1", "label_2"],
              ingredients: [Ingredient("2", "things")],
              steps: ["do stuff", "do more stuff"],
            ),
            Recipe(
              "recipe_1",
              labels: ["label_3", "label_4", "label_5"],
              ingredients: [Ingredient("2", "things")],
              steps: ["do stuff", "do more stuff"],
            ),
            Recipe(
              "recipe_2",
              labels: ["label_6", "label_7", "label_8"],
              ingredients: [Ingredient("2", "things")],
              steps: ["do stuff", "do more stuff"],
            ),
          ],
          labelColors: [LabelColor("TPS", Colors.lime), LabelColor("reports", Colors.purple)],
          searchText: null,
        );
        final expectedRecipes = [
          Recipe(
            "recipe_0",
            labels: ["label_0", "label_1", "label_2"],
            ingredients: [Ingredient("2", "things")],
            steps: ["do stuff", "do more stuff"],
          ),
          Recipe(
            "recipe_1",
            labels: ["label_3", "label_4", "label_5"],
            ingredients: [Ingredient("2", "things")],
            steps: ["do stuff", "do more stuff"],
          ),
          Recipe(
            "recipe_2",
            labels: ["label_6", "label_7", "label_8"],
            ingredients: [Ingredient("2", "things")],
            steps: ["do stuff", "do more stuff"],
          ),
        ];
        final expectedLabelColors = [LabelColor("TPS", Colors.lime), LabelColor("reports", Colors.purple)];
        final expectedSearchText = null;
        expect(state.recipes, expectedRecipes);
        expect(state.labels, expectedLabelColors.labels);
        expect(state.labelsIndexesPlus1, Iterable<int>.generate(expectedLabelColors.labels.length + 1));
        expect(state.labelColors, expectedLabelColors);
        expect(state.searchText, expectedSearchText);
        expect(
          state.props,
          [
            expectedRecipes[0],
            expectedRecipes[1],
            expectedRecipes[2],
            expectedLabelColors[0],
            expectedLabelColors[1],
            expectedSearchText,
          ],
        );
      });
    });

    group('equality', () {
      test('when empty', () {
        expect(
          RecipesExplorerState(
            recipes: [],
            labelColors: [],
            searchText: null,
          ),
          RecipesExplorerState(
            recipes: [],
            labelColors: [],
            searchText: null,
          ),
        );
      });

      test('no recipes, different searchText', () {
        expect(
          RecipesExplorerState(
            recipes: [],
            labelColors: [],
            searchText: "PC load letter",
          ),
          isNot(
            RecipesExplorerState(
              recipes: [],
              labelColors: [],
              searchText: null,
            ),
          ),
        );

        expect(
          RecipesExplorerState(
            recipes: [],
            labelColors: [],
            searchText: "PC",
          ),
          isNot(
            RecipesExplorerState(
              recipes: [],
              labelColors: [],
              searchText: "load letter",
            ),
          ),
        );
      });

      test('no recipes, different labels', () {
        expect(
          RecipesExplorerState(
            recipes: [],
            labelColors: [LabelColor("TPS", Colors.red)],
            searchText: null,
          ),
          isNot(
            RecipesExplorerState(
              recipes: [],
              labelColors: [],
              searchText: null,
            ),
          ),
        );
      });

      test('1 recipe with 1 label & with `null` searchText', () {
        final actual = RecipesExplorerState(
          recipes: [
            Recipe(
              "this recipe",
              labels: ["a label"],
              ingredients: [Ingredient("2", "things")],
              steps: ["do stuff", "do more stuff"],
            ),
          ],
          labelColors: [LabelColor("TPS", Colors.red)],
          searchText: null,
        );
        final expected = RecipesExplorerState(
          recipes: [
            Recipe(
              "this recipe",
              labels: ["a label"],
              ingredients: [Ingredient("2", "things")],
              steps: ["do stuff", "do more stuff"],
            ),
          ],
          labelColors: [LabelColor("TPS", Colors.red)],
          searchText: null,
        );
        expect(actual, expected);
        expect(actual.toString(), expected.toString());
      });

      test('many recipes with many labels & with searchText', () {
        final actual = RecipesExplorerState(
          recipes: [
            Recipe(
              "recipe_0",
              labels: ["label_0", "label_1", "label_2"],
              ingredients: [Ingredient("2", "things")],
              steps: ["do stuff", "do more stuff"],
            ),
            Recipe(
              "recipe_1",
              labels: ["label_3", "label_4", "label_5"],
              ingredients: [Ingredient("2", "things")],
              steps: ["do stuff", "do more stuff"],
            ),
            Recipe(
              "recipe_2",
              labels: ["label_6", "label_7", "label_8"],
              ingredients: [Ingredient("2", "things")],
              steps: ["do stuff", "do more stuff"],
            ),
          ],
          labelColors: [LabelColor("TPS", Colors.red)],
          searchText: "PC load letter",
        );
        final expected = RecipesExplorerState(
          recipes: [
            Recipe(
              "recipe_0",
              labels: ["label_0", "label_1", "label_2"],
              ingredients: [Ingredient("2", "things")],
              steps: ["do stuff", "do more stuff"],
            ),
            Recipe(
              "recipe_1",
              labels: ["label_3", "label_4", "label_5"],
              ingredients: [Ingredient("2", "things")],
              steps: ["do stuff", "do more stuff"],
            ),
            Recipe(
              "recipe_2",
              labels: ["label_6", "label_7", "label_8"],
              ingredients: [Ingredient("2", "things")],
              steps: ["do stuff", "do more stuff"],
            ),
          ],
          labelColors: [LabelColor("TPS", Colors.red)],
          searchText: "PC load letter",
        );
        expect(actual, expected);
        expect(actual.toString(), expected.toString());
      });
    });
  });
}
