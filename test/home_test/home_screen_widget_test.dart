import 'package:bloc_test/bloc_test.dart';
import 'package:captainfoodmaker/data_models/label_colors.dart';
import 'package:captainfoodmaker/data_models/recipe.dart';
import 'package:captainfoodmaker/deleted_recipes/deleted_recipes_screen.dart';
import 'package:captainfoodmaker/edit_labels/edit_labels_screen.dart';
import 'package:captainfoodmaker/export/export_recipes_screen.dart';
import 'package:captainfoodmaker/home/home_screen.dart';
import 'package:captainfoodmaker/home/recipes_explorer_cubit.dart';
import 'package:captainfoodmaker/interfaces/clipboard_interface.dart';
import 'package:captainfoodmaker/interfaces/hive_storage_interface.dart';
import 'package:captainfoodmaker/interfaces/web_interface.dart';
import 'package:captainfoodmaker/recipe/recipe_cubit.dart';
import 'package:captainfoodmaker/recipe/recipe_screen.dart';
import 'package:captainfoodmaker/shared_widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

// ignore_for_file: prefer_const_constructors
// ignore_for_file: prefer_const_declarations
// ignore_for_file: prefer_const_literals_to_create_immutables

class MockRecipesExplorerCubit extends MockCubit<RecipesExplorerState> implements RecipesExplorerCubit {}

class MockRecipeCubit extends MockCubit<RecipeState> implements RecipeCubit {}

class MockClipboardInterface extends Mock implements ClipboardInterface {}

class MockHiveStorageInterface extends Mock implements HiveStorageInterface {}

class MockWebInterface extends Mock implements WebInterface {}

void main() {
  group('HomeView', () {
    late RecipesExplorerCubit recipesExplorerCubit;
    late RecipeCubit recipeCubit;
    late ClipboardInterface clipboardInterface;
    late HiveStorageInterface hiveStorageInterface;
    late WebInterface webInterface;

    setUp(() {
      recipesExplorerCubit = MockRecipesExplorerCubit();
      when(() => recipesExplorerCubit.state).thenReturn(
        RecipesExplorerState(
          recipes: [],
          labelColors: [],
          searchText: "",
        ),
      );

      recipeCubit = MockRecipeCubit();
      when(() => recipeCubit.state).thenReturn(
        RecipeState(
          Recipe("", labels: [], ingredients: [], steps: []),
          labelColors: [],
        ),
      );

      clipboardInterface = MockClipboardInterface();
      hiveStorageInterface = MockHiveStorageInterface();
      webInterface = MockWebInterface();
    });

    group('on init (search disabled)', () {
      testWidgets('has correct basic UI components and can enable search', (WidgetTester tester) async {
        when(() => recipesExplorerCubit.state).thenReturn(
          RecipesExplorerState(
            recipes: [
              Recipe("with_label_0", labels: ["label_0"], ingredients: [], steps: []),
              Recipe("with_label_1", labels: ["label_1"], ingredients: [], steps: []),
              Recipe("with_both", labels: ["label_0", "label_1"], ingredients: [], steps: []),
            ],
            labelColors: [
              LabelColor("label_0", Colors.green),
              LabelColor("label_1", Colors.grey),
            ],
            searchText: null,
          ),
        );
        await tester.pumpWidget(
          RepositoryProvider.value(
            value: recipesExplorerCubit,
            child: MaterialApp(
              home: HomeView(title: "initech"),
            ),
          ),
        );
        expect(find.byType(AppBar), findsOneWidget);
        expect(find.text('initech'), findsOneWidget);
        expect(find.byIcon(Icons.search), findsOneWidget);
        expect(find.byWidgetPredicate((widget) {
          return widget is IconButton && (widget.icon as Icon).icon == Icons.search;
        }), findsOneWidget);
        expect(find.byWidgetPredicate((widget) => widget is PopupMenuButton), findsOneWidget);
        expect(find.byType(TabBar), findsOneWidget);
        expect(
          find.byWidgetPredicate((widget) {
            return widget is TabBar &&
                widget.indicatorColor == Colors.tealAccent &&
                widget.tabs.length == 3 &&
                widget.tabs[0] is Tab &&
                (widget.tabs[0] as Tab).icon.toString() == Icon(Icons.sort_by_alpha).toString() &&
                widget.tabs[1] is LabelTab &&
                (widget.tabs[1] as LabelTab).label == "label_0" &&
                (widget.tabs[1] as LabelTab).iconColor == Colors.green &&
                widget.tabs[2] is LabelTab &&
                (widget.tabs[2] as LabelTab).label == "label_1" &&
                (widget.tabs[2] as LabelTab).iconColor == Colors.grey;
          }),
          findsOneWidget,
        );
        expect(find.byType(HomeViewBody), findsOneWidget);

        await tester.tap(find.byIcon(Icons.search));
        verify(recipesExplorerCubit.enableSearch).called(1);
      });
    });

    group("emit nav-to-delete-recipes state", () {
      testWidgets("triggers navigation (displays new screen)", (WidgetTester tester) async {
        when(hiveStorageInterface.getDeletedRecipes).thenReturn([]);
        whenListen(
            recipesExplorerCubit,
            Stream.fromIterable([
              RecipesExplorerStateNavigateToDeletedRecipesScreen.fromState(recipesExplorerCubit.state),
            ]));
        await tester.pumpWidget(
          MultiRepositoryProvider(
            providers: [
              RepositoryProvider.value(value: hiveStorageInterface),
              RepositoryProvider.value(value: recipesExplorerCubit),
            ],
            child: MaterialApp(
              home: HomeView(title: ""),
            ),
          ),
        );
        await tester.pumpAndSettle();
        expect(find.byType(DeletedRecipesScreen), findsOneWidget);
      });
    });

    group("emit nav-to-add-new-recipe state", () {
      testWidgets("triggers navigation (displays new screen)", (WidgetTester tester) async {
        final _labelColor = LabelColor("TPS", Colors.red);
        final _initialState = RecipesExplorerState(recipes: [], labelColors: [_labelColor], searchText: null);
        whenListen(
          recipesExplorerCubit,
          Stream<RecipesExplorerState>.value(
            RecipesExplorerStateNavigateToScreenAddNewRecipe.fromState(_initialState),
          ),
          initialState: _initialState,
        );
        await tester.pumpWidget(
          MultiRepositoryProvider(
            providers: [
              RepositoryProvider.value(value: hiveStorageInterface),
              RepositoryProvider.value(value: clipboardInterface),
              RepositoryProvider.value(value: webInterface),
              RepositoryProvider.value(value: recipesExplorerCubit),
            ],
            child: MaterialApp(
              home: HomeView(title: ""),
            ),
          ),
        );
        await tester.pumpAndSettle();
        expect(
          find.byWidgetPredicate((widget) {
            return widget is RecipeScreen &&
                widget.labelColors.length == 1 &&
                widget.labelColors[0] == _labelColor &&
                widget.recipe == Recipe.blank;
          }),
          findsOneWidget,
        );
      });
    });

    group('when search is enabled', () {
      testWidgets('appBar title & actions are in search-mode and can disable search', (WidgetTester tester) async {
        await tester.pumpWidget(
          RepositoryProvider.value(
            value: recipesExplorerCubit,
            child: MaterialApp(
              home: HomeView(title: "initech"),
            ),
          ),
        );
        expect(find.text('initech'), findsNothing);
        expect(
          find.byWidgetPredicate((widget) {
            return widget is SearchField && widget.text == "";
          }),
          findsOneWidget,
        );
        expect(find.byType(TextField), findsOneWidget);
        expect(find.byIcon(Icons.clear), findsOneWidget);

        await tester.tap(find.byIcon(Icons.clear));
        verify(recipesExplorerCubit.disableSearch).called(1);
      });
    });

    group("searching for some text", () {
      testWidgets("triggers cubit.updateSearch()", (WidgetTester tester) async {
        await tester.pumpWidget(
          RepositoryProvider.value(
            value: recipesExplorerCubit,
            child: MaterialApp(
              home: HomeView(title: ""),
            ),
          ),
        );

        await tester.enterText(find.byType(TextField), "PC");
        verify(() => recipesExplorerCubit.updateSearch("PC")).called(1);
        await tester.enterText(find.byType(TextField), "load");
        verify(() => recipesExplorerCubit.updateSearch("load")).called(1);
        await tester.enterText(find.byType(TextField), "letter");
        verify(() => recipesExplorerCubit.updateSearch("letter")).called(1);
      });
    });

    group("pop-up menu (app-bar action)", () {
      testWidgets("has option to add a new recipe", (WidgetTester tester) async {
        await tester.pumpWidget(
          MultiRepositoryProvider(
            providers: [
              RepositoryProvider.value(value: recipesExplorerCubit),
              RepositoryProvider.value(value: clipboardInterface),
            ],
            child: MaterialApp(
              home: HomeView(title: ""),
            ),
          ),
        );

        await tester.tap(find.byWidgetPredicate((widget) => widget is PopupMenuButton));
        await tester.pumpAndSettle();
        expect(
          find.byWidgetPredicate((widget) => widget is PopupMenuItem),
          findsNWidgets(4),
        );
        expect(find.byType(PopupMenuItemAddNewRecipe), findsOneWidget);

        await tester.tap(find.byType(PopupMenuItemAddNewRecipe));
        await tester.pumpAndSettle();
        verify(recipesExplorerCubit.navigateToRecipeScreenToAddNewRecipe).called(1);
      });

      testWidgets("has option to export recipes", (WidgetTester tester) async {
        await tester.pumpWidget(
          MultiRepositoryProvider(
            providers: [
              RepositoryProvider.value(value: recipesExplorerCubit),
              RepositoryProvider.value(value: clipboardInterface),
            ],
            child: MaterialApp(
              home: HomeView(title: ""),
            ),
          ),
        );

        await tester.tap(find.byWidgetPredicate((widget) => widget is PopupMenuButton));
        await tester.pumpAndSettle();
        expect(
          find.byWidgetPredicate((widget) => widget is PopupMenuItem),
          findsNWidgets(4),
        );
        expect(find.byType(PopupMenuItemExport), findsOneWidget);

        await tester.tap(find.byType(PopupMenuItemExport));
        await tester.pumpAndSettle();
        expect(find.byType(ExportRecipesScreen), findsOneWidget);
      });

      testWidgets("has option to edit labels", (WidgetTester tester) async {
        when(recipesExplorerCubit.updateIfRecipesWereUpdated).thenReturn(true);

        await tester.pumpWidget(
          MultiRepositoryProvider(
            providers: [
              RepositoryProvider.value(value: recipesExplorerCubit),
              RepositoryProvider.value(value: clipboardInterface),
              RepositoryProvider.value(value: hiveStorageInterface),
            ],
            child: MaterialApp(
              home: HomeView(title: ""),
            ),
          ),
        );

        await tester.tap(find.byWidgetPredicate((widget) => widget is PopupMenuButton));
        await tester.pumpAndSettle();
        expect(
          find.byWidgetPredicate((widget) => widget is PopupMenuItem),
          findsNWidgets(4),
        );
        expect(find.byType(PopupMenuItemEditLabels), findsOneWidget);

        await tester.tap(find.byType(PopupMenuItemEditLabels));
        await tester.pumpAndSettle();
        expect(find.byType(EditLabelsScreen), findsOneWidget);

        await tester.tap(find.byIcon(Icons.arrow_back));
        await tester.pumpAndSettle();
        expect(find.byType(HomeView), findsOneWidget);
        verify(recipesExplorerCubit.updateWithFreshLabelsAndColors).called(1);
        verify(recipesExplorerCubit.updateIfRecipesWereUpdated).called(1);
      });

      testWidgets("has option to view deleted recipes", (WidgetTester tester) async {
        await tester.pumpWidget(
          RepositoryProvider.value(
            value: recipesExplorerCubit,
            child: MaterialApp(
              home: HomeView(title: ""),
            ),
          ),
        );

        await tester.tap(find.byWidgetPredicate((widget) => widget is PopupMenuButton));
        await tester.pumpAndSettle();
        expect(
          find.byWidgetPredicate((widget) => widget is PopupMenuItem),
          findsNWidgets(4),
        );
        expect(find.byType(PopupMenuItemDeletedRecipes), findsOneWidget);

        await tester.tap(find.byType(PopupMenuItemDeletedRecipes));
        await tester.pumpAndSettle();
        verify(recipesExplorerCubit.navigateToDeletedRecipesScreen).called(1);
      });
    });

    group("SearchField", () {
      testWidgets("with different strings as search-text", (WidgetTester tester) async {
        for (String searchText in ["", "PC", "load letter"]) {
          await tester.pumpWidget(
            MaterialApp(
              home: Scaffold(
                body: SearchField(text: searchText),
              ),
            ),
          );
          expect(find.byIcon(Icons.search), findsOneWidget);
          expect(
            find.byWidgetPredicate((widget) {
              return widget is TextField &&
                  widget.autofocus == true &&
                  widget.controller!.text == searchText &&
                  widget.controller!.selection.baseOffset == searchText.length && // cursor position
                  (widget.decoration!.icon as Icon).icon == Icons.search &&
                  (widget.decoration!.icon as Icon).color == Colors.white.withOpacity(0.5) &&
                  widget.decoration!.focusedBorder ==
                      UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.black12),
                      );
            }),
            findsOneWidget,
          );
        }
      });
    });

    group('TabWithLabel', () {
      testWidgets('with non-null color', (WidgetTester tester) async {
        await tester.pumpWidget(
          MaterialApp(
            home: Scaffold(
              body: LabelTab(
                "a label",
                iconColor: Colors.cyan,
              ),
            ),
          ),
        );
        expect(find.byType(Tab), findsOneWidget);
        expect(find.byWidgetPredicate((widget) {
          return widget is Tab &&
              widget.child is Row &&
              (widget.child as Row).children.length == 3 &&
              (widget.child as Row).children[0] is Stack;
        }), findsOneWidget);
        expect(find.byType(Icon), findsNWidgets(2));
        expect(
          find.byWidgetPredicate((widget) {
            return widget is Icon && widget.color == Colors.white && widget.icon == Icons.label;
          }),
          findsOneWidget,
        );
        expect(
          find.byWidgetPredicate((widget) {
            return widget is Icon && widget.color == Colors.cyan.withOpacity(0.6) && widget.icon == Icons.label;
          }),
          findsOneWidget,
        );
        expect(find.byType(Text), findsOneWidget);
        expect(find.text('a label'), findsOneWidget);
      });

      testWidgets('with null as color', (WidgetTester tester) async {
        await tester.pumpWidget(
          MaterialApp(
            home: Scaffold(
              body: LabelTab(
                "TPS",
                iconColor: null,
              ),
            ),
          ),
        );
        expect(find.byType(Tab), findsOneWidget);
        expect(find.byWidgetPredicate((widget) {
          return widget is Tab &&
              widget.child is Row &&
              (widget.child as Row).children.length == 3 &&
              (widget.child as Row).children[0] is Stack;
        }), findsOneWidget);
        expect(find.byType(Icon), findsNWidgets(2));
        expect(
          find.byWidgetPredicate((widget) {
            return widget is Icon && widget.color == Colors.white && widget.icon == Icons.label;
          }),
          findsOneWidget,
        );
        expect(
          find.byWidgetPredicate((widget) {
            return widget is Icon && widget.color == Colors.grey.withOpacity(0.6) && widget.icon == Icons.label;
          }),
          findsOneWidget,
        );
        expect(find.byType(Text), findsOneWidget);
        expect(find.text('TPS'), findsOneWidget);
      });
    });

    group('HomeViewBody', () {
      final _labelColors = [
        LabelColor("0", Colors.amber),
        LabelColor("1", Colors.orange),
      ];
      final _tabsCount = 3;

      group('search_text is `null`', () {
        final _searchText = null;

        testWidgets('when all tabs have recipes -- has correct UI', (WidgetTester tester) async {
          List<Recipe> recipes = [
            Recipe('PC', labels: [], ingredients: [], steps: []),
            Recipe('load', labels: [_labelColors.labels.first], ingredients: [], steps: []),
            Recipe('letter', labels: [_labelColors.labels.last], ingredients: [], steps: []),
          ];
          when(() => recipesExplorerCubit.state).thenReturn(
            RecipesExplorerState(recipes: recipes, labelColors: _labelColors, searchText: _searchText),
          );
          await tester.pumpWidget(
            RepositoryProvider.value(
              value: recipesExplorerCubit,
              child: MaterialApp(
                home: DefaultTabController(
                  length: _tabsCount,
                  child: Scaffold(
                    body: HomeViewBody(),
                  ),
                ),
              ),
            ),
          );
          expect(
            find.byWidgetPredicate((widget) {
              return widget is TabBarView &&
                  widget.children.length == _tabsCount &&
                  widget.children[0] is PaddedListView &&
                  widget.children[1] is PaddedListView &&
                  widget.children[2] is PaddedListView;
            }),
            findsOneWidget,
          );
          expect(find.byType(PaddedListView), findsOneWidget); // only one visible at a time
          expect(find.byType(RecipeListTile), findsNWidgets(recipes.length));
        });

        testWidgets('when all tabs have no recipes -- has correct UI', (WidgetTester tester) async {
          when(() => recipesExplorerCubit.state).thenReturn(
            RecipesExplorerState(recipes: [], labelColors: _labelColors, searchText: _searchText),
          );
          await tester.pumpWidget(
            RepositoryProvider.value(
              value: recipesExplorerCubit,
              child: MaterialApp(
                home: DefaultTabController(
                  length: _tabsCount,
                  child: Scaffold(
                    body: HomeViewBody(),
                  ),
                ),
              ),
            ),
          );
          expect(find.byType(TabBarView), findsOneWidget);
          expect(
            find.byWidgetPredicate((widget) {
              return widget is TabBarView &&
                  widget.children.length == _tabsCount &&
                  ((widget.children[0] as ListTile).title as Text).data == '\n🙈   No recipes found.' &&
                  ((widget.children[1] as ListTile).title as Text).data ==
                      '\n🙈   No recipes found with "${_labelColors[0].label}" label.' &&
                  ((widget.children[2] as ListTile).title as Text).data ==
                      '\n🙈   No recipes found with "${_labelColors[1].label}" label.';
            }),
            findsOneWidget,
          );
          expect(find.byType(PaddedListView), findsNothing);
          expect(find.byType(RecipeListTile), findsNothing);
        });
      });

      group('search_text is blank', () {
        final _searchText = "";

        testWidgets('when all tabs have recipes -- has correct UI', (WidgetTester tester) async {
          List<Recipe> recipes = [
            Recipe('PC', labels: [], ingredients: [], steps: []),
            Recipe('load', labels: [_labelColors.labels.first], ingredients: [], steps: []),
            Recipe('letter', labels: [_labelColors.labels.last], ingredients: [], steps: []),
          ];
          when(() => recipesExplorerCubit.state).thenReturn(
            RecipesExplorerState(recipes: recipes, labelColors: _labelColors, searchText: _searchText),
          );
          await tester.pumpWidget(
            RepositoryProvider.value(
              value: recipesExplorerCubit,
              child: MaterialApp(
                home: DefaultTabController(
                  length: _tabsCount,
                  child: Scaffold(
                    body: HomeViewBody(),
                  ),
                ),
              ),
            ),
          );
          expect(
            find.byWidgetPredicate((widget) {
              return widget is TabBarView &&
                  widget.children.length == _tabsCount &&
                  widget.children[0] is PaddedListView &&
                  widget.children[1] is PaddedListView &&
                  widget.children[2] is PaddedListView;
            }),
            findsOneWidget,
          );
          expect(find.byType(PaddedListView), findsOneWidget); // only one visible at a time
          expect(find.byType(RecipeListTile), findsNWidgets(recipes.length));
        });

        testWidgets('when all tabs have no recipes -- has correct UI', (WidgetTester tester) async {
          when(() => recipesExplorerCubit.state).thenReturn(
            RecipesExplorerState(recipes: [], labelColors: _labelColors, searchText: _searchText),
          );
          await tester.pumpWidget(
            RepositoryProvider.value(
              value: recipesExplorerCubit,
              child: MaterialApp(
                home: DefaultTabController(
                  length: _tabsCount,
                  child: Scaffold(
                    body: HomeViewBody(),
                  ),
                ),
              ),
            ),
          );
          expect(find.byType(TabBarView), findsOneWidget);
          expect(
            find.byWidgetPredicate((widget) {
              return widget is TabBarView &&
                  widget.children.length == _tabsCount &&
                  ((widget.children[0] as ListTile).title as Text).data == '\n🙈   No recipes found.' &&
                  ((widget.children[1] as ListTile).title as Text).data ==
                      '\n🙈   No recipes found with "${_labelColors[0].label}" label.' &&
                  ((widget.children[2] as ListTile).title as Text).data ==
                      '\n🙈   No recipes found with "${_labelColors[1].label}" label.';
            }),
            findsOneWidget,
          );
          expect(find.byType(PaddedListView), findsNothing);
          expect(find.byType(RecipeListTile), findsNothing);
        });
      });

      group('with search_text', () {
        final _searchText = "TPS";

        testWidgets('when all tabs have recipes -- has correct UI', (WidgetTester tester) async {
          List<Recipe> recipes = [
            Recipe('PC', labels: [], ingredients: [], steps: []),
            Recipe('load', labels: [_labelColors.labels.first], ingredients: [], steps: []),
            Recipe('letter', labels: [_labelColors.labels.last], ingredients: [], steps: []),
          ];
          when(() => recipesExplorerCubit.state).thenReturn(
            RecipesExplorerState(recipes: recipes, labelColors: _labelColors, searchText: _searchText),
          );
          await tester.pumpWidget(
            RepositoryProvider.value(
              value: recipesExplorerCubit,
              child: MaterialApp(
                home: DefaultTabController(
                  length: _tabsCount,
                  child: Scaffold(
                    body: HomeViewBody(),
                  ),
                ),
              ),
            ),
          );
          expect(
            find.byWidgetPredicate((widget) {
              return widget is TabBarView &&
                  widget.children.length == _tabsCount &&
                  widget.children[0] is PaddedListView &&
                  widget.children[1] is PaddedListView &&
                  widget.children[2] is PaddedListView;
            }),
            findsOneWidget,
          );
          expect(find.byType(PaddedListView), findsOneWidget); // only one visible at a time
          expect(find.byType(RecipeListTile), findsNWidgets(recipes.length));
        });

        testWidgets('when all tabs have no recipes -- has correct UI', (WidgetTester tester) async {
          when(() => recipesExplorerCubit.state).thenReturn(
            RecipesExplorerState(recipes: [], labelColors: _labelColors, searchText: _searchText),
          );
          await tester.pumpWidget(
            RepositoryProvider.value(
              value: recipesExplorerCubit,
              child: MaterialApp(
                home: DefaultTabController(
                  length: _tabsCount,
                  child: Scaffold(
                    body: HomeViewBody(),
                  ),
                ),
              ),
            ),
          );
          expect(find.byType(TabBarView), findsOneWidget);
          expect(
            find.byWidgetPredicate((widget) {
              return widget is TabBarView &&
                  widget.children.length == _tabsCount &&
                  ((widget.children[0] as ListTile).title as Text).data ==
                      '\n🙈   No recipes found when searching "$_searchText".' &&
                  ((widget.children[1] as ListTile).title as Text).data ==
                      '\n🙈   No recipes found with "${_labelColors[0].label}" label when searching "$_searchText".' &&
                  ((widget.children[2] as ListTile).title as Text).data ==
                      '\n🙈   No recipes found with "${_labelColors[1].label}" label when searching "$_searchText".';
            }),
            findsOneWidget,
          );
          expect(find.byType(PaddedListView), findsNothing);
          expect(find.byType(RecipeListTile), findsNothing);
        });
      });
    });

    group('RecipeListTile', () {
      testWidgets('has correct UI', (WidgetTester tester) async {
        Recipe recipe = Recipe("PC load letter", labels: [], ingredients: [], steps: []);
        await tester.pumpWidget(
          MaterialApp(
            home: Scaffold(
              body: RecipeListTile(
                recipe,
                labelColors: [],
              ),
            ),
          ),
        );
        expect(find.byType(ListTile), findsOneWidget);
        expect(find.text(recipe.title), findsOneWidget);
        expect(find.byType(LabelsRow), findsOneWidget);
      });

      testWidgets('tapping goes to recipe-screen', (WidgetTester tester) async {
        await tester.pumpWidget(
          MultiRepositoryProvider(
            providers: [
              RepositoryProvider.value(value: recipeCubit),
              RepositoryProvider.value(value: clipboardInterface),
              RepositoryProvider.value(value: hiveStorageInterface),
              RepositoryProvider.value(value: webInterface),
            ],
            child: MaterialApp(
              home: Scaffold(
                body: RecipeListTile(
                  Recipe("", labels: [], ingredients: [], steps: []),
                  labelColors: [],
                ),
              ),
            ),
          ),
        );
        await tester.tap(find.byType(RecipeListTile));
        await tester.pumpAndSettle();
        expect(find.byType(RecipeScreen), findsOneWidget);
      });
    });

    group('LabelsRow', () {
      testWidgets('has correct UI', (WidgetTester tester) async {
        List<LabelColor> labelColors = [
          LabelColor("PC", Colors.deepOrange),
          LabelColor("load letter", Colors.green),
        ];
        await tester.pumpWidget(
          MaterialApp(
            home: Scaffold(
              body: LabelsRow(
                labelColors.map((labelColor) => labelColor.label),
                labelColors: labelColors,
              ),
            ),
          ),
        );
        for (final labelColor in labelColors) {
          expect(
            find.byWidgetPredicate((widget) {
              return widget is LabelCard && widget.title == labelColor.label && widget.color == labelColor.color;
            }),
            findsOneWidget,
          );
        }
      });
    });
  });
}
