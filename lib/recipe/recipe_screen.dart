import 'package:captainfoodmaker/data_models/label_colors.dart';
import 'package:captainfoodmaker/data_models/recipe.dart';
import 'package:captainfoodmaker/interfaces/clipboard_interface.dart';
import 'package:captainfoodmaker/interfaces/hive_storage_interface.dart';
import 'package:captainfoodmaker/interfaces/web_interface.dart';
import 'package:captainfoodmaker/recipe/recipe_cubit.dart';
import 'package:captainfoodmaker/shared_getters.dart';
import 'package:captainfoodmaker/shared_widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wakelock/wakelock.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';

// todo -- expand UI around "Save changes" button so it has more info and feels 'separated' a bit
class RecipeScreen extends StatelessWidget {
  final Recipe recipe;
  final List<LabelColor> labelColors;

  const RecipeScreen(
    this.recipe, {
    required this.labelColors,
    Key? key,
  }) : super(key: key);

  factory RecipeScreen.withBlankRecipe({required List<LabelColor> labelColors}) {
    return RecipeScreen(
      Recipe.blank,
      labelColors: labelColors,
    );
  }

  @override
  Widget build(BuildContext context) {
    Wakelock.enable();
    return BlocProvider(
      create: (BuildContext context) {
        return RecipeCubit(
          recipe,
          labelColors,
          context.read<ClipboardInterface>(),
          context.read<HiveStorageInterface>(),
          context.read<WebInterface>(),
        );
      },
      child: const RecipeView(),
    );
  }
}

class RecipeView extends StatelessWidget {
  static const List<String> tabsTexts = [
    "Ingredients",
    "Steps",
    "Labels",
    "Website",
  ];

  const RecipeView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<RecipeCubit, RecipeState>(
      listener: (BuildContext context, RecipeState state) {
        if (state.isRecipeDeleted) {
          ScaffoldMessenger.of(context).clearSnackBars();
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              content: const Text("Recipe was deleted!"),
              action: SnackBarAction(
                label: "Undo",
                onPressed: context.read<RecipeCubit>().restoreRecipe,
              ),
            ),
          );
        } else if (state is RecipeStateEditModeRestored) {
          ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(
              content: Text("Restored previous unsaved edits."),
            ),
          );
        }
      },
      buildWhen: _shouldBuild,
      builder: (BuildContext context, RecipeState state) {
        if (state is RecipeStatePromptForBlankRecipeOrPasteFromClipboard) {
          return const PromptScaffold();
        } else {
          return DefaultTabController(
            length: tabsTexts.length,
            child: Scaffold(
              appBar: RecipeViewAppBar(
                indicatorColor: Theme.of(context).colorScheme.secondary,
              ),
              body: const RecipeViewerBody(),
            ),
          );
        }
      },
    );
  }

  bool _shouldBuild(RecipeState previousState, RecipeState currentState) {
    return currentState is! RecipeStateProcessingClipboardData;
  }
}

class PromptScaffold extends StatelessWidget {
  const PromptScaffold({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocListener<RecipeCubit, RecipeState>(
      listenWhen: _shouldListen,
      listener: (BuildContext context, RecipeState state) {
        ScaffoldMessenger.of(context).showSnackBar(_snackBar(state));
      },
      child: Scaffold(
        appBar: AppBar(
          title: const Text("Add new recipe"),
        ),
        body: const PromptBody(),
      ),
    );
  }

  bool _shouldListen(RecipeState previousState, RecipeState currentState) {
    return currentState is RecipeStateErrorParsingRecipeFromClipboard;
  }

  SnackBar _snackBar(RecipeState state) {
    late final String snackBarMessage;
    if (state is RecipeStateNoClipboardDataToPaste) {
      snackBarMessage = "Unable to get anything from the clipboard.\n\nMaybe try copying again?";
    } else if (state is RecipeStateFailedHttpGetRequest) {
      snackBarMessage = "Unable to fetch the data from that website.\n\n"
          "Maybe check your connection?\n\n"
          "Further error-details:\n"
          "'${state.failure}'";
    } else if (state is RecipeStateUnableToParseRecipeFromHttpResponseBodyError) {
      snackBarMessage = "Unable to parse recipe from that website.\n\n"
          "This feature is under development.  "
          "Please email never.ads.info@gmail.com with the website's url, and we'll make it work ASAP.";
    } else {
      snackBarMessage = "Unable to parse recipe. 😞\n\n"
          "This feature is under development.  "
          "Please email never.ads.info@gmail.com with the text you have copied, and we'll make it work ASAP.";
    }

    return SnackBar(
      content: Text(snackBarMessage),
    );
  }
}

class PromptBody extends StatelessWidget {
  const PromptBody({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<RecipeCubit, RecipeState>(
      builder: (BuildContext context, RecipeState state) {
        return Center(
          child: state is RecipeStatePromptForBlankRecipeOrPasteFromClipboard
              ? _promptButtons
              : const CircularProgressIndicator(),
        );
      },
    );
  }

  Widget get _promptButtons {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.center,
      children: const [
        StartFromScratchButton(),
        SizedBox(height: 8),
        PasteFromClipboardButton(),
      ],
    );
  }
}

class StartFromScratchButton extends StatelessWidget {
  const StartFromScratchButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // todo -- change to be list_tile(?) with explanation/subtitle
    final Color color = Theme.of(context).colorScheme.onSurfaceVariant;
    return TextButton.icon(
      icon: Icon(
        Icons.note_add_outlined,
        color: color,
      ),
      label: Text(
        "Start from scratch",
        style: Theme.of(context).textTheme.bodyLarge?.copyWith(color: color),
      ),
      onPressed: context.read<RecipeCubit>().startFromScratch,
    );
  }
}

class PasteFromClipboardButton extends StatelessWidget {
  const PasteFromClipboardButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Color color = Theme.of(context).colorScheme.onSurfaceVariant;
    return TextButton.icon(
      icon: Icon(
        Icons.paste,
        color: color,
      ),
      label: Text(
        "Paste from clipboard",
        style: Theme.of(context).textTheme.bodyLarge?.copyWith(color: color),
      ),
      onPressed: context.read<RecipeCubit>().pasteFromClipboard,
    );
  }
}

class RecipeViewAppBar extends AppBar {
  RecipeViewAppBar({required Color indicatorColor, Key? key})
      : super(
          key: key,
          title: const RecipeTitle(),
          actions: [
            const ToggleEditModeIconButton(),
            const RecipeScreenPopupButton(),
          ],
          bottom: TabBar(
            indicatorColor: indicatorColor,
            isScrollable: true,
            tabs: RecipeView.tabsTexts.map((tabText) {
              return Tab(text: tabText);
            }).toList(),
          ),
        );
}

class RecipeTitle extends StatefulWidget {
  const RecipeTitle({Key? key}) : super(key: key);

  static const String hintText = "recipe title";

  @override
  State<RecipeTitle> createState() => _RecipeTitleState();
}

class _RecipeTitleState extends State<RecipeTitle> {
  late TextEditingController _controller;
  late RecipeCubit cubit;

  @override
  void initState() {
    super.initState();
    cubit = context.read<RecipeCubit>();
  }

  @override
  Widget build(BuildContext context) {
    final Color colorOnPrimary = Theme.of(context).colorScheme.onPrimary;

    return BlocBuilder<RecipeCubit, RecipeState>(
      buildWhen: _isTogglingEditMode,
      builder: (BuildContext context, RecipeState state) {
        RecipeState state = cubit.state;
        bool isInEditMode = state is RecipeStateEditMode;
        Recipe recipe = isInEditMode ? state.editedRecipe! : state.recipe;
        _controller = TextEditingController(text: recipe.title);

        return TextField(
          controller: _controller,
          decoration: RecipeTextDecoration(
            hasBorder: isInEditMode,
            hintText: RecipeTitle.hintText,
          ),
          style: paragraphStyleFromContext(context)?.copyWith(
            color: colorOnPrimary,
            fontWeight: Theme.of(context).textTheme.titleSmall?.fontWeight,
            overflow: TextOverflow.ellipsis,
          ),
          maxLines: 2,
          minLines: 1,
          onChanged: context.read<RecipeCubit>().updateTitle,
          enabled: state is RecipeStateEditMode,
          cursorColor: Colors.grey,
        );
      },
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  bool _isTogglingEditMode(RecipeState previousState, RecipeState currentState) {
    final bool isChangingToViewMode = previousState is RecipeStateEditMode && currentState is! RecipeStateEditMode;
    final bool isChangingToEditMode = previousState is! RecipeStateEditMode && currentState is RecipeStateEditMode;
    return isChangingToViewMode || isChangingToEditMode;
  }
}

class ToggleEditModeIconButton extends StatelessWidget {
  const ToggleEditModeIconButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<RecipeCubit, RecipeState>(
      builder: (BuildContext context, RecipeState state) {
        if (state is RecipeStateEditMode || state.isRecipeDeleted) {
          return const SizedBox();
        } else {
          return IconButton(
            icon: const Icon(Icons.edit),
            onPressed: context.read<RecipeCubit>().enableEditMode,
          );
        }
      },
    );
  }
}

class RecipeScreenPopupButton extends StatelessWidget {
  const RecipeScreenPopupButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<RecipeCubit, RecipeState>(
      builder: (BuildContext context, RecipeState state) {
        if (state is RecipeStateEditMode) {
          return const SizedBox();
        } else {
          return PopupMenuButton<String>(
            itemBuilder: (BuildContext context) {
              return state.isRecipeDeleted
                  ? [PopupMenuItemRestoreRecipe()]
                  : [PopupMenuItemExport(), PopupMenuItemDeleteRecipe()];
            },
            onSelected: (String value) {
              final RecipeCubit cubit = context.read<RecipeCubit>();
              if (value == PopupMenuItemExport.valueString) {
                cubit.exportRecipe();
                ScaffoldMessenger.of(context).clearSnackBars();
                ScaffoldMessenger.of(context).showSnackBar(
                  const SnackBar(
                    content: Text(
                      "Recipe copied to clipboard!  You can paste it in a email, text document, etc.",
                    ),
                  ),
                );
              } else if (value == PopupMenuItemDeleteRecipe.valueString) {
                cubit.deleteRecipe();
              } else if (value == PopupMenuItemRestoreRecipe.valueString) {
                cubit.restoreRecipe();
                ScaffoldMessenger.of(context).clearSnackBars();
                ScaffoldMessenger.of(context).showSnackBar(
                  const SnackBar(
                    content: Text(
                      "Recipe was restored (un-deleted).",
                    ),
                  ),
                );
              }
            },
          );
        }
      },
    );
  }
}

class RecipeViewerBody extends StatelessWidget {
  const RecipeViewerBody({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(
          child: TabBarView(
            children: [
              IngredientsListView(),
              StepsListView(),
              const LabelsListView(),
              const WebsiteWidget(),
            ],
          ),
        ),
        const DoneEditingRecipeWidget(),
      ],
    );
  }
}

class IngredientsListView extends StatelessWidget {
  IngredientsListView({Key? key}) : super(key: key);

  final ScrollController scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<RecipeCubit, RecipeState>(
      buildWhen: _shouldBuild,
      builder: (BuildContext context, RecipeState state) {
        if (state is RecipeStateEditModeAddIngredient) {
          SchedulerBinding.instance.addPostFrameCallback((_) {
            scrollController.animateTo(
              scrollController.position.maxScrollExtent,
              duration: const Duration(milliseconds: 300),
              curve: Curves.ease,
            );
          });
        }

        final bool isInEditMode = state is RecipeStateEditMode;
        final Recipe recipe = isInEditMode ? state.editedRecipe! : state.recipe;

        final PaddedReorderableListView reorderableIngredients = PaddedReorderableListView(
          padding: isInEditMode ? ScrollViewEdgeInsets.onlyTop : const ScrollViewEdgeInsets(),
          children: recipe.ingredientsIndexes.map((index) {
            return IngredientWidget(index);
          }).toList(),
          onReorder: isInEditMode ? context.read<RecipeCubit>().reorderIngredient : null,
          controller: scrollController,
        );

        if (isInEditMode) {
          return Column(
            children: [
              Expanded(child: reorderableIngredients),
              const AddIngredientButton(),
            ],
          );
        } else {
          return reorderableIngredients;
        }
      },
    );
  }

  bool _shouldBuild(RecipeState previousState, RecipeState currentState) {
    return currentState is RecipeStateEditModeAddIngredient ||
        _isTogglingEditMode(previousState, currentState) ||
        !_areCompletedIngredientsSame(previousState, currentState);
  }

  bool _isTogglingEditMode(RecipeState previousState, RecipeState currentState) {
    final bool isChangingToViewMode = previousState is RecipeStateEditMode && currentState is! RecipeStateEditMode;
    final bool isChangingToEditMode = previousState is! RecipeStateEditMode && currentState is RecipeStateEditMode;
    return isChangingToViewMode || isChangingToEditMode;
  }

  bool _areCompletedIngredientsSame(RecipeState previousState, RecipeState currentState) {
    if (previousState is RecipeStateEditMode || currentState is RecipeStateEditMode) {
      return true;
    } else {
      return previousState.completedIngredients == currentState.completedIngredients;
    }
  }
}

class IngredientWidget extends StatefulWidget {
  final int _ingredientIndex;

  IngredientWidget(this._ingredientIndex) : super(key: UniqueKey());

  @override
  State<IngredientWidget> createState() => _IngredientWidgetState();
}

class _IngredientWidgetState extends State<IngredientWidget> {
  late TextEditingController _amountController;
  late TextEditingController _descriptionController;
  late final RecipeCubit cubit;
  late bool isInEditMode;

  @override
  void initState() {
    super.initState();
    cubit = context.read<RecipeCubit>();
  }

  @override
  Widget build(BuildContext context) {
    final RecipeState state = cubit.state;
    isInEditMode = state is RecipeStateEditMode;
    final Recipe recipe = isInEditMode ? state.editedRecipe! : state.recipe;
    final Ingredient _ingredient = recipe.ingredients[widget._ingredientIndex];
    final bool _isIngredientCompleted = state.completedIngredients.contains(_ingredient);
    _amountController = TextEditingController(text: _ingredient.amount);
    _descriptionController = TextEditingController(text: _ingredient.description);

    return isInEditMode
        ? ListTile(
            title: _ingredientWidget,
            trailing: const ReorderIconButton(),
          )
        : CheckboxListTile(
            value: _isIngredientCompleted,
            title: Opacity(
              opacity: _isIngredientCompleted ? 0.5 : 1.0,
              child: _ingredientWidget,
            ),
            onChanged: (bool? newValue) {
              if (newValue != null) {
                cubit.toggleIngredientCompleted(
                  _ingredient,
                  newValue: newValue,
                );
                final CompletedIngredient completedIngredient = CompletedIngredient(
                  _ingredient,
                  recipeTitle: recipe.title,
                );
                if (newValue) {
                  sessionCompletedIngredients.add(completedIngredient);
                } else {
                  sessionCompletedIngredients.remove(completedIngredient);
                }
              }
            },
          );
  }

  @override
  void dispose() {
    _amountController.dispose();
    _descriptionController.dispose();
    super.dispose();
  }

  Row get _ingredientWidget {
    final Widget amountWidget = Expanded(
      child: TextField(
        controller: _amountController,
        decoration: RecipeTextDecoration(
          hasBorder: isInEditMode,
          hintText: isInEditMode ? "amount" : null,
        ),
        textAlign: TextAlign.end,
        maxLines: null,
        enabled: isInEditMode,
        onChanged: isInEditMode
            ? (String newText) {
                return cubit.updateIngredient(
                  Ingredient(newText, _descriptionController.text),
                  ingredientIndex: widget._ingredientIndex,
                );
              }
            : null,
      ),
      flex: 4,
    );

    final Widget descriptionWidget = Expanded(
      child: TextField(
        controller: _descriptionController,
        decoration: RecipeTextDecoration(
          hasBorder: isInEditMode,
          hintText: isInEditMode ? "ingredient" : null,
        ),
        textAlign: TextAlign.start,
        maxLines: null,
        enabled: isInEditMode,
        onChanged: isInEditMode
            ? (String newText) {
                return cubit.updateIngredient(
                  Ingredient(_amountController.text, newText),
                  ingredientIndex: widget._ingredientIndex,
                );
              }
            : null,
      ),
      flex: 7,
    );

    return Row(
      children: [
        amountWidget,
        const SizedBox(width: 24),
        descriptionWidget,
      ],
    );
  }
}

class AddIngredientButton extends StatelessWidget {
  const AddIngredientButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AddRecipeElementButton(
      "Add ingredient",
      onPressed: context.read<RecipeCubit>().addIngredient,
    );
  }
}

class StepsListView extends StatelessWidget {
  StepsListView({Key? key}) : super(key: key);

  final ScrollController scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<RecipeCubit, RecipeState>(
      buildWhen: _shouldBuild,
      builder: (BuildContext context, RecipeState state) {
        if (state is RecipeStateEditModeAddStep) {
          SchedulerBinding.instance.addPostFrameCallback((_) {
            scrollController.animateTo(
              scrollController.position.maxScrollExtent,
              duration: const Duration(milliseconds: 300),
              curve: Curves.ease,
            );
          });
        }

        final bool isInEditMode = state is RecipeStateEditMode;
        final Recipe recipe = isInEditMode ? state.editedRecipe! : state.recipe;

        final PaddedReorderableListView reorderableSteps = PaddedReorderableListView(
          padding: isInEditMode ? ScrollViewEdgeInsets.onlyTop : const ScrollViewEdgeInsets(),
          children: recipe.stepsIndexes.map((index) {
            return StepWidget(index);
          }).toList(),
          onReorder: isInEditMode ? context.read<RecipeCubit>().reorderStep : null,
          controller: scrollController,
        );

        if (isInEditMode) {
          return Column(
            children: [
              Expanded(child: reorderableSteps),
              const AddStepButton(),
            ],
          );
        } else {
          return reorderableSteps;
        }
      },
    );
  }

  bool _shouldBuild(RecipeState previousState, RecipeState currentState) {
    return currentState is RecipeStateEditModeAddStep ||
        _isTogglingEditMode(previousState, currentState) ||
        !_areCompletedStepsSame(previousState, currentState);
  }

  bool _isTogglingEditMode(RecipeState previousState, RecipeState currentState) {
    final bool isChangingToViewMode = previousState is RecipeStateEditMode && currentState is! RecipeStateEditMode;
    final bool isChangingToEditMode = previousState is! RecipeStateEditMode && currentState is RecipeStateEditMode;
    return isChangingToViewMode || isChangingToEditMode;
  }

  bool _areCompletedStepsSame(RecipeState previousState, RecipeState currentState) {
    if (previousState is RecipeStateEditMode || currentState is RecipeStateEditMode) {
      return true;
    } else {
      return previousState.completedSteps == currentState.completedSteps;
    }
  }
}

class StepWidget extends StatefulWidget {
  final int _stepIndex;

  StepWidget(this._stepIndex) : super(key: UniqueKey());

  @override
  State<StepWidget> createState() => _StepWidgetState();
}

class _StepWidgetState extends State<StepWidget> {
  late TextEditingController _controller;
  late final RecipeCubit cubit;
  late bool isInEditMode;

  @override
  void initState() {
    super.initState();
    cubit = context.read<RecipeCubit>();
  }

  @override
  Widget build(BuildContext context) {
    final RecipeState state = cubit.state;
    isInEditMode = state is RecipeStateEditMode;
    final Recipe recipe = isInEditMode ? state.editedRecipe! : state.recipe;
    final String _step = recipe.steps[widget._stepIndex];
    final bool _isStepCompleted = state.completedSteps.contains(_step);
    _controller = TextEditingController(text: _step);

    return isInEditMode
        ? ListTile(
            title: _textField,
            trailing: const ReorderIconButton(),
          )
        : CheckboxListTile(
            value: _isStepCompleted,
            title: Opacity(
              opacity: _isStepCompleted ? 0.5 : 1,
              child: _textField,
            ),
            onChanged: (bool? newValue) {
              if (newValue != null) {
                cubit.toggleStepCompleted(
                  _step,
                  newValue: newValue,
                );
                final CompletedStep completedStep = CompletedStep(_step, recipeTitle: recipe.title);
                if (newValue) {
                  sessionCompletedSteps.add(completedStep);
                } else {
                  sessionCompletedSteps.remove(completedStep);
                }
              }
            },
          );
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  TextField get _textField {
    return TextField(
      controller: _controller,
      decoration: _decoration,
      style: paragraphStyleFromContext(context),
      maxLines: null,
      enabled: isInEditMode,
      onChanged: isInEditMode
          ? (String newText) {
              return cubit.updateStep(
                newText,
                stepIndex: widget._stepIndex,
              );
            }
          : null,
    );
  }

  int get _displayIndex => widget._stepIndex + 1;

  RecipeTextDecoration get _decoration {
    return RecipeTextDecoration(
      hasBorder: isInEditMode,
      hintText: "step",
      prefixText: "$_displayIndex.  ",
    );
  }
}

class AddStepButton extends StatelessWidget {
  const AddStepButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AddRecipeElementButton(
      "Add step",
      onPressed: context.read<RecipeCubit>().addStep,
    );
  }
}

class LabelsListView extends StatelessWidget {
  const LabelsListView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<RecipeCubit, RecipeState>(
      buildWhen: _shouldBuild,
      builder: (BuildContext context, RecipeState state) {
        bool isInEditMode = state is RecipeStateEditMode;
        Recipe recipe = isInEditMode ? state.editedRecipe! : state.recipe;

        final PaddedReorderableListView reorderableLabels = PaddedReorderableListView(
          padding: isInEditMode ? ScrollViewEdgeInsets.none : ScrollViewEdgeInsets.onlyBottom,
          children: recipe.labelsIndexes.map((index) {
            return LabelWidget(index);
          }).toList(),
          onReorder: state is RecipeStateEditMode ? context.read<RecipeCubit>().reorderLabel : null,
        );

        if (isInEditMode) {
          return Column(
            children: [
              Expanded(child: reorderableLabels),
              const AddLabelButton(),
            ],
          );
        } else {
          return reorderableLabels;
        }
      },
    );
  }

  bool _shouldBuild(RecipeState previousState, RecipeState currentState) {
    return currentState is RecipeStateEditModeAddLabel ||
        currentState is RecipeStateEditModeDeletedLabel ||
        _isTogglingEditMode(previousState, currentState);
  }

  bool _isTogglingEditMode(RecipeState previousState, RecipeState currentState) {
    final bool isChangingToViewMode = previousState is RecipeStateEditMode && currentState is! RecipeStateEditMode;
    final bool isChangingToEditMode = previousState is! RecipeStateEditMode && currentState is RecipeStateEditMode;
    return isChangingToViewMode || isChangingToEditMode;
  }
}

class LabelWidget extends StatefulWidget {
  final int _labelIndex;

  LabelWidget(this._labelIndex) : super(key: UniqueKey());

  @override
  State<LabelWidget> createState() => _LabelWidgetState();
}

class _LabelWidgetState extends State<LabelWidget> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<RecipeCubit, RecipeState>(
      builder: (BuildContext context, RecipeState state) {
        final bool isInEditMode = state is RecipeStateEditMode;
        final Recipe recipe = isInEditMode ? state.editedRecipe! : state.recipe;
        final String label = recipe.labels[widget._labelIndex];
        final Color color = state.labelColors.getColor(label);
        final LabelTextField labelTextField = LabelTextField(label, widget._labelIndex, isInEditMode);

        return Container(
          decoration: isInEditMode ? null : LabelColorGradient(color),
          child: ListTile(
            leading: ColorSelector(label),
            title: isInEditMode
                ? Row(
                    children: [
                      Expanded(child: labelTextField),
                      DeleteLabelButton(labelIndex: widget._labelIndex),
                    ],
                  )
                : labelTextField,
            trailing: isInEditMode ? const ReorderIconButton() : null,
          ),
        );
      },
    );
  }
}

class LabelTextField extends StatefulWidget {
  // todo -- widget-test this in view_mode & edit_mode

  final String label;
  final int labelIndex;
  final bool isInEditMode;

  const LabelTextField(
    this.label,
    this.labelIndex,
    this.isInEditMode, {
    Key? key,
  }) : super(key: key);

  @override
  State<LabelTextField> createState() => _LabelTextFieldState();
}

class _LabelTextFieldState extends State<LabelTextField> {
  late TextEditingController _controller;

  @override
  void initState() {
    super.initState();
    _controller = TextEditingController(text: widget.label);
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final RecipeCubit cubit = context.read<RecipeCubit>();

    return TypeAheadField(
      textFieldConfiguration: TextFieldConfiguration(
        controller: _controller,
        decoration: RecipeTextDecoration(
          hasBorder: widget.isInEditMode,
          hintText: "label",
        ),
        enabled: widget.isInEditMode,
        onChanged: (String newText) => cubit.updateLabelText(
          newText,
          labelAtIndex: widget.labelIndex,
        ),
      ),
      suggestionsCallback: cubit.getMatchingLabels,
      itemBuilder: (BuildContext context, dynamic suggestion) {
        return ListTile(
          title: Text(suggestion),
        );
      },
      noItemsFoundBuilder: (BuildContext context) => const SizedBox(),
      onSuggestionSelected: (dynamic suggestion) {
        cubit.updateLabelText(
          suggestion,
          labelAtIndex: widget.labelIndex,
        );
        setState(() => _controller.text = suggestion);
      },
    );
  }
}

class DeleteLabelButton extends StatelessWidget {
  final int labelIndex;

  const DeleteLabelButton({Key? key, required this.labelIndex}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onLongPress: () {
        context.read<RecipeCubit>().deleteLabel(labelIndex);
        ScaffoldMessenger.of(context).showSnackBar(
          _snackBar("Label removed."),
        );
      },
      child: IconButton(
        icon: Icon(
          Icons.delete_outline,
          color: Theme.of(context).colorScheme.error,
        ),
        onPressed: () {
          ScaffoldMessenger.of(context).showSnackBar(
            _snackBar("Long-press to remove label."),
          );
        },
      ),
    );
  }

  SnackBar _snackBar(String content) => SnackBar(content: Text(content));
}

class AddLabelButton extends StatelessWidget {
  const AddLabelButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AddRecipeElementButton(
      "Add label",
      onPressed: context.read<RecipeCubit>().addLabel,
    );
  }
}

class ColorSelector extends StatelessWidget {
  // todo -- widget-test this in view_mode & edit_mode

  final String _label;

  const ColorSelector(
    this._label, {
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<RecipeCubit, RecipeState>(
      builder: (BuildContext context, RecipeState state) {
        final bool isInEditMode = state is RecipeStateEditMode;
        final Color color = state.labelColors.getColor(_label);

        return DropdownButton<Color>(
          value: Color(color.value),
          underline: const SizedBox(),
          icon: isInEditMode
              // passing `null` means it will use the default icon
              ? null
              // should not be visible when in view-mode; not possible to change/select
              : _transparentSpacer,
          items: _colorOptions,
          onChanged: isInEditMode
              ? (Color? newColor) {
                  if (newColor != null) {
                    context.read<RecipeCubit>().updateLabelColor(_label, newColor.value);
                  }
                }
              : null,
        );
      },
    );
  }

  List<DropdownMenuItem<Color>> get _colorOptions {
    return selectableLabelColors.map((Color selectableColor) {
      return _colorOption(selectableColor);
    }).toList();
  }

  DropdownMenuItem<Color> _colorOption(Color color) {
    return DropdownMenuItem<Color>(
      value: Color(color.value),
      child: LabelColorIcon(color),
    );
  }

  Opacity get _transparentSpacer {
    return const Opacity(
      opacity: 0,
      child: Icon(Icons.arrow_drop_down),
    );
  }
}

class CopyButton extends StatelessWidget {
  final bool isEnabled;

  const CopyButton({
    Key? key,
    required this.isEnabled,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return IconButton(
      icon: Icon(
        Icons.copy,
        color: isEnabled ? Theme.of(context).colorScheme.tertiary : Colors.transparent,
      ),
      onPressed: isEnabled
          ? () {
              final RecipeCubit cubit = context.read<RecipeCubit>();
              final String? url = cubit.state.recipe.url;
              final bool hasEmptyUrl = url == null || url.trim() == "";
              final String message = hasEmptyUrl ? "Nothing to copy." : 'Copied to clipboard:\n\n"$url"';
              if (!hasEmptyUrl) {
                cubit.copyUrlToClipboard();
              }
              ScaffoldMessenger.of(context).clearSnackBars();
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                  content: Text(message),
                ),
              );
            }
          : null,
    );
  }
}

class WebsiteWidget extends StatefulWidget {
  const WebsiteWidget({Key? key}) : super(key: key);

  @override
  State<WebsiteWidget> createState() => _WebsiteWidgetState();
}

class _WebsiteWidgetState extends State<WebsiteWidget> {
  final TextEditingController _controller = TextEditingController();
  late RecipeCubit cubit;

  @override
  void initState() {
    super.initState();
    cubit = context.read<RecipeCubit>();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<RecipeCubit, RecipeState>(
      buildWhen: _isTogglingEditMode,
      builder: (BuildContext context, RecipeState state) {
        final bool isInEditMode = state is RecipeStateEditMode;
        Recipe recipe = isInEditMode ? state.editedRecipe! : state.recipe;
        _controller.text = recipe.url ?? "";

        final TextField textField = TextField(
          controller: _controller,
          decoration: RecipeTextDecoration(
            hasBorder: isInEditMode,
            labelText: "URL for this recipe",
          ),
          onChanged: context.read<RecipeCubit>().updateUrl,
          enabled: isInEditMode,
        );

        return Padding(
          padding: ScrollViewEdgeInsets.onlyTop,
          child: ListTile(
            leading: CopyButton(isEnabled: !isInEditMode),
            title: textField,
          ),
        );
      },
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  bool _isTogglingEditMode(RecipeState previousState, RecipeState currentState) {
    final bool isChangingToViewMode = previousState is RecipeStateEditMode && currentState is! RecipeStateEditMode;
    final bool isChangingToEditMode = previousState is! RecipeStateEditMode && currentState is RecipeStateEditMode;
    return isChangingToViewMode || isChangingToEditMode;
  }
}

class DoneEditingRecipeWidget extends StatelessWidget {
  const DoneEditingRecipeWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<RecipeCubit, RecipeState>(
      builder: (BuildContext context, RecipeState state) {
        if (state is RecipeStateEditMode) {
          return Padding(
            padding: const EdgeInsets.symmetric(vertical: 24),
            child: ElevatedButton.icon(
              icon: const Icon(Icons.done),
              label: const Text("Save changes"),
              onPressed: state.editedRecipe!.title.trim() == ""
                  ? () => _showRejectionSnackBar(context)
                  : context.read<RecipeCubit>().saveEdits,
            ),
          );
        } else {
          return const SizedBox();
        }
      },
    );
  }

  void _showRejectionSnackBar(BuildContext context) {
    ScaffoldMessenger.of(context).showSnackBar(
      const SnackBar(
        content: Text("Recipe title cannot be blank."),
      ),
    );
  }
}

class LabelColorIcon extends StatelessWidget {
  // todo -- add widget-test for this
  final Color _color;

  const LabelColorIcon(
    this._color, {
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Icon(
      Icons.label,
      size: 32,
      color: _color.withOpacity(0.6),
    );
  }
}

class AddRecipeElementButton extends StatelessWidget {
  final String text;
  final VoidCallback onPressed;

  const AddRecipeElementButton(
    this.text, {
    required this.onPressed,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Color color = Theme.of(context).colorScheme.onSurfaceVariant;
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
      child: Row(
        children: [
          OutlinedButton.icon(
            icon: Icon(
              Icons.add,
              color: color,
            ),
            label: Text(
              text,
              style: Theme.of(context).textTheme.bodyLarge?.copyWith(color: color),
            ),
            onPressed: onPressed,
          ),
        ],
      ),
    );
  }
}

class RecipeTextDecoration extends InputDecoration {
  const RecipeTextDecoration({
    required bool hasBorder,
    String? labelText,
    String? hintText,
    String? prefixText,
  }) : super(
          labelText: labelText,
          hintText: hintText,
          hintStyle: hintText == RecipeTitle.hintText ? const TextStyle(color: Colors.white70) : null,
          prefixText: prefixText,
          border: hasBorder ? null : InputBorder.none,
        );
}

TextStyle? paragraphStyleFromContext(BuildContext context) {
  return Theme.of(context).textTheme.titleMedium?.copyWith(height: 1.5);
}
