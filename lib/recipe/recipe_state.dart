part of 'recipe_cubit.dart';

class RecipeState extends Equatable {
  final Recipe recipe;
  final Recipe? editedRecipe;
  final List<LabelColor> labelColors;
  final bool isRecipeDeleted;
  final List<Ingredient> completedIngredients;
  final List<String> completedSteps;

  const RecipeState(
    this.recipe, {
    this.editedRecipe,
    required this.labelColors,
    this.isRecipeDeleted = false,
    this.completedIngredients = const <Ingredient>[],
    this.completedSteps = const <String>[],
  });

  @override
  List<Object?> get props {
    final _props = [];
    _props.add(recipe);
    _props.add(editedRecipe);
    _props.addAll(labelColors);
    _props.add(isRecipeDeleted);
    _props.add(completedIngredients.toString());
    _props.add(completedSteps.toString());
    return _props;
  }
}

class RecipeStateInitial extends RecipeState {
  RecipeStateInitial(
    Recipe recipe, {
    required List<LabelColor> labelColors,
  }) : super(
          recipe.copyWith(blankElementsRemoved: true),
          labelColors: labelColors,
          completedIngredients: sessionCompletedIngredients
              .where((completedIngredient) => completedIngredient.recipeTitle == recipe.title)
              .map((completedIngredient) => completedIngredient.ingredient)
              .toList(),
          completedSteps: sessionCompletedSteps
              .where((completedStep) => completedStep.recipeTitle == recipe.title)
              .map((completedStep) => completedStep.step)
              .toList(),
        );
}

class RecipeStateEditMode extends RecipeState {
  const RecipeStateEditMode({
    required Recipe recipe,
    required Recipe editedRecipe,
    required List<LabelColor> labelColors,
    List<Ingredient> completedIngredients = const <Ingredient>[],
    List<String> completedSteps = const <String>[],
  }) : super(
          recipe,
          editedRecipe: editedRecipe,
          labelColors: labelColors,
          isRecipeDeleted: false,
          completedIngredients: completedIngredients,
          completedSteps: completedSteps,
        );
}

class RecipeStateEditModeRestored extends RecipeStateEditMode {
  const RecipeStateEditModeRestored({
    required Recipe recipe,
    required Recipe editedRecipe,
    required List<LabelColor> labelColors,
    List<Ingredient> completedIngredients = const <Ingredient>[],
    List<String> completedSteps = const <String>[],
  }) : super(
          recipe: recipe,
          editedRecipe: editedRecipe,
          labelColors: labelColors,
          completedIngredients: completedIngredients,
          completedSteps: completedSteps,
        );
}

class RecipeStateEditModeAddIngredient extends RecipeStateEditMode {
  RecipeStateEditModeAddIngredient({
    required Recipe recipe,
    required Recipe editedRecipe,
    required List<LabelColor> labelColors,
    List<Ingredient> completedIngredients = const <Ingredient>[],
    List<String> completedSteps = const <String>[],
  }) : super(
          recipe: recipe,
          editedRecipe: editedRecipe.copyWith(addedIngredient: Ingredient.empty),
          labelColors: labelColors,
          completedIngredients: completedIngredients,
          completedSteps: completedSteps,
        );
}

class RecipeStateEditModeAddStep extends RecipeStateEditMode {
  RecipeStateEditModeAddStep({
    required Recipe recipe,
    required Recipe editedRecipe,
    required List<LabelColor> labelColors,
    List<Ingredient> completedIngredients = const <Ingredient>[],
    List<String> completedSteps = const <String>[],
  }) : super(
          recipe: recipe,
          editedRecipe: editedRecipe.copyWith(addedStep: ""),
          labelColors: labelColors,
          completedIngredients: completedIngredients,
          completedSteps: completedSteps,
        );
}

class RecipeStateEditModeAddLabel extends RecipeStateEditMode {
  RecipeStateEditModeAddLabel({
    required Recipe recipe,
    required Recipe editedRecipe,
    required List<LabelColor> labelColors,
    List<Ingredient> completedIngredients = const <Ingredient>[],
    List<String> completedSteps = const <String>[],
  }) : super(
          recipe: recipe,
          editedRecipe: editedRecipe.copyWith(addedLabel: ""),
          labelColors: labelColors,
          completedIngredients: completedIngredients,
          completedSteps: completedSteps,
        );
}

class RecipeStateEditModeDeletedLabel extends RecipeStateEditMode {
  const RecipeStateEditModeDeletedLabel({
    required Recipe recipe,
    required Recipe editedRecipe,
    required List<LabelColor> labelColors,
    List<Ingredient> completedIngredients = const <Ingredient>[],
    List<String> completedSteps = const <String>[],
  }) : super(
          recipe: recipe,
          editedRecipe: editedRecipe,
          labelColors: labelColors,
          completedIngredients: completedIngredients,
          completedSteps: completedSteps,
        );
}

class RecipeStatePromptForBlankRecipeOrPasteFromClipboard extends RecipeState {
  RecipeStatePromptForBlankRecipeOrPasteFromClipboard({
    required List<LabelColor> labelColors,
  }) : super(
          Recipe.blank,
          labelColors: labelColors,
        );
}

class RecipeStateProcessingClipboardData extends RecipeState {
  RecipeStateProcessingClipboardData({
    required List<LabelColor> labelColors,
  }) : super(
          Recipe.blank,
          labelColors: labelColors,
        );
}

abstract class RecipeStateErrorParsingRecipeFromClipboard extends RecipeStatePromptForBlankRecipeOrPasteFromClipboard {
  RecipeStateErrorParsingRecipeFromClipboard({
    required List<LabelColor> labelColors,
  }) : super(labelColors: labelColors);
}

class RecipeStateNoClipboardDataToPaste extends RecipeStateErrorParsingRecipeFromClipboard {
  RecipeStateNoClipboardDataToPaste({
    required List<LabelColor> labelColors,
  }) : super(labelColors: labelColors);
}

class RecipeStateUnableToParseRecipeFromClipboardText extends RecipeStateErrorParsingRecipeFromClipboard {
  RecipeStateUnableToParseRecipeFromClipboardText({
    required List<LabelColor> labelColors,
  }) : super(labelColors: labelColors);
}

class RecipeStateUnableToParseRecipeFromHttpResponseBodyError extends RecipeStateErrorParsingRecipeFromClipboard {
  RecipeStateUnableToParseRecipeFromHttpResponseBodyError({
    required List<LabelColor> labelColors,
  }) : super(labelColors: labelColors);
}

class RecipeStateFailedHttpGetRequest extends RecipeStateErrorParsingRecipeFromClipboard {
  final String failure;

  RecipeStateFailedHttpGetRequest({
    required List<LabelColor> labelColors,
    required this.failure,
  }) : super(labelColors: labelColors);

  @override
  List<Object?> get props {
    final _props = [];
    _props.addAll(labelColors);
    _props.add(failure);
    return _props;
  }
}
