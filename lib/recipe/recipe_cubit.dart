import 'package:captainfoodmaker/data_models/label_colors.dart';
import 'package:captainfoodmaker/data_models/recipe.dart';
import 'package:captainfoodmaker/interfaces/clipboard_interface.dart';
import 'package:captainfoodmaker/interfaces/hive_storage_interface.dart';
import 'package:captainfoodmaker/interfaces/web_interface.dart';
import 'package:captainfoodmaker/shared_getters.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'recipe_state.dart';

class RecipeCubit extends Cubit<RecipeState> {
  final ClipboardInterface _clipboardInterface;
  final HiveStorageInterface _storageInterface;
  final WebInterface _webInterface;

  RecipeCubit(
    Recipe _recipe,
    List<LabelColor> labelColors,
    this._clipboardInterface,
    this._storageInterface,
    this._webInterface,
  ) : super(
          _recipe == Recipe.blank
              ? RecipeStatePromptForBlankRecipeOrPasteFromClipboard(labelColors: labelColors)
              : RecipeStateInitial(
                  _recipe,
                  labelColors: labelColors,
                ),
        );

  Future<void> exportRecipe() async {
    return await _clipboardInterface.copyTextToClipboard(
      state.recipe.toString(),
    );
  }

  void deleteRecipe() async {
    _storageInterface.setDeletionStatusForRecipe(state.recipe, newDeletionStatus: true);
    emit(
      RecipeState(
        state.recipe,
        labelColors: state.labelColors,
        isRecipeDeleted: true,
      ),
    );
  }

  void restoreRecipe() async {
    _storageInterface.setDeletionStatusForRecipe(state.recipe, newDeletionStatus: false);
    emit(
      RecipeState(
        state.recipe,
        labelColors: state.labelColors,
        isRecipeDeleted: false,
      ),
    );
  }

  void enableEditMode() {
    Recipe? editedRecipeInProgress = getEditedRecipe(state.recipe.title);
    if (editedRecipeInProgress == null) {
      emit(
        RecipeStateEditMode(
          recipe: state.recipe,
          editedRecipe: state.recipe,
          labelColors: state.labelColors,
          completedIngredients: state.completedIngredients,
          completedSteps: state.completedSteps,
        ),
      );
    } else {
      emit(
        RecipeStateEditModeRestored(
          recipe: state.recipe,
          editedRecipe: editedRecipeInProgress,
          labelColors: state.labelColors,
          completedIngredients: state.completedIngredients,
          completedSteps: state.completedSteps,
        ),
      );
    }
  }

  Future<void> saveEdits() async {
    final Recipe editedRecipeFinished = state.editedRecipe!.copyWith(
      blankElementsRemoved: true,
      elementsTrimmed: true,
    );

    await _storageInterface.removeFromRecipes(state.recipe);
    await _storageInterface.addToRecipes(editedRecipeFinished);

    editedRecipes.remove(state.recipe.title);

    emit(
      RecipeState(
        editedRecipeFinished,
        labelColors: state.labelColors,
        completedIngredients: state.completedIngredients,
        completedSteps: state.completedSteps,
      ),
    );
  }

  void toggleIngredientCompleted(Ingredient ingredient, {required bool newValue}) {
    final List<Ingredient> updatedCompletedIngredients = state.completedIngredients.toList();

    if (newValue == true) {
      updatedCompletedIngredients.add(ingredient);
    } else {
      updatedCompletedIngredients.remove(ingredient);
    }

    emit(
      RecipeState(
        state.recipe,
        labelColors: state.labelColors,
        completedIngredients: updatedCompletedIngredients,
        completedSteps: state.completedSteps,
      ),
    );
  }

  void toggleStepCompleted(String step, {required bool newValue}) {
    final List<String> updatedCompletedSteps = state.completedSteps.toList();

    if (newValue == true) {
      updatedCompletedSteps.add(step);
    } else {
      updatedCompletedSteps.remove(step);
    }

    emit(
      RecipeState(
        state.recipe,
        labelColors: state.labelColors,
        completedIngredients: state.completedIngredients,
        completedSteps: updatedCompletedSteps,
      ),
    );
  }

  void reorderIngredient(int oldIndex, int newIndex) {
    final List<Ingredient> updatedIngredients = state.editedRecipe!.ingredients.toList();
    if (oldIndex > newIndex) {
      final Ingredient removedIngredient = updatedIngredients.removeAt(oldIndex);
      updatedIngredients.insert(newIndex, removedIngredient);
    } else {
      updatedIngredients.insert(newIndex, state.editedRecipe!.ingredients[oldIndex]);
      updatedIngredients.removeAt(oldIndex);
    }

    final Recipe updatedEditedRecipe = Recipe(
      state.editedRecipe!.title,
      labels: state.editedRecipe!.labels,
      ingredients: updatedIngredients,
      steps: state.editedRecipe!.steps,
      url: state.editedRecipe!.url,
    );

    emit(
      RecipeStateEditMode(
        recipe: state.recipe,
        editedRecipe: updatedEditedRecipe,
        labelColors: state.labelColors,
        completedIngredients: state.completedIngredients,
        completedSteps: state.completedSteps,
      ),
    );

    setEditedRecipe(state.recipe.title, updatedEditedRecipe);
  }

  void reorderStep(int oldIndex, int newIndex) {
    final List<String> updatedSteps = state.editedRecipe!.steps.toList();
    if (oldIndex > newIndex) {
      final String removedStep = updatedSteps.removeAt(oldIndex);
      updatedSteps.insert(newIndex, removedStep);
    } else {
      updatedSteps.insert(newIndex, state.editedRecipe!.steps[oldIndex]);
      updatedSteps.removeAt(oldIndex);
    }

    final Recipe updatedEditedRecipe = Recipe(
      state.editedRecipe!.title,
      labels: state.editedRecipe!.labels,
      ingredients: state.editedRecipe!.ingredients,
      steps: updatedSteps,
      url: state.editedRecipe!.url,
    );

    emit(
      RecipeStateEditMode(
        recipe: state.recipe,
        editedRecipe: updatedEditedRecipe,
        labelColors: state.labelColors,
        completedIngredients: state.completedIngredients,
        completedSteps: state.completedSteps,
      ),
    );

    setEditedRecipe(state.recipe.title, updatedEditedRecipe);
  }

  void reorderLabel(int oldIndex, int newIndex) {
    final List<String> updatedLabels = state.editedRecipe!.labels.toList();
    if (oldIndex > newIndex) {
      final String removedLabel = updatedLabels.removeAt(oldIndex);
      updatedLabels.insert(newIndex, removedLabel);
    } else {
      updatedLabels.insert(newIndex, state.editedRecipe!.labels[oldIndex]);
      updatedLabels.removeAt(oldIndex);
    }

    final Recipe updatedEditedRecipe = Recipe(
      state.editedRecipe!.title,
      labels: updatedLabels,
      ingredients: state.editedRecipe!.ingredients,
      steps: state.editedRecipe!.steps,
      url: state.editedRecipe!.url,
    );

    emit(
      RecipeStateEditMode(
        recipe: state.recipe,
        editedRecipe: updatedEditedRecipe,
        labelColors: state.labelColors,
        completedIngredients: state.completedIngredients,
        completedSteps: state.completedSteps,
      ),
    );

    setEditedRecipe(state.recipe.title, updatedEditedRecipe);
  }

  void updateUrl(String newUrl) {
    final Recipe updatedEditedRecipe = Recipe(
      state.editedRecipe!.title,
      labels: state.editedRecipe!.labels,
      ingredients: state.editedRecipe!.ingredients,
      steps: state.editedRecipe!.steps,
      url: newUrl,
    );

    emit(
      RecipeStateEditMode(
        recipe: state.recipe,
        editedRecipe: updatedEditedRecipe,
        labelColors: state.labelColors,
        completedIngredients: state.completedIngredients,
        completedSteps: state.completedSteps,
      ),
    );

    setEditedRecipe(state.recipe.title, updatedEditedRecipe);
  }

  Future<void> copyUrlToClipboard() async {
    return await _clipboardInterface.copyTextToClipboard(
      state.recipe.url.toString(),
    );
  }

  void updateTitle(String newTitle) {
    final Recipe updatedEditedRecipe = Recipe(
      newTitle,
      labels: state.editedRecipe!.labels,
      ingredients: state.editedRecipe!.ingredients,
      steps: state.editedRecipe!.steps,
      url: state.editedRecipe!.url,
    );

    emit(
      RecipeStateEditMode(
        recipe: state.recipe,
        editedRecipe: updatedEditedRecipe,
        labelColors: state.labelColors,
        completedIngredients: state.completedIngredients,
        completedSteps: state.completedSteps,
      ),
    );

    setEditedRecipe(state.recipe.title, updatedEditedRecipe);
  }

  void updateIngredient(Ingredient newIngredient, {required int ingredientIndex}) {
    final List<Ingredient> updatedIngredients = state.editedRecipe!.ingredients.toList();
    updatedIngredients[ingredientIndex] = newIngredient;

    final Recipe updatedEditedRecipe = Recipe(
      state.editedRecipe!.title,
      labels: state.editedRecipe!.labels,
      ingredients: updatedIngredients,
      steps: state.editedRecipe!.steps,
      url: state.editedRecipe!.url,
    );

    final List<Ingredient> updatedCompletedIngredients = state.completedIngredients.toList();
    final Ingredient oldIngredient = state.editedRecipe!.ingredients[ingredientIndex];
    final bool wasRemoved = updatedCompletedIngredients.remove(oldIngredient);
    if (wasRemoved) {
      updatedCompletedIngredients.add(newIngredient);
    }

    emit(
      RecipeStateEditMode(
        recipe: state.recipe,
        editedRecipe: updatedEditedRecipe,
        labelColors: state.labelColors,
        completedIngredients: updatedCompletedIngredients,
        completedSteps: state.completedSteps,
      ),
    );

    setEditedRecipe(state.recipe.title, updatedEditedRecipe);
  }

  void updateStep(String newStepText, {required int stepIndex}) {
    final List<String> updatedSteps = state.editedRecipe!.steps.toList();
    updatedSteps[stepIndex] = newStepText;

    final Recipe updatedEditedRecipe = Recipe(
      state.editedRecipe!.title,
      labels: state.editedRecipe!.labels,
      ingredients: state.editedRecipe!.ingredients,
      steps: updatedSteps,
      url: state.editedRecipe!.url,
    );

    final List<String> updatedCompletedSteps = state.completedSteps.toList();
    final String oldStepText = state.editedRecipe!.steps[stepIndex];
    final bool wasRemoved = updatedCompletedSteps.remove(oldStepText);
    if (wasRemoved) {
      updatedCompletedSteps.add(newStepText);
    }

    emit(
      RecipeStateEditMode(
        recipe: state.recipe,
        editedRecipe: updatedEditedRecipe,
        labelColors: state.labelColors,
        completedIngredients: state.completedIngredients,
        completedSteps: updatedCompletedSteps,
      ),
    );

    setEditedRecipe(state.recipe.title, updatedEditedRecipe);
  }

  void updateLabelColor(String label, int newColorValue) {
    /// this is not widget-tested; dropdowns are impossible to test properly

    final List<LabelColor> updatedLabelColors = state.labelColors.toList();
    updatedLabelColors.setColor(label, Color(newColorValue));

    final List<LabelColor> globalLabelColors = _storageInterface.getLabelColors();
    globalLabelColors.setColor(label, Color(newColorValue));
    _storageInterface.storeLabelColors(globalLabelColors);

    emit(
      RecipeStateEditMode(
        recipe: state.recipe,
        editedRecipe: state.editedRecipe!,
        labelColors: updatedLabelColors,
        completedIngredients: state.completedIngredients,
        completedSteps: state.completedSteps,
      ),
    );
  }

  void updateLabelText(String newLabelText, {required int labelAtIndex}) {
    final List<String> updatedLabels = state.editedRecipe!.labels.toList();
    updatedLabels[labelAtIndex] = newLabelText;

    final String oldLabelText = state.editedRecipe!.labels[labelAtIndex];
    final Color colorCarriedOver = state.labelColors.getColor(oldLabelText);
    final List<LabelColor> updatedLabelColors = state.labelColors.toList();
    updatedLabelColors.add(LabelColor(newLabelText, colorCarriedOver));

    final Recipe updatedEditedRecipe = Recipe(
      state.editedRecipe!.title,
      labels: updatedLabels,
      ingredients: state.editedRecipe!.ingredients,
      steps: state.editedRecipe!.steps,
      url: state.editedRecipe!.url,
    );

    emit(
      RecipeStateEditMode(
        recipe: state.recipe,
        editedRecipe: updatedEditedRecipe,
        labelColors: updatedLabelColors,
        completedIngredients: state.completedIngredients,
        completedSteps: state.completedSteps,
      ),
    );

    setEditedRecipe(state.recipe.title, updatedEditedRecipe);
  }

  void deleteLabel(int labelIndex) {
    final String labelText = state.editedRecipe!.labels[labelIndex];

    final List<LabelColor> updatedLabelColors = state.labelColors.toList();
    updatedLabelColors.removeWhere((labelColor) => labelColor.label == labelText);

    final List<String> updatedLabels = state.editedRecipe!.labels.toList();
    updatedLabels.removeAt(labelIndex);

    final Recipe updatedEditedRecipe = Recipe(
      state.editedRecipe!.title,
      labels: updatedLabels,
      ingredients: state.editedRecipe!.ingredients,
      steps: state.editedRecipe!.steps,
      url: state.editedRecipe!.url,
    );

    emit(
      RecipeStateEditModeDeletedLabel(
        recipe: state.recipe,
        editedRecipe: updatedEditedRecipe,
        labelColors: updatedLabelColors,
        completedIngredients: state.completedIngredients,
        completedSteps: state.completedSteps,
      ),
    );

    setEditedRecipe(state.recipe.title, updatedEditedRecipe);
  }

  void addIngredient() {
    emit(
      RecipeStateEditModeAddIngredient(
        recipe: state.recipe,
        editedRecipe: state.editedRecipe!,
        labelColors: state.labelColors,
        completedIngredients: state.completedIngredients,
        completedSteps: state.completedSteps,
      ),
    );
  }

  void addStep() {
    emit(
      RecipeStateEditModeAddStep(
        recipe: state.recipe,
        editedRecipe: state.editedRecipe!,
        labelColors: state.labelColors,
        completedIngredients: state.completedIngredients,
        completedSteps: state.completedSteps,
      ),
    );
  }

  void addLabel() {
    emit(
      RecipeStateEditModeAddLabel(
        recipe: state.recipe,
        editedRecipe: state.editedRecipe!,
        labelColors: state.labelColors,
        completedIngredients: state.completedIngredients,
        completedSteps: state.completedSteps,
      ),
    );
  }

  void startFromScratch() {
    emit(
      RecipeStateEditMode(
        recipe: Recipe.blank,
        editedRecipe: Recipe("", labels: const [""], ingredients: const [Ingredient.empty], steps: const [""]),
        labelColors: state.labelColors,
      ),
    );
  }

  Future<void> pasteFromClipboard() async {
    emit(
      RecipeStateProcessingClipboardData(
        labelColors: state.labelColors,
      ),
    );
    final String? text = await _clipboardInterface.getCopiedText();
    if (text == null) {
      emit(
        RecipeStateNoClipboardDataToPaste(
          labelColors: state.labelColors,
        ),
      );
    } else {
      late final Recipe recipeFromText;
      final Uri? uriFromText = _webInterface.uriFromText(text);
      if (uriFromText == null) {
        try {
          recipeFromText = Recipe.fromString(text);
        } catch (e) {
          emit(
            RecipeStateUnableToParseRecipeFromClipboardText(
              labelColors: state.labelColors,
            ),
          );
          return;
        }
      } else {
        late final String responseBody;
        try {
          responseBody = (await _webInterface.httpGet(uriFromText)).body;
        } catch (exception) {
          emit(
            RecipeStateFailedHttpGetRequest(
              labelColors: state.labelColors,
              failure: exception.toString(),
            ),
          );
          return;
        }

        final Recipe? parsedRecipe = await _webInterface.parseRecipeFromHttpResponseBody(responseBody);
        if (parsedRecipe == null) {
          emit(
            RecipeStateUnableToParseRecipeFromHttpResponseBodyError(
              labelColors: state.labelColors,
            ),
          );
          return;
        } else {
          recipeFromText = parsedRecipe;
        }
      }

      emit(
        RecipeStateEditMode(
          recipe: Recipe.blank,
          editedRecipe: recipeFromText,
          labelColors: state.labelColors,
        ),
      );
    }
  }

  Iterable<String> getMatchingLabels(String pattern) {
    final String patternTrimmed = pattern.trim().toLowerCase();
    final List<String> allLabels = _storageInterface.getLabelColors().labels;

    return allLabels.where((label) {
      return label.trim().isNotEmpty && label.toLowerCase().contains(patternTrimmed);
    });
  }
}
