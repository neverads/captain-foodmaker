import 'package:captainfoodmaker/data_models/recipe.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

const List<Color> selectableLabelColors = [
  Colors.blue,
  Colors.indigo,
  Colors.deepPurple,
  Colors.purple,
  Colors.pink,
  Colors.red,
  Colors.deepOrange,
  Colors.orange,
  Colors.amber,
  Colors.yellow,
  Colors.lime,
  Colors.lightGreen,
  Colors.green,
  Colors.teal,
  Colors.cyan,
  Colors.lightBlue,
  Colors.blueGrey,
  Colors.brown,
  Colors.grey
];

Map<String, Recipe> editedRecipes = {};

Recipe? getEditedRecipe(String uneditedTitle) => editedRecipes[uneditedTitle];

void setEditedRecipe(String uneditedTitle, Recipe editedRecipe) => editedRecipes[uneditedTitle] = editedRecipe;

final List<CompletedIngredient> sessionCompletedIngredients = [];

class CompletedIngredient extends Equatable {
  final Ingredient ingredient;
  final String recipeTitle;

  const CompletedIngredient(
    this.ingredient, {
    required this.recipeTitle,
  });

  @override
  List<Object?> get props => [ingredient, recipeTitle];
}

final List<CompletedStep> sessionCompletedSteps = [];

class CompletedStep extends Equatable {
  final String step;
  final String recipeTitle;

  const CompletedStep(
    this.step, {
    required this.recipeTitle,
  });

  @override
  List<Object?> get props => [step, recipeTitle];
}
