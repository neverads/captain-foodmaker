import 'package:captainfoodmaker/data_models/recipe.dart';
import 'package:captainfoodmaker/interfaces/hive_storage_interface.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'deleted_recipes_state.dart';

class DeletedRecipesCubit extends Cubit<DeletedRecipesState> {
  final HiveStorageInterface _hiveStorageInterface;

  DeletedRecipesCubit(
    this._hiveStorageInterface,
  ) : super(
          DeletedRecipesStateInitial(
            _hiveStorageInterface.getDeletedRecipes(),
          ),
        );

  Future<void> unDeleteRecipe(Recipe recipe) async {
    _setNewStatusForRecipe(
      DeletionStatus.restored,
      recipe: recipe,
    );
    await _hiveStorageInterface.setDeletionStatusForRecipe(recipe, newDeletionStatus: false);
  }

  void reDeleteRecipe(Recipe recipe) {
    _setNewStatusForRecipe(
      DeletionStatus.softDeleted,
      recipe: recipe,
    );
    _hiveStorageInterface.setDeletionStatusForRecipe(recipe, newDeletionStatus: true);
  }

  Future<void> foreverDeleteRecipe(Recipe recipe) async {
    _setNewStatusForRecipe(
      DeletionStatus.foreverDeleted,
      recipe: recipe,
    );
    await _hiveStorageInterface.removeFromRecipes(recipe);
  }

  Future<void> unForeverDeleteRecipe(Recipe recipe) async {
    _setNewStatusForRecipe(
      DeletionStatus.softDeleted,
      recipe: recipe,
    );
    await _hiveStorageInterface.addToRecipes(recipe);
  }

  void _setNewStatusForRecipe(DeletionStatus newStatus, {required Recipe recipe}) {
    final int index = state.deletedRecipes.indexOf(recipe);
    if (index >= 0) {
      final List<DeletionStatus> newStatuses = state.statuses.map((status) => status).toList();
      newStatuses[index] = newStatus;
      emit(
        DeletedRecipesState(
          state.deletedRecipes,
          statuses: newStatuses,
        ),
      );
    }
  }
}
