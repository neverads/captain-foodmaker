part of 'deleted_recipes_cubit.dart';

class DeletedRecipesState extends Equatable {
  final List<Recipe> deletedRecipes;
  final List<DeletionStatus> statuses;

  const DeletedRecipesState(
    this.deletedRecipes, {
    required this.statuses,
  });

  @override
  List<Object?> get props => [statuses];

  DeletionStatus statusForRecipe(Recipe recipe) {
    final int index = deletedRecipes.indexOf(recipe);
    if (index >= 0) {
      return statuses[index];
    } else {
      // this should never be needed; here just to make it fool-proof
      return DeletionStatus.softDeleted;
    }
  }
}

class DeletedRecipesStateInitial extends DeletedRecipesState {
  DeletedRecipesStateInitial(List<Recipe> deletedRecipes)
      : super(
          deletedRecipes,
          statuses: deletedRecipes.map((_) {
            return DeletionStatus.softDeleted;
          }).toList(),
        );
}

enum DeletionStatus {
  softDeleted,
  restored,
  foreverDeleted,
}
