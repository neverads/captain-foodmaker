import 'package:captainfoodmaker/data_models/recipe.dart';
import 'package:captainfoodmaker/deleted_recipes/deleted_recipes_cubit.dart';
import 'package:captainfoodmaker/interfaces/hive_storage_interface.dart';
import 'package:captainfoodmaker/shared_widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class DeletedRecipesScreen extends StatelessWidget {
  const DeletedRecipesScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (BuildContext context) {
        return DeletedRecipesCubit(
          context.read<HiveStorageInterface>(),
        );
      },
      child: const DeletedRecipesView(),
    );
  }
}

class DeletedRecipesView extends StatelessWidget {
  const DeletedRecipesView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<DeletedRecipesCubit, DeletedRecipesState>(
      builder: (BuildContext context, state) {
        return Scaffold(
          appBar: AppBar(
            title: const Text("Deleted recipes"),
          ),
          body: state.deletedRecipes.isEmpty
              ? const Center(
                  child: Text("No deleted recipes"),
                )
              : DeletedRecipesListView(state.deletedRecipes.length),
        );
      },
    );
  }
}

class DeletedRecipesListView extends PaddedListView {
  DeletedRecipesListView(
    int recipesCount, {
    Key? key,
  }) : super(
          key: key,
          itemCount: recipesCount,
          itemBuilder: (BuildContext context, int index) {
            return DeletedRecipeListTile(index: index);
          },
        );
}

class DeletedRecipeListTile extends StatelessWidget {
  final int index;

  const DeletedRecipeListTile({
    required this.index,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<DeletedRecipesCubit, DeletedRecipesState>(
      buildWhen: (previousState, currentState) {
        return previousState.statuses[index] != currentState.statuses[index];
      },
      builder: (BuildContext context, state) {
        final Recipe recipe = state.deletedRecipes[index];
        final DeletionStatus deletionStatus = state.statuses[index];

        final DeletedRecipesCubit cubit = context.read<DeletedRecipesCubit>();

        final MaterialStateProperty<Color> colorRestored = MaterialStateProperty.all(
          Theme.of(context).colorScheme.tertiary,
        );
        final MaterialStateProperty<Color> colorForeverDeleted = MaterialStateProperty.all(
          Theme.of(context).colorScheme.error,
        );

        late final String subtitle;
        late final TextStyle? subtitleStyle;
        if (deletionStatus == DeletionStatus.restored) {
          subtitle = "Restored to the main screen";
          subtitleStyle = Theme.of(context).textTheme.bodyLarge?.copyWith(
                color: Theme.of(context).colorScheme.onSurfaceVariant,
              );
        } else if (deletionStatus == DeletionStatus.foreverDeleted) {
          subtitle = "Deleted forever from your device";
          subtitleStyle = Theme.of(context).textTheme.bodyLarge?.copyWith(
                color: Theme.of(context).colorScheme.error,
              );
        } else {
          subtitle = "Removed from the main screen";
          subtitleStyle = Theme.of(context).textTheme.bodyMedium?.copyWith(
                color: Theme.of(context).colorScheme.onSurface.withOpacity(0.7),
              );
        }

        return ListTile(
          title: Text(recipe.title),
          subtitle: Text(
            subtitle,
            style: subtitleStyle,
          ),
          trailing: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              deletionStatus == DeletionStatus.restored
                  ? ElevatedButton(
                      child: const Icon(Icons.restore_from_trash_outlined),
                      style: ButtonStyle(backgroundColor: colorRestored),
                      onPressed: () => cubit.reDeleteRecipe(recipe),
                    )
                  : OutlinedButton(
                      child: const Icon(Icons.restore_from_trash_outlined),
                      style: ButtonStyle(foregroundColor: colorRestored),
                      onPressed: () => cubit.unDeleteRecipe(recipe),
                    ),
              const SizedBox(width: 8),
              deletionStatus == DeletionStatus.foreverDeleted
                  ? ElevatedButton(
                      child: const Icon(Icons.delete_forever_outlined),
                      style: ButtonStyle(backgroundColor: colorForeverDeleted),
                      onPressed: () async => await cubit.unForeverDeleteRecipe(recipe),
                    )
                  : OutlinedButton(
                      child: const Icon(Icons.delete_forever_outlined),
                      style: ButtonStyle(foregroundColor: colorForeverDeleted),
                      onPressed: () async => await cubit.foreverDeleteRecipe(recipe),
                    ),
            ],
          ),
        );
      },
    );
  }
}
