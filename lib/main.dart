import 'package:captainfoodmaker/data_models/label_colors.dart';
import 'package:captainfoodmaker/data_models/recipe.dart';
import 'package:captainfoodmaker/home/home_screen.dart';
import 'package:captainfoodmaker/interfaces/clipboard_interface.dart';
import 'package:captainfoodmaker/interfaces/hive_storage_interface.dart';
import 'package:captainfoodmaker/interfaces/web_interface.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final storageDirectory = await getApplicationDocumentsDirectory();
  await Hive.initFlutter(storageDirectory.path);
  final HiveStorageInterface storageInterface = HiveStorageInterface();
  await storageInterface.openStorageBox();

  // move data from shared-prefs to hive
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  try {
    // get labels from prefs
    const String prefsKeyLabelTitles = "prefsKeyLabelTitles";
    final List<String>? labelsAsStringsFromPrefs = prefs.getStringList(prefsKeyLabelTitles);
    if (labelsAsStringsFromPrefs != null && labelsAsStringsFromPrefs.isNotEmpty) {
      final List<LabelColor> labelColorsFromPrefs = labelsAsStringsFromPrefs.map((string) {
        return LabelColor(string, Colors.grey);
      }).toList();

      // save to hive
      final List<LabelColor> labelColorsFromHive = storageInterface.getLabelColors().toList();
      final List<LabelColor> allLabelColors = labelColorsFromPrefs + labelColorsFromHive;
      await storageInterface.storeLabelColors(allLabelColors);

      // delete from prefs
      await prefs.remove(prefsKeyLabelTitles);
    }
  } catch (_) {}
  try {
    // get recipes from prefs
    const String prefsKeySavedRecipes = "prefsKeySavedRecipes";
    final List<String>? savedRecipesStrings = prefs.getStringList(prefsKeySavedRecipes);
    if (savedRecipesStrings != null && savedRecipesStrings.isNotEmpty) {
      final List<Recipe> recipesFromPrefs = savedRecipesStrings.map((string) {
        final List<String> split = string.split('__');

        final String title = split.first;

        final List<String> labels = split[1].split('||').where((it) => it.isNotEmpty).toList();

        final List<String> ingredientsAsPrefsStrings = split[2].split('##').where((it) => it.isNotEmpty).toList();
        final List<Ingredient> ingredients = ingredientsAsPrefsStrings.map((str) {
          final List<String> split = str.split('^^');
          return Ingredient(split.first, split.last);
        }).toList();

        final List<String> steps = split[3].split('@@').where((it) => it.isNotEmpty).toList();
        final String? url = split.length < 5 ? null : split[4];

        return Recipe(
          title,
          labels: labels,
          ingredients: ingredients,
          steps: steps,
          url: url,
        );
      }).toList();

      // save to hive
      final List<Recipe> recipesFromHive = storageInterface.getRecipes().toList();
      final Iterable<String> titlesOfRecipesFromPrefs = recipesFromPrefs.map((recipe) => recipe.title);
      recipesFromHive.removeWhere((recipe) => titlesOfRecipesFromPrefs.contains(recipe.title));
      final List<Recipe> allRecipes = recipesFromPrefs + recipesFromHive;
      allRecipes.sort((a, b) => a.title.toLowerCase().compareTo(b.title.toLowerCase()));
      await storageInterface.storeRecipes(allRecipes.map((recipe) => recipe.toString()).toList());

      // delete from prefs
      await prefs.remove(prefsKeySavedRecipes);
    }
  } catch (_) {}

  runApp(
    CaptainFoodmakerApp(
      storageInterface: storageInterface,
      clipboardInterface: const ClipboardInterface(),
      webInterface: const WebInterface(),
    ),
  );
}

class CaptainFoodmakerApp extends StatelessWidget {
  final HiveStorageInterface storageInterface;
  final ClipboardInterface clipboardInterface;
  final WebInterface webInterface;

  const CaptainFoodmakerApp({
    Key? key,
    required this.storageInterface,
    required this.clipboardInterface,
    required this.webInterface,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const String appTitle = "Captain Foodmaker";

    return MultiRepositoryProvider(
      providers: [
        RepositoryProvider.value(value: storageInterface),
        RepositoryProvider.value(value: clipboardInterface),
        RepositoryProvider.value(value: webInterface),
      ],
      child: MaterialApp(
        title: appTitle,
        theme: themeData,
        darkTheme: darkThemeData,
        home: const HomeScreen(title: appTitle),
      ),
    );
  }

  ThemeData get themeData {
    const MaterialColor primaryColor = Colors.teal;

    return ThemeData(
      colorScheme: ColorScheme(
        brightness: Brightness.light,
        primary: primaryColor.shade800,
        onPrimary: Colors.white,
        secondary: Colors.tealAccent,
        onSecondary: Colors.grey.shade800,
        tertiary: primaryColor,
        error: Colors.redAccent,
        onError: Colors.grey.shade800,
        background: Colors.white,
        onBackground: Colors.grey.shade800,
        surface: Colors.grey.shade300,
        onSurface: Colors.grey.shade800,
        onSurfaceVariant: primaryColor,
        onPrimaryContainer: Colors.black87,
        onTertiaryContainer: Colors.white70,
      ),
      cardTheme: const CardTheme(elevation: 0),
      checkboxTheme: CheckboxThemeData(
        fillColor: MaterialStateProperty.all(primaryColor),
        checkColor: MaterialStateProperty.all(Colors.white),
      ),
    );
  }

  ThemeData get darkThemeData {
    const MaterialColor primaryColor = Colors.teal;
    final Color accentColor = Colors.tealAccent.shade100;
    final Color accentColorLight = accentColor.withOpacity(0.8);

    return ThemeData(
      colorScheme: ColorScheme(
        brightness: Brightness.dark,
        primary: primaryColor.shade800,
        onPrimary: Colors.white,
        secondary: accentColorLight,
        onSecondary: Colors.grey.shade800,
        tertiary: primaryColor,
        error: Colors.redAccent,
        onError: Colors.grey.shade800,
        background: Colors.white,
        onBackground: Colors.grey.shade800,
        surface: primaryColor.shade800,
        onSurface: Colors.white,
        onSurfaceVariant: accentColor,
        onPrimaryContainer: Colors.black,
        onTertiaryContainer: Colors.white38,
      ),
      cardTheme: const CardTheme(elevation: 0),
      checkboxTheme: CheckboxThemeData(
        fillColor: MaterialStateProperty.all(accentColorLight),
        checkColor: MaterialStateProperty.all(Colors.black87),
      ),
    );
  }
}
