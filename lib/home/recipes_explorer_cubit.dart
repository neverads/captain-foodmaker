import 'package:captainfoodmaker/data_models/label_colors.dart';
import 'package:captainfoodmaker/data_models/recipe.dart';
import 'package:captainfoodmaker/interfaces/hive_storage_interface.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'recipes_explorer_state.dart';

class RecipesExplorerCubit extends Cubit<RecipesExplorerState> {
  final HiveStorageInterface _storageInterface;

  RecipesExplorerCubit(
    this._storageInterface,
  ) : super(
          RecipesExplorerState(
            recipes: _storageInterface.getRecipes(excludeDeletedRecipes: true),
            labelColors: _storageInterface.getLabelColors(),
            searchText: null,
          ),
        );

  void enableSearch() {
    emit(
      RecipesExplorerState(
        recipes: state.recipes,
        labelColors: state.labelColors,
        searchText: "",
      ),
    );
  }

  void disableSearch() {
    emit(
      RecipesExplorerState(
        recipes: _storageInterface.getRecipes(excludeDeletedRecipes: true),
        labelColors: state.labelColors,
        searchText: null,
      ),
    );
  }

  void updateSearch(String newSearchText) {
    final List<Recipe> relevantRecipes = _storageInterface.getRecipes(excludeDeletedRecipes: true).where((recipe) {
      return recipe.title.toLowerCase().contains(newSearchText.toLowerCase());
    }).toList();

    emit(
      RecipesExplorerState(
        recipes: relevantRecipes,
        labelColors: state.labelColors,
        searchText: newSearchText,
      ),
    );
  }

  void updateWithFreshLabelsAndColors() {
    emit(
      RecipesExplorerState(
        recipes: state.recipes,
        labelColors: _storageInterface.getLabelColors(),
        searchText: state.searchText,
      ),
    );
  }

  bool updateIfRecipesWereUpdated() {
    /// Returns true if emitting a new state; false if no emit.

    List<Recipe> recipesFromStorage = _storageInterface.getRecipes(excludeDeletedRecipes: true);
    final bool shouldUpdateState = state.recipes != recipesFromStorage;
    if (shouldUpdateState) {
      emit(
        RecipesExplorerState(
          recipes: recipesFromStorage,
          labelColors: _storageInterface.getLabelColors(),
          searchText: state.searchText,
        ),
      );
    }
    return shouldUpdateState;
  }

  void navigateToDeletedRecipesScreen() {
    emit(
      RecipesExplorerStateNavigateToDeletedRecipesScreen.fromState(state),
    );
  }

  void navigateToRecipeScreenToAddNewRecipe() {
    emit(
      RecipesExplorerStateNavigateToScreenAddNewRecipe.fromState(state),
    );
  }
}
