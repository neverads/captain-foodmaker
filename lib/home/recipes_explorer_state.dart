part of 'recipes_explorer_cubit.dart';

class RecipesExplorerState extends Equatable {
  final List<Recipe> recipes;
  final List<LabelColor> labelColors; // todo -- create & use a LabelsWithColors class
  final String? searchText;

  const RecipesExplorerState({
    required this.recipes,
    required this.labelColors,
    required this.searchText,
  });

  @override
  List<Object?> get props {
    final List<Object?> _props = [];
    _props.addAll(recipes);
    _props.addAll(labelColors);
    _props.add(searchText);
    return _props;
  }

  List<String> get labels {
    return labelColors.labels;
  }

  Iterable<int> get labelsIndexesPlus1 {
    return Iterable<int>.generate(labels.length + 1);
  }
}

abstract class RecipesExplorerStateNavigateToScreen extends RecipesExplorerState {
  const RecipesExplorerStateNavigateToScreen({
    required List<Recipe> recipes,
    required List<LabelColor> labelColors,
    required String? searchText,
  }) : super(
          recipes: recipes,
          labelColors: labelColors,
          searchText: searchText,
        );
}

class RecipesExplorerStateNavigateToDeletedRecipesScreen extends RecipesExplorerStateNavigateToScreen {
  const RecipesExplorerStateNavigateToDeletedRecipesScreen({
    required List<Recipe> recipes,
    required List<LabelColor> labelColors,
    required String? searchText,
  }) : super(
          recipes: recipes,
          labelColors: labelColors,
          searchText: searchText,
        );

  factory RecipesExplorerStateNavigateToDeletedRecipesScreen.fromState(RecipesExplorerState state) {
    return RecipesExplorerStateNavigateToDeletedRecipesScreen(
      recipes: state.recipes,
      labelColors: state.labelColors,
      searchText: state.searchText,
    );
  }
}

class RecipesExplorerStateNavigateToScreenAddNewRecipe extends RecipesExplorerStateNavigateToScreen {
  const RecipesExplorerStateNavigateToScreenAddNewRecipe({
    required List<Recipe> recipes,
    required List<LabelColor> labelColors,
    required String? searchText,
  }) : super(
          recipes: recipes,
          labelColors: labelColors,
          searchText: searchText,
        );

  factory RecipesExplorerStateNavigateToScreenAddNewRecipe.fromState(RecipesExplorerState state) {
    return RecipesExplorerStateNavigateToScreenAddNewRecipe(
      recipes: state.recipes,
      labelColors: state.labelColors,
      searchText: state.searchText,
    );
  }
}
