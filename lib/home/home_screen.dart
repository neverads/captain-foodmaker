import 'package:captainfoodmaker/data_models/label_colors.dart';
import 'package:captainfoodmaker/data_models/recipe.dart';
import 'package:captainfoodmaker/deleted_recipes/deleted_recipes_screen.dart';
import 'package:captainfoodmaker/edit_labels/edit_labels_screen.dart';
import 'package:captainfoodmaker/export/export_recipes_screen.dart';
import 'package:captainfoodmaker/home/recipes_explorer_cubit.dart';
import 'package:captainfoodmaker/interfaces/hive_storage_interface.dart';
import 'package:captainfoodmaker/recipe/recipe_screen.dart';
import 'package:captainfoodmaker/shared_widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomeScreen extends StatelessWidget {
  final String title;

  const HomeScreen({
    Key? key,
    required this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (BuildContext context) {
        return RecipesExplorerCubit(
          RepositoryProvider.of<HiveStorageInterface>(context),
        );
      },
      child: HomeView(title: title),
    );
  }
}

class HomeView extends StatelessWidget {
  final String title;

  const HomeView({Key? key, required this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final RecipesExplorerCubit cubit = context.read<RecipesExplorerCubit>();

    return BlocConsumer<RecipesExplorerCubit, RecipesExplorerState>(
      buildWhen: (previous, current) {
        return current is! RecipesExplorerStateNavigateToScreen;
      },
      builder: (context, state) {
        final bool isSearching = state.searchText != null;
        final List<Widget> tabs = [];
        tabs.add(
          const Tab(
            icon: Icon(Icons.sort_by_alpha),
          ),
        );
        tabs.addAll(
          state.labels.map((label) {
            return LabelTab(
              label,
              iconColor: state.labelColors.getColor(label),
            );
          }),
        );

        return DefaultTabController(
          length: tabs.length,
          child: Scaffold(
            appBar: AppBar(
              title: isSearching
                  ? SearchField(
                      text: state.searchText!,
                    )
                  : Text(title),
              actions: [
                isSearching
                    ? IconButton(
                        onPressed: cubit.disableSearch,
                        icon: const Icon(Icons.clear),
                      )
                    : IconButton(
                        onPressed: cubit.enableSearch,
                        icon: const Icon(Icons.search),
                      ),
                PopupMenuButton(
                  itemBuilder: (BuildContext context) {
                    return <PopupMenuEntry<String>>[
                      PopupMenuItemAddNewRecipe(),
                      PopupMenuItemExport(),
                      PopupMenuItemEditLabels(),
                      PopupMenuItemDeletedRecipes(),
                    ];
                  },
                  onSelected: (value) async {
                    if (value == PopupMenuItemAddNewRecipe.valueString) {
                      cubit.navigateToRecipeScreenToAddNewRecipe();
                    } else if (value == PopupMenuItemExport.valueString) {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) {
                            return ExportRecipesScreen(
                              state.recipes,
                              labelColors: state.labelColors,
                            );
                          },
                        ),
                      );
                    } else if (value == PopupMenuItemEditLabels.valueString) {
                      await Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) {
                            return EditLabelsScreen(
                              state.labels,
                              labelColors: state.labelColors.map((labelColor) => labelColor.copy()).toList(),
                            );
                          },
                        ),
                      );
                      cubit.updateWithFreshLabelsAndColors();
                      cubit.updateIfRecipesWereUpdated();
                    } else if (value == PopupMenuItemDeletedRecipes.valueString) {
                      cubit.navigateToDeletedRecipesScreen();
                    }
                  },
                ),
              ],
              bottom: TabBar(
                tabs: tabs,
                indicatorColor: Colors.tealAccent,
                isScrollable: true,
              ),
            ),
            body: const HomeViewBody(),
          ),
        );
      },
      listenWhen: (RecipesExplorerState previousState, RecipesExplorerState currentState) {
        return currentState is RecipesExplorerStateNavigateToScreen;
      },
      listener: (context, state) async {
        if (state is RecipesExplorerStateNavigateToDeletedRecipesScreen) {
          await Navigator.of(context).push(
            MaterialPageRoute(builder: (context) {
              return const DeletedRecipesScreen();
            }),
          );
        } else if (state is RecipesExplorerStateNavigateToScreenAddNewRecipe) {
          await Navigator.of(context).push(
            MaterialPageRoute(builder: (context) {
              return RecipeScreen.withBlankRecipe(labelColors: state.labelColors);
            }),
          );
        }
        cubit.updateIfRecipesWereUpdated(); // untested
      },
    );
  }
}

class SearchField extends StatelessWidget {
  final String text;

  const SearchField({
    Key? key,
    required this.text,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final TextEditingController textController = TextEditingController(
      text: text,
    );
    // todo -- avoid moving to the end of the string if currently typing in the middle
    textController.selection = TextSelection.fromPosition(
      TextPosition(
        offset: textController.text.length,
      ),
    );
    final Color colorOnPrimary = Theme.of(context).colorScheme.onPrimary;

    return TextField(
      autofocus: true,
      controller: textController,
      onChanged: (newText) {
        final RecipesExplorerCubit cubit = context.read<RecipesExplorerCubit>();
        cubit.updateSearch(newText);
      },
      style: Theme.of(context).textTheme.titleMedium!.copyWith(
            color: colorOnPrimary,
          ),
      decoration: InputDecoration(
        // todo -- tap this icon to explain how the search-filtering works via a snack-bar
        icon: Icon(
          Icons.search,
          color: colorOnPrimary.withOpacity(0.5),
        ),
        focusedBorder: const UnderlineInputBorder(
          borderSide: BorderSide(color: Colors.black12),
        ),
      ),
      cursorColor: colorOnPrimary,
    );
  }
}

class LabelTab extends StatelessWidget {
  final String label;
  final Color? iconColor;

  const LabelTab(
    this.label, {
    Key? key,
    required this.iconColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Tab(
      child: Row(
        children: [
          Stack(
            alignment: AlignmentDirectional.center,
            children: [
              const Icon(
                Icons.label,
                size: 18,
                color: Colors.white,
              ),
              Icon(
                Icons.label,
                size: 16,
                color: (iconColor ?? Colors.grey).withOpacity(0.6),
              ),
            ],
          ),
          const SizedBox(width: 4),
          Text(label),
        ],
      ),
    );
  }
}

class HomeViewBody extends StatelessWidget {
  const HomeViewBody({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<RecipesExplorerCubit, RecipesExplorerState>(
      buildWhen: (previous, current) {
        return current is! RecipesExplorerStateNavigateToScreen;
      },
      builder: (context, state) {
        return TabBarView(
          children: state.labelsIndexesPlus1.map((tabBarViewIndex) {
            final Iterable<Recipe> relevantRecipes = tabBarViewIndex == 0
                ? state.recipes
                : state.recipes.where((recipe) {
                    final String labelForThisTab = state.labels[tabBarViewIndex - 1];
                    return recipe.labels.contains(labelForThisTab);
                  });
            return relevantRecipes.isEmpty
                ? ListTile(
                    title: Text(
                      state.searchText?.isEmpty ?? true
                          ? tabBarViewIndex == 0
                              ? '\n🙈   No recipes found.'
                              : '\n🙈   No recipes found with "${state.labels[tabBarViewIndex - 1]}" label.'
                          : tabBarViewIndex == 0
                              ? '\n🙈   No recipes found when searching "${state.searchText}".'
                              : '\n🙈   No recipes found with "${state.labels[tabBarViewIndex - 1]}" label when searching "${state.searchText}".',
                    ),
                  )
                : PaddedListView(
                    itemCount: relevantRecipes.length,
                    itemBuilder: (BuildContext context, int itemBuilderIndex) {
                      return RecipeListTile(
                        relevantRecipes.elementAt(itemBuilderIndex),
                        labelColors: state.labelColors,
                      );
                    },
                  );
          }).toList(),
        );
      },
    );
  }
}

class RecipeListTile extends Builder {
  final Recipe recipe;
  final List<LabelColor> labelColors;

  RecipeListTile(
    this.recipe, {
    Key? key,
    required this.labelColors,
  }) : super(
          key: key,
          builder: (BuildContext context) {
            return ListTile(
              key: key,
              title: Text(recipe.title),
              subtitle: LabelsRow(
                recipe.labels,
                labelColors: labelColors,
              ),
              onTap: () async {
                // todo -- move this to a bloc-listener & test it
                await Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) {
                      return RecipeScreen(
                        recipe,
                        labelColors: labelColors,
                      );
                    },
                  ),
                );
                if (context.read<RecipesExplorerCubit>().updateIfRecipesWereUpdated()) {
                  ScaffoldMessenger.of(context).clearSnackBars();
                }
              },
            );
          },
        );
}
