import 'package:captainfoodmaker/data_models/label_colors.dart';
import 'package:captainfoodmaker/edit_labels/edit_labels_cubit.dart';
import 'package:captainfoodmaker/interfaces/hive_storage_interface.dart';
import 'package:captainfoodmaker/shared_getters.dart';
import 'package:captainfoodmaker/shared_widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class EditLabelsScreen extends StatelessWidget {
  final List<String> labels;
  final List<LabelColor> labelColors;

  const EditLabelsScreen(
    this.labels, {
    Key? key,
    required this.labelColors,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (BuildContext context) {
        return EditLabelsCubit(
          context.read<HiveStorageInterface>(),
          labelColors: labelColors,
        );
      },
      child: const EditLabelsView(),
    );
  }
}

class EditLabelsView extends StatefulWidget {
  const EditLabelsView({Key? key}) : super(key: key);

  @override
  State<EditLabelsView> createState() => _EditLabelsViewState();
}

class _EditLabelsViewState extends State<EditLabelsView> {
  @override
  Widget build(BuildContext context) {
    final EditLabelsCubit cubit = context.read<EditLabelsCubit>();

    return Scaffold(
      appBar: AppBar(
        title: const Text("Edit labels"),
      ),
      body: BlocBuilder<EditLabelsCubit, EditLabelsState>(
        buildWhen: (previousState, currentState) {
          return previousState.labelColors.colorsValues == currentState.labelColors.colorsValues;
        },
        builder: (BuildContext context, state) {
          return ReorderableListView.builder(
            itemBuilder: (BuildContext context, int index) {
              final LabelColor labelColor = cubit.state.labelColors.elementAt(index);
              final String label = labelColor.label;
              final Color selectedColor = labelColor.color;
              return Container(
                key: ValueKey(label),
                decoration: LabelColorGradient(selectedColor),
                child: ListTile(
                  title: TextField(
                    controller: TextEditingController(text: label),
                    decoration: const InputDecoration(border: InputBorder.none),
                    cursorColor: Colors.grey,
                    onChanged: (newText) async {
                      return await cubit.updateLabelText(labelColor, newText);
                    },
                  ),
                  leading: DropdownButton<Color>(
                    value: Color(cubit.state.labelColors.elementAt(index).color.value),
                    underline: const SizedBox(),
                    items: selectableLabelColors.map((Color selectableColor) {
                      return DropdownMenuItem<Color>(
                        value: Color(selectableColor.value),
                        child: SizedBox(
                          width: 32,
                          height: 20,
                          child: Card(
                            margin: EdgeInsets.zero,
                            color: selectableColor.withOpacity(0.6),
                          ),
                        ),
                      );
                    }).toList(),
                    onChanged: (Color? newColor) {
                      if (newColor != null) {
                        // todo -- setting state here is an anti-pattern, but it was the easy quick fix :shrug:
                        setState(() {
                          cubit.updateLabelColors(
                            label,
                            newColor,
                          );
                        });
                      }
                    },
                  ),
                  trailing: const ReorderIconButton(),
                ),
              );
            },
            itemCount: cubit.state.labelColors.length,
            onReorder: cubit.reorder,
          );
        },
      ),
    );
  }
}
