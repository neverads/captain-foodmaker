part of 'edit_labels_cubit.dart';

class EditLabelsState extends Equatable {
  final List<LabelColor> labelColors;

  const EditLabelsState(this.labelColors);

  @override
  List<Object?> get props => [labelColors];
}
