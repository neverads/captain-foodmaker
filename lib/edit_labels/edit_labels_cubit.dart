import 'package:captainfoodmaker/data_models/label_colors.dart';
import 'package:captainfoodmaker/data_models/recipe.dart';
import 'package:captainfoodmaker/interfaces/hive_storage_interface.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'edit_labels_state.dart';

class EditLabelsCubit extends Cubit<EditLabelsState> {
  final HiveStorageInterface _hiveStorageInterface;

  EditLabelsCubit(
    this._hiveStorageInterface, {
    required List<LabelColor> labelColors,
  }) : super(
          EditLabelsState(
            labelColors,
          ),
        );

  void reorder(int oldIndex, int newIndex) {
    final List<LabelColor> reorderedLabels = state.labelColors;
    final LabelColor labelBeingReordered = reorderedLabels.removeAt(oldIndex);
    if (newIndex > reorderedLabels.length) {
      reorderedLabels.add(labelBeingReordered);
    } else {
      reorderedLabels.insert(newIndex, labelBeingReordered);
    }
    emit(
      EditLabelsState(reorderedLabels),
    );
    _hiveStorageInterface.storeLabelColors(reorderedLabels);
  }

  void updateLabelColors(String label, Color color) {
    final List<LabelColor> labelColors = state.labelColors.toList();
    labelColors.setColor(label, color);
    emit(
      EditLabelsState(labelColors),
    );
    _hiveStorageInterface.storeLabelColors(labelColors);
  }

  Future<void> updateLabelText(LabelColor labelColor, String newLabelText) async {
    final List<LabelColor> newLabelColors = [];
    for (var currentLabelColor in state.labelColors) {
      if (currentLabelColor == labelColor) {
        newLabelColors.add(
          LabelColor(
            newLabelText,
            currentLabelColor.color,
          ),
        );
      } else {
        newLabelColors.add(currentLabelColor);
      }
    }
    newLabelColors.removeWhere((labelColor) => labelColor.label.trim().isEmpty);

    emit(
      EditLabelsState(newLabelColors),
    );
    await _hiveStorageInterface.storeLabelColors(newLabelColors);

    final List<Recipe> updatedRecipes = [];
    for (var currentRecipe in _hiveStorageInterface.getRecipes()) {
      final int indexOfLabel = currentRecipe.labels.indexOf(labelColor.label);
      if (indexOfLabel >= 0) {
        currentRecipe.labels[indexOfLabel] = newLabelText;
      }
      updatedRecipes.add(currentRecipe);
    }
    return await _hiveStorageInterface.storeRecipes(
      updatedRecipes.map((recipe) => recipe.toString()).toList(),
    );
  }
}
