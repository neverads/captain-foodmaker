import 'package:captainfoodmaker/data_models/label_colors.dart';
import 'package:captainfoodmaker/data_models/recipe.dart';
import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';

class HiveStorageInterface {
  final HiveInterface _hiveApi;
  late Box _storageBox;

  HiveStorageInterface({HiveInterface? hiveApi}) : _hiveApi = hiveApi ?? Hive; // null for prod; mock-hive for testing

  static const String _generalStorageBoxName = "_generalStorageBoxName";
  static const String _generalStorageBoxKeyRecipes = "_generalStorageBoxKeyRecipes";
  static const String _generalStorageBoxKeyLabelColors = "_generalStorageBoxKeyLabelColors";

  Future<void> openStorageBox() async {
    await _hiveApi.openBox(_generalStorageBoxName);
    _storageBox = _hiveApi.box(_generalStorageBoxName);
  }

  List<Recipe> getRecipes({bool excludeDeletedRecipes = false}) {
    List<String>? recipesAsStrings = _getRecipesAsStrings();
    late List<Recipe> recipes;
    if (recipesAsStrings == null) {
      _storeDefaultRecipes();
      recipes = defaultRecipes;
    } else {
      recipes = recipesAsStrings.map((string) {
        return Recipe.fromString(string);
      }).toList();
    }

    if (excludeDeletedRecipes) {
      recipes.removeWhere((recipe) => recipe.isDeleted);
    }

    recipes.sort((a, b) => a.title.toLowerCase().compareTo(b.title.toLowerCase()));
    return recipes;
  }

  Future<void> addToRecipes(Recipe recipe) async {
    List<String>? recipesAsStrings = _getRecipesAsStrings() ?? [];
    recipesAsStrings.add(recipe.toString());
    await storeRecipes(recipesAsStrings);
  }

  List<String>? _getRecipesAsStrings() {
    return _storageBox.get(
      _generalStorageBoxKeyRecipes,
    );
  }

  List<Recipe> getDeletedRecipes() {
    return getRecipes().where((recipe) => recipe.isDeleted).toList();
  }

  List<LabelColor> getLabelColors() {
    try {
      final Map? rawLabelColorsAsMapFromStorage = _storageBox.get(_generalStorageBoxKeyLabelColors);
      if (rawLabelColorsAsMapFromStorage == null) {
        storeLabelColors(_defaultLabelColors);
        return _defaultLabelColors;
      } else {
        final Map<String, int> labelColorsAsMapFromStorage = rawLabelColorsAsMapFromStorage.map((label, colorValue) {
          return MapEntry(label, colorValue);
        });
        final List<LabelColor> labelColors = [];
        labelColors.setFromMap(labelColorsAsMapFromStorage);
        return labelColors;
      }
    } catch (e) {
      return [];
    }
  }

  Future<void> storeLabelColors(List<LabelColor> labelColors) async {
    return await _storageBox.put(
      _generalStorageBoxKeyLabelColors,
      labelColors.toMap(),
    );
  }

  Future<void> setDeletionStatusForRecipe(Recipe recipeToUpdate, {required bool newDeletionStatus}) async {
    final List<Recipe> allRecipes = getRecipes();
    final int index = allRecipes.indexOf(recipeToUpdate);
    allRecipes[index].isDeleted = newDeletionStatus;
    await storeRecipes(allRecipes.map((recipe) => recipe.toString()).toList());
  }

  Future<void> removeFromRecipes(Recipe recipeToRemove) async {
    final List<String> recipesAsStrings = _getRecipesAsStrings() ?? [];
    recipesAsStrings.remove(recipeToRemove.toString());
    await storeRecipes(recipesAsStrings);
  }

  Future<void> _storeDefaultRecipes() async {
    await storeRecipes(
      defaultRecipes.map((recipe) => recipe.toString()).toList(),
    );
  }

  Future<void> storeRecipes(List<String> recipesAsStrings) async {
    await _storageBox.put(
      _generalStorageBoxKeyRecipes,
      recipesAsStrings,
    );
  }

  List<LabelColor> get _defaultLabelColors {
    return const [
      LabelColor("savory", Colors.blue),
      LabelColor("sweet", Colors.pink),
    ];
  }
}
