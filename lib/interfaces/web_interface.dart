import 'dart:convert';

import 'package:captainfoodmaker/data_models/recipe.dart';
import 'package:html/dom.dart';
import 'package:html/parser.dart';
import 'package:http/http.dart';

class WebInterface {
  const WebInterface();

  Uri? uriFromText(String text) {
    final bool startsWithHttp = text.startsWith("http://") || text.startsWith("https://");
    final String textWithHttp = startsWithHttp ? text : "https://$text";
    final Uri? parsed = Uri.tryParse(textWithHttp);
    if (parsed != null && parsed.host.isNotEmpty && parsed.host.replaceFirst("www.", "").contains(".")) {
      return parsed;
    } else {
      return null;
    }
  }

  Future<Response> httpGet(Uri uri) async {
    return await get(uri);
  }

  Future<Recipe?> parseRecipeFromHttpResponseBody(String responseBody) async {
    try {
      final Document document = parse(responseBody);
      final List<Element> scripts = document.getElementsByTagName("script");
      final Iterable<Element> jsonsLd = scripts.where((e) => e.attributes["type"] == "application/ld+json");
      final dynamic jsonLd = json.decode(jsonsLd.first.text);

      late final Recipe recipe;
      if (jsonLd is Map) {
        final Map recipeMap = _getRecipeMapFromJsonLd(jsonLd);
        recipe = _getRecipeFromRecipeMap(recipeMap);
      } else {
        final Iterable<Element> itemProps =
            document.querySelectorAll("*").where((e) => e.attributes.containsKey("itemprop"));
        recipe = _getRecipeFromItemProps(itemProps);
      }
      return recipe;
    } catch (_) {
      return null;
    }
  }

  Map _getRecipeMapFromJsonLd(Map jsonLd) {
    final bool hasContentDirectlyInThisMap =
        ["recipeIngredient", "recipeInstructions"].every((key) => jsonLd.containsKey(key));
    final Map recipeMap = hasContentDirectlyInThisMap
        ? jsonLd
        : jsonLd["@graph"].firstWhere((_map) {
            return _map["@type"] == "Recipe";
          });
    return recipeMap;
  }

  Recipe _getRecipeFromRecipeMap(Map recipeMap) {
    final String title = recipeMap["name"];
    final List<Ingredient> ingredients = _getIngredientsFromRecipeMap(recipeMap);
    final List<String> steps = _getStepsFromRecipeMap(recipeMap);
    return Recipe(
      title,
      labels: const [],
      ingredients: ingredients,
      steps: steps,
    );
  }

  Recipe _getRecipeFromItemProps(Iterable<Element> itemProps) {
    final String title = _getTitleFromItemProps(itemProps);
    final List<Ingredient> ingredients = _getIngredientsFromItemProps(itemProps);
    final List<String> steps = _getStepsFromItemProps(itemProps);
    return Recipe(
      title,
      labels: const [],
      ingredients: ingredients,
      steps: steps,
    );
  }

  List<Ingredient> _getIngredientsFromRecipeMap(Map recipeMap) {
    final List ingredientsStrings = recipeMap["recipeIngredient"];
    final List<Ingredient> ingredients = ingredientsStrings.map((string) => Ingredient.fromString(string)).toList();
    return ingredients;
  }

  List<String> _getStepsFromRecipeMap(Map recipeMap) {
    final List stepsMaps = recipeMap["recipeInstructions"];
    final List<String> steps = [];
    for (Map stepMap in stepsMaps) {
      if (stepMap.containsKey("text")) {
        final String step = Recipe.formatString(stepMap["text"]);
        steps.add(step);
      } else if (stepMap.containsKey("itemListElement")) {
        final List nestedStepMaps = stepMap["itemListElement"];
        for (Map nestedStepMap in nestedStepMaps) {
          if (nestedStepMap.containsKey("text")) {
            final String step = Recipe.formatString(nestedStepMap["text"]);
            steps.add(step);
          }
        }
      }
    }
    return steps;
  }

  String _getTitleFromItemProps(Iterable<Element> itemProps) {
    final Iterable<Element> titleElements = itemProps.where((e) {
      return e.attributes["itemprop"] == "name" && e.localName != "meta" && e.text.trim().isNotEmpty;
    });
    final String title = titleElements.isEmpty ? "" : titleElements.first.text;
    return Recipe.formatString(title);
  }

  List<Ingredient> _getIngredientsFromItemProps(Iterable<Element> itemProps) {
    final Iterable<Element> ingredientsElements = itemProps.where((e) {
      return e.attributes["itemprop"] == "recipeIngredient" && e.text.trim().isNotEmpty;
    });
    final List<Ingredient> ingredients = ingredientsElements.map((e) => Ingredient.fromString(e.text)).toList();
    return ingredients;
  }

  List<String> _getStepsFromItemProps(Iterable<Element> itemProps) {
    final Element stepsElement = itemProps.firstWhere((e) {
      return e.attributes["itemprop"] == "recipeInstructions" && e.text.trim().isNotEmpty;
    });

    List<String> steps = stepsElement.text.split("\n");
    steps = steps.map((step) => step.trim()).toList();
    steps.removeWhere((step) => step.isEmpty);
    steps = steps.map((step) {
      // if `step` starts with a numbered prefix like "1) Preheat oven", remove that prefix
      final int indexOfClosingParen = step.indexOf(")");
      if (indexOfClosingParen == -1 || indexOfClosingParen > 2) {
        // if paren not found or comes later in string, assume no numbered prefix
        return Recipe.formatString(step);
      } else {
        // from "1)" to "99)"
        final String withoutPrefix = step.substring(indexOfClosingParen + 1);
        return Recipe.formatString(withoutPrefix);
      }
    }).toList();

    return steps;
  }
}
