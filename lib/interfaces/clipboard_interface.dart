import 'package:flutter/services.dart';

class ClipboardInterface {
  const ClipboardInterface();

  Future<void> copyTextToClipboard(String _text) async {
    await Clipboard.setData(
      ClipboardData(text: _text),
    );
  }

  Future<String?> getCopiedText() async {
    /// Gets text from clipboard, if possible.
    ///   non-empty String found -> return String
    ///   empty String found -> return `null`
    ///   `null` found -> return `null`
    ///   on exception -> return `null`
    try {
      final ClipboardData? data = await Clipboard.getData(Clipboard.kTextPlain);
      final String? text = data?.text?.trim();
      if (text == null || text.isEmpty) {
        return null;
      } else {
        return text;
      }
    } catch (e) {
      return null;
    }
  }
}
