import 'package:captainfoodmaker/data_models/label_colors.dart';
import 'package:flutter/material.dart';

class LabelColorGradient extends BoxDecoration {
  // todo -- test this
  LabelColorGradient(Color color)
      : super(
          gradient: LinearGradient(
            begin: Alignment.centerLeft,
            end: Alignment.centerRight,
            colors: [
              color.withOpacity(0.1),
              color.withOpacity(0.3),
            ],
            stops: const [0.3, 0.8],
          ),
        );
}

class LabelCard extends StatelessWidget {
  final String title;
  final Color color;

  const LabelCard(
    this.title, {
    required this.color,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      key: key,
      color: Color.alphaBlend(
        Theme.of(context).colorScheme.onTertiaryContainer,
        color,
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 4, horizontal: 8),
        child: Row(
          children: [
            Text(
              title,
              style: Theme.of(context).textTheme.bodyMedium?.copyWith(
                    color: Theme.of(context).colorScheme.onPrimaryContainer,
                  ),
            ),
          ],
        ),
      ),
    );
  }
}

class LabelsRow extends SingleChildScrollView {
  final Iterable<String> labels;
  final List<LabelColor> labelColors;

  LabelsRow(
    this.labels, {
    Key? key,
    required this.labelColors,
  }) : super(
          key: key,
          scrollDirection: Axis.horizontal,
          child: Row(
            children: labels.map((label) {
              return LabelCard(
                label,
                color: labelColors.getColor(label),
              );
            }).toList(),
          ),
        );
}

class ScrollViewEdgeInsets extends EdgeInsets {
  const ScrollViewEdgeInsets({
    bool padTop = true,
    bool padBottom = true,
  }) : super.only(
          top: padTop ? 12.0 : 0.0,
          bottom: padBottom ? 64.0 : 0.0,
        );

  static const ScrollViewEdgeInsets onlyBottom = ScrollViewEdgeInsets(padTop: false);

  static const ScrollViewEdgeInsets onlyTop = ScrollViewEdgeInsets(padBottom: false);

  static const ScrollViewEdgeInsets none = ScrollViewEdgeInsets(padTop: false, padBottom: false);
}

class PaddedListView extends ListView {
  PaddedListView({
    Key? key,
    int? itemCount,
    required IndexedWidgetBuilder itemBuilder,
    bool isShrinkWrapped = false,
  }) : super.builder(
          key: key,
          padding: const ScrollViewEdgeInsets(),
          itemCount: itemCount,
          itemBuilder: itemBuilder,
          shrinkWrap: isShrinkWrapped,
        );
}

class PaddedReorderableListView extends ReorderableListView {
  PaddedReorderableListView({
    Key? key,
    ScrollViewEdgeInsets padding = const ScrollViewEdgeInsets(),
    required List<Widget> children,
    required ReorderCallback? onReorder,
    ScrollController? controller,
  }) : super(
          key: key,
          padding: padding,
          children: children,
          onReorder: onReorder ?? (_, __) {},
          buildDefaultDragHandles: onReorder != null,
          scrollController: controller
        );
}

class ReorderIconButton extends StatelessWidget {
  const ReorderIconButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return IconButton(
      icon: const Icon(Icons.drag_indicator),
      visualDensity: VisualDensity.compact,
      color: Colors.grey,
      onPressed: () {
        ScaffoldMessenger.of(context).clearSnackBars();
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(
            content: Text("Long-press and drag to reorder."),
          ),
        );
      },
    );
  }
}

class CustomPopupMenuItem extends PopupMenuItem<String> {
  CustomPopupMenuItem({
    required String text,
    required String value,
    Key? key,
  }) : super(
          key: key,
          value: value,
          child: Text(text),
        );
}

class PopupMenuItemAddNewRecipe extends CustomPopupMenuItem {
  static const String valueString = "_add_new_recipe"; // can access publicly without an instance

  PopupMenuItemAddNewRecipe({Key? key})
      : super(
          key: key,
          text: "Add new recipe",
          value: valueString,
        );
}

class PopupMenuItemExport extends CustomPopupMenuItem {
  static const String valueString = "_export_share_recipes"; // can access publicly without an instance

  PopupMenuItemExport({Key? key})
      : super(
          key: key,
          text: "Export / share",
          value: valueString,
        );
}

class PopupMenuItemEditLabels extends CustomPopupMenuItem {
  static const String valueString = "_edit_labels"; // can access publicly without an instance

  PopupMenuItemEditLabels({Key? key})
      : super(
          key: key,
          text: "Edit labels",
          value: valueString,
        );
}

class PopupMenuItemDeletedRecipes extends CustomPopupMenuItem {
  static const String valueString = "_deleted_recipes"; // can access publicly without an instance

  PopupMenuItemDeletedRecipes({Key? key})
      : super(
          key: key,
          text: "Deleted recipes",
          value: valueString,
        );
}

class PopupMenuItemDeleteRecipe extends CustomPopupMenuItem {
  static const String valueString = "_delete_recipe"; // can access publicly without an instance

  PopupMenuItemDeleteRecipe({Key? key})
      : super(
          key: key,
          text: "Delete",
          value: valueString,
        );
}

class PopupMenuItemRestoreRecipe extends CustomPopupMenuItem {
  static const String valueString = "_restore_recipe"; // can access publicly without an instance

  PopupMenuItemRestoreRecipe({Key? key})
      : super(
          key: key,
          text: "Restore (un-delete)",
          value: valueString,
        );
}
