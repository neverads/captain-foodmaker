import 'package:captainfoodmaker/interfaces/clipboard_interface.dart';
import 'package:captainfoodmaker/data_models/recipe.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'export_recipes_state.dart';

class ExportRecipesCubit extends Cubit<ExportRecipesState> {
  final ClipboardInterface _clipboardInterface;

  ExportRecipesCubit(
    this._clipboardInterface,
    final List<Recipe> _recipes,
  ) : super(
          ExportRecipesState(
            allRecipes: _recipes,
            selectedRecipes: _recipes,
          ),
        );

  void selectRecipe(Recipe selectedRecipe) {
    final List<Recipe> updatedSelectedRecipes = state.selectedRecipes + [selectedRecipe];

    emit(
      ExportRecipesState(
        allRecipes: state.allRecipes,
        selectedRecipes: updatedSelectedRecipes,
      ),
    );
  }

  void deselectRecipe(Recipe deselectedRecipe) {
    final List<Recipe> updatedSelectedRecipes = state.selectedRecipes.where((recipe) {
      return recipe != deselectedRecipe;
    }).toList();

    emit(
      ExportRecipesState(
        allRecipes: state.allRecipes,
        selectedRecipes: updatedSelectedRecipes,
      ),
    );
  }

  Future<void> exportRecipes() async {
    final Iterable<String> recipesAsStrings = state.selectedRecipes.map((recipe) => recipe.toString());
    const String recipeSeparator = "\n\n\n\n";
    return await _clipboardInterface.copyTextToClipboard(
      recipesAsStrings.join(recipeSeparator),
    );
  }
}
