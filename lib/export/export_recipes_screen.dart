import 'package:captainfoodmaker/data_models/label_colors.dart';
import 'package:captainfoodmaker/data_models/recipe.dart';
import 'package:captainfoodmaker/export/export_recipes_cubit.dart';
import 'package:captainfoodmaker/interfaces/clipboard_interface.dart';
import 'package:captainfoodmaker/shared_widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ExportRecipesScreen extends StatelessWidget {
  final List<Recipe> allRecipes;
  final List<LabelColor> labelColors;

  const ExportRecipesScreen(
    this.allRecipes, {
    Key? key,
    required this.labelColors,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (BuildContext context) {
        return ExportRecipesCubit(context.read<ClipboardInterface>(), allRecipes);
      },
      child: ExportRecipesView(
        labelColors: labelColors,
      ),
    );
  }
}

class ExportRecipesView extends StatelessWidget {
  final List<LabelColor> labelColors;

  const ExportRecipesView({
    Key? key,
    required this.labelColors,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Export / share"),
        actions: [
          infoButton(context),
        ],
      ),
      body: Column(
        children: [
          ListViewOfRecipesToExport(
            labelColors: labelColors,
          ),
          const ExportButton(),
        ],
      ),
    );
  }

  IconButton infoButton(BuildContext context) {
    return IconButton(
      icon: const Icon(Icons.info_outline),
      onPressed: () {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: const Text("Why export/share my recipes?"),
              content: const Text(
                "Maybe you just want to send a recipe to a friend.  "
                "If they also have this app, they can import it to keep a copy.\n"
                "\n"
                "Also, we don't collect your data - at all.\n"
                "\n"
                "That means your recipes exist only on your device - not on a computer somewhere in the cloud.  "
                "If you accidentally delete something, or if a toddler destroys your device, you might want a backup.\n"
                "\n"
                "When you tap the 'Export' button, the app will copy your recipes so that you can paste them somewhere - "
                "as an email, for example.",
              ),
              actions: [
                OutlinedButton(
                  child: Text(
                    "ok",
                    style: Theme.of(context).textTheme.bodyLarge?.copyWith(
                          color: Theme.of(context).colorScheme.onSurfaceVariant,
                        ),
                  ),
                  onPressed: Navigator.of(context).pop,
                ),
              ],
            );
          },
        );
      },
    );
  }
}

class ListViewOfRecipesToExport extends StatelessWidget {
  final List<LabelColor> labelColors;

  const ListViewOfRecipesToExport({
    Key? key,
    required this.labelColors,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final ExportRecipesCubit cubit = context.read<ExportRecipesCubit>();
    final List<Recipe> allRecipes = cubit.state.allRecipes;

    return Expanded(
      child: PaddedListView(
        itemCount: allRecipes.length,
        itemBuilder: (BuildContext context, int index) {
          final Recipe recipe = allRecipes[index];
          return RecipeCheckboxListTile(
            recipe,
            labelColors: labelColors,
          );
        },
        isShrinkWrapped: true,
      ),
    );
  }
}

class RecipeCheckboxListTile extends StatelessWidget {
  final Recipe recipe;
  final List<LabelColor> labelColors;

  const RecipeCheckboxListTile(
    this.recipe, {
    Key? key,
    required this.labelColors,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final ExportRecipesCubit cubit = context.read<ExportRecipesCubit>();

    return BlocBuilder<ExportRecipesCubit, ExportRecipesState>(
      builder: (BuildContext context, state) {
        final bool isChecked = state.selectedRecipes.contains(recipe);
        return CheckboxListTile(
          value: isChecked,
          key: key,
          title: Text(recipe.title),
          subtitle: LabelsRow(
            recipe.labels,
            labelColors: labelColors,
          ),
          onChanged: (bool? _value) {
            isChecked ? cubit.deselectRecipe(recipe) : cubit.selectRecipe(recipe);
          },
        );
      },
    );
  }
}

class ExportButton extends StatelessWidget {
  const ExportButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final ExportRecipesCubit cubit = context.read<ExportRecipesCubit>();

    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 16),
          child: ElevatedButton(
            onPressed: () {
              cubit.exportRecipes();
              ScaffoldMessenger.of(context).clearSnackBars();
              ScaffoldMessenger.of(context).showSnackBar(
                const SnackBar(
                  content: Text(
                    "Recipes copied to clipboard!  You can paste them in a email, text document, etc.",
                  ),
                ),
              );
            },
            child: const Text("Export"),
          ),
        ),
      ],
    );
  }
}
