part of 'export_recipes_cubit.dart';

class ExportRecipesState extends Equatable {
  final List<Recipe> allRecipes;
  final List<Recipe> selectedRecipes;

  const ExportRecipesState({
    required this.allRecipes,
    required this.selectedRecipes,
  });

  @override
  List<Object?> get props => [allRecipes, selectedRecipes];
}
