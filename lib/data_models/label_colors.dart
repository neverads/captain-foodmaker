import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

class LabelColor extends Equatable {
  final String label;
  final Color color;

  const LabelColor(this.label, this.color);

  factory LabelColor.fromMapEntry(MapEntry<String, int> mapEntry) {
    try {
      return LabelColor(mapEntry.key, Color(mapEntry.value));
    } catch (_) {
      return const LabelColor("<error>", Colors.grey);
    }
  }

  LabelColor copyWith({required Color newColor}) {
    return LabelColor(label, newColor);
  }

  LabelColor copy() {
    return LabelColor(label, Color(color.value));
  }

  @override
  List<Object?> get props => [label, color.value];

  MapEntry<String, int> toMapEntry() {
    return MapEntry(label, color.value);
  }
}

extension ListOfLabelColors on List<LabelColor> {
  List<String> get labels {
    final List<String> _labels = [];
    forEach((labelColor) {
      _labels.add(labelColor.label);
    });
    return _labels;
  }

  List<int> get colorsValues {
    final List<int> values = [];
    forEach((labelColor) {
      values.add(labelColor.color.value);
    });
    return values;
  }

  Color getColor(String label, {Color backupColor = Colors.grey}) {
    for (LabelColor labelColor in this) {
      if (labelColor.label == label) {
        return labelColor.color;
      }
    }
    return backupColor;
  }

  void setColor(String label, Color newColor) {
    final List<LabelColor> newLabelColors = [];
    for (LabelColor labelColor in this) {
      if (labelColor.label == label) {
        newLabelColors.add(labelColor.copyWith(newColor: newColor));
      } else {
        newLabelColors.add(labelColor);
      }
    }
    clear();
    addAll(newLabelColors);
    // todo -- getter for `labels` (like `dict.keys`) -- OR MAYBE AN ACTUAL CLASS: LabelColors (a `List<LabelColor>`)
    if (!map((labelColor) => labelColor.label).contains(label)) {
      add(LabelColor(label, newColor));
    }
  }

  Map<String, int> toMap() {
    return Map.fromEntries(
      map((labelColor) => labelColor.toMapEntry()),
    );
  }

  void setFromMap(Map<String, int> _map) {
    clear;
    try {
      addAll(
        _map.entries.map((labelColorAsMapEntry) {
          return LabelColor.fromMapEntry(labelColorAsMapEntry);
        }),
      );
    } catch (_) {
      add(const LabelColor("<error>", Colors.grey));
    }
  }
}
