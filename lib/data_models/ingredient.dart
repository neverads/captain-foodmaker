part of 'recipe.dart';

class Ingredient extends Equatable {
  final String amount;
  final String description;

  const Ingredient(this.amount, this.description);

  static const Ingredient empty = Ingredient("", "");

  factory Ingredient.fromString(String string) {
    // first, remove bullet-prefix  (likely from `toString`)
    if (string.startsWith("• ")) {
      string = string.replaceFirst("• ", "");
    }

    string = Recipe.formatString(string);

    if (string.isEmpty) {
      return const Ingredient("", "");
    }

    final List<String> split = string.split(" ");
    if (split.length == 1) {
      return Ingredient("", split.first);
    }

    int indexOfStartOfDescription = 2;
    if (split.length == 3 && split[1] == "&") {
      indexOfStartOfDescription = 0;
    } else if (["eggs", "egg", "plums"].any((word) => split.contains(word)) ||
        ["c", "g"].any((measurementChar) => split.first.endsWith(measurementChar)) ||
        (split.length > 2 &&
            isNumeric(split.first) &&
            (split[1].toLowerCase().contains("onion") || split.sublist(1, 3).join(" ").startsWith("medium onion"))) ||
        string.contains("shortcrust pastry")) {
      indexOfStartOfDescription -= 1;
    }

    int indexOfMeasurement = -1;
    for (String measurement in ["cups", "teaspoons", "tsp", "tablespoons", "tbsp", "Tbsp"]) {
      indexOfMeasurement = split.indexOf(measurement);
      if (indexOfMeasurement != -1) {
        break;
      }
    }

    if (indexOfMeasurement >= 0) {
      indexOfStartOfDescription = indexOfMeasurement + 1;
    }

    final String amount = split.sublist(0, indexOfStartOfDescription).join(" ");
    final String description = split.sublist(indexOfStartOfDescription).join(" ");
    return Ingredient(amount, description);
  }

  bool get isBlank {
    return amount.trim() == "" && description.trim() == "";
  }

  @override
  List<Object?> get props {
    return [amount, description];
  }

  @override
  String toString() {
    final String _separator = amount.isEmpty ? "" : " ";
    return "• $amount$_separator$description";
  }
}
