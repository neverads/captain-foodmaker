import 'package:equatable/equatable.dart';
import 'package:validators/validators.dart';

part 'ingredient.dart';

class Recipe extends Equatable {
  final String title;
  final List<String> labels;
  final List<Ingredient> ingredients;
  final List<String> steps;
  final String? url;
  bool isDeleted;

  static Recipe blank = Recipe("", labels: const [], ingredients: const [], steps: const []);

  Recipe(
    this.title, {
    required this.labels,
    required this.ingredients,
    required this.steps,
    this.url,
    this.isDeleted = false,
  });

  factory Recipe.fromString(String string) {
    final List<String> splitLines = string.split("\n");
    final bool isDeleted = splitLines.first.endsWith(_deletedSuffix);
    final String title = isDeleted ? splitLines.first.replaceAll(_deletedSuffix, "") : splitLines.first;
    final int indexOfLabelsHeader = splitLines.indexOf("Labels:");
    final int indexOfIngredientsHeader = splitLines.indexOf("Ingredients:");
    final int indexOfStepsHeader = splitLines.indexOf("Steps:");
    final String labelsLine = splitLines[indexOfLabelsHeader + 1];
    final List<String> labels = labelsLine.isEmpty ? [] : labelsLine.split(", ");
    final List<Ingredient> ingredients = splitLines
        .sublist(indexOfIngredientsHeader + 1, indexOfStepsHeader - 1)
        .where((ingredientString) => ingredientString.isNotEmpty)
        .map((ingredientString) => Ingredient.fromString(ingredientString))
        .toList(growable: false);
    List<String> steps = splitLines.sublist(indexOfStepsHeader + 1, splitLines.length - 2).toList();
    steps = steps
        .map((step) {
          final List<String> stepSplitAsWords = step.split(" ");
          if (stepSplitAsWords.length >= 2 &&
              stepSplitAsWords.first.endsWith(")") &&
              double.tryParse(stepSplitAsWords.first.substring(0, stepSplitAsWords.first.length - 1)) != null) {
            return step.substring(step.indexOf(")") + 2);
          } else {
            return step;
          }
        })
        .where((step) => step.isNotEmpty)
        .toList();

    return Recipe(
      title,
      labels: labels,
      ingredients: ingredients,
      steps: steps,
      url: splitLines.last == "null" ? null : splitLines.last,
      isDeleted: isDeleted,
    );
  }

  static String get _deletedSuffix => "<DELETED>";

  static String formatString(String string) {
    /// Makes a `String` ready for use in a `Recipe`.

    // remove excess white-space
    string = string.trim();
    string = string.replaceAll("\t", " ");
    string = string.replaceAll(" ", " "); // `NBSP` -> ` `
    while (string.contains("  ")) {
      string = string.replaceAll("  ", " ");
    }
    string = string.trim();

    // replace non-standard chars
    string = string.replaceAll("&#8217;", "'");
    string = string.replaceAll("&#8243;", '"');
    return string;
  }

  Recipe copyWith({
    Ingredient? addedIngredient,
    String? addedStep,
    String? addedLabel,
    bool blankElementsRemoved = false,
    bool elementsTrimmed = false,
    bool? isDeleted,
  }) {
    String copiedTitle = title.toString();
    List<String> copiedLabels = labels.toList();
    List<Ingredient> copiedIngredients = ingredients.toList();
    List<String> copiedSteps = steps.toList();
    String? copiedUrl = url?.toString();

    if (addedIngredient != null) {
      copiedIngredients.add(addedIngredient);
    }
    if (addedStep != null) {
      copiedSteps.add(addedStep);
    }
    if (addedLabel != null) {
      copiedLabels.add(addedLabel);
    }

    if (blankElementsRemoved) {
      copiedLabels.removeWhere((label) => label.trim() == "");
      copiedIngredients.removeWhere((ingredient) => ingredient.isBlank);
      copiedSteps.removeWhere((step) => step.trim() == "");
    }

    if (elementsTrimmed) {
      copiedTitle = copiedTitle.trim();
      copiedLabels = copiedLabels.map((label) => label.trim()).toList();
      copiedIngredients = copiedIngredients.map((ingredient) {
        // todo -- create `trim()` helper-method for `Ingredient` class, rather than having this logic here
        return Ingredient(
          ingredient.amount.trim(),
          ingredient.description.trim(),
        );
      }).toList();
      copiedSteps = copiedSteps.map((step) => step.trim()).toList();
      copiedUrl = copiedUrl?.trim();
    }

    return Recipe(
      copiedTitle,
      labels: copiedLabels,
      ingredients: copiedIngredients,
      steps: copiedSteps,
      url: copiedUrl,
      isDeleted: isDeleted ?? this.isDeleted,
    );
  }

  @override
  List<Object?> get props {
    /// use one flattened list to avoid issues with nested lists which evaluate as unequal

    final List<dynamic> _props = [];
    _props.add(title);
    _props.addAll(labels);
    _props.addAll(ingredients);
    _props.addAll(steps);
    _props.add(url);
    return _props;
  }

  @override
  String toString() {
    return "$title${isDeleted ? _deletedSuffix : ""}\n"
        "\n"
        "Labels:\n"
        "${labels.join(", ")}\n"
        "\n"
        "Ingredients:\n"
        "${ingredients.map((ingredient) => ingredient.toString()).join("\n")}\n"
        "\n"
        "Steps:\n"
        "${steps.map((step) => "${steps.indexOf(step) + 1}) $step").join("\n")}\n"
        "\n"
        "Web-link:\n"
        "$url";
  }

  Iterable<int> get labelsIndexes {
    return _indexes(labels);
  }

  Iterable<int> get ingredientsIndexes {
    return _indexes(ingredients);
  }

  Iterable<int> get stepsIndexes {
    return _indexes(steps);
  }

  Iterable<int> _indexes(List<dynamic> list) {
    return Iterable<int>.generate(list.length);
  }
}

List<Recipe> get defaultRecipes {
  return [
    Recipe(
      'Chocolate Sandwich Cookies',
      labels: const [
        'sweet',
      ],
      ingredients: const [
        Ingredient('8 oz', 'butter, softened'),
        Ingredient('7.5 oz', 'sugar'),
        Ingredient('2 tsp', 'salt'),
        Ingredient('2', 'eggs'),
        Ingredient('10 oz', 'flour'),
        Ingredient('5.5 oz', 'cocoa powder'),
        Ingredient('1/2 tsp', 'baking soda'),
      ],
      steps: const [
        'Heat oven to 160°C.',
        'Cream butter, sugar, and salt in stand mixer until smooth.',
        'Add eggs and mix.',
        'Sift flour, cocoa powder, and baking soda into bowl and mix until combined.',
        'Transfer to floured surface & knead until it comes together.',
        'Wrap & chill for 1 hour.',
        'Roll out & cut into circles with small cookie cutter or champagne glass.',
        'Bake on a cookie tray with silpat at 160°C for 15 min.',
        'Cool on wire rack.',
      ],
    ),
    Recipe(
      'Chocolate Sandwich Cookies (Filling)',
      labels: const [
        'sweet',
      ],
      ingredients: const [
        Ingredient('4 oz', 'butter'),
        Ingredient('8.8 oz', 'powdered sugar, sifted'),
        Ingredient('1 tsp', 'vanilla'),
      ],
      steps: const [
        'Beat all ingredients in stand mixer bowl until smooth.',
        'Add a small amount to the bottom of half the cookies and top with the other half.',
      ],
    ),
    Recipe(
      'Oatmeal Cream Pies (Cookies)',
      labels: const [
        'sweet',
      ],
      ingredients: const [
        Ingredient('8 oz', 'butter, softened'),
        Ingredient('5.6 oz', 'brown sugar'),
        Ingredient('3.75 oz', 'white sugar'),
        Ingredient('2', 'eggs'),
        Ingredient('1 tsp', 'vanilla'),
        Ingredient('1 Tbsp', 'honey'),
        Ingredient('8.75 oz', 'flour'),
        Ingredient('1 tsp', 'baking soda'),
        Ingredient('1/4 tsp', 'cinnamon'),
        Ingredient('1/2 tsp', 'salt'),
        Ingredient('7.5 oz', 'oats'),
      ],
      steps: const [
        'Heat oven to 180°C.',
        'Cream butter & sugars in stand mixer bowl until light & fluffy.',
        'Add eggs, honey, vanilla.  Mix.',
        'In a separate bowl, mix all dry ingredients except oats.  Add to wet and mix.',
        'Fold in oats.',
        'Transfer to a different bowl & refrigerate for 15 minutes.',
        'Bake at 180°C 8-10 minutes until the edges start to brown.',
      ],
    ),
    Recipe(
      'Oatmeal Cream Pies (Marshmallow Frosting)',
      labels: const [
        'sweet',
      ],
      ingredients: const [
        Ingredient('2', 'egg whites'),
        Ingredient('3.75 oz', 'white sugar'),
        Ingredient('1/4 tsp', 'cream of tartar'),
        Ingredient('1/2 tsp', 'vanilla'),
        Ingredient('1/4 tsp', 'ground clove'),
        Ingredient('1/4 tsp', 'cinnamon'),
      ],
      steps: const [
        'In a double boiler, whisk egg whites, sugar, and cream of tartar until eggs become frothy and sugar dissolves.',
        'Pour egg mixture into a stand mixer bowl & whip on high until stiff peaks form.',
        'Fold in vanilla & spices.',
        'Using a piping bag, add frosting to half the cookies & top with the other half.',
      ],
    ),
    Recipe(
      'Tomato Pie',
      labels: const [
        'savory',
      ],
      ingredients: const [
        Ingredient('1 recipe', 'pie dough'),
        Ingredient('1 kg', 'heirloom tomatoes'),
        Ingredient('1', 'onion, sliced'),
        Ingredient('', 'butter'),
        Ingredient('3 cloves', 'garlic minced'),
        Ingredient('', 'salt & pepper'),
      ],
      steps: const [
        'Heat oven to 180°C.',
        'Melt butter in a medium pan.',
        'Add onions & sauté until translucent.',
        'Add garlic & cook a few more minutes.  Add salt & pepper to taste.',
        'Roll out dough & place on a baking tray.',
        'Add sautéed onions & garlic.  Top with all the sliced tomatoes, sprinkling salt on each layer.',
        'Fold edges of dough over tomatoes.',
        'Bake at 180°C until dough is golden brown.',
      ],
    ),
    Recipe(
      'Veggie Pot Pie',
      labels: const [
        'savory',
      ],
      ingredients: const [
        Ingredient('2', 'medium onions, diced'),
        Ingredient('200 grams', 'oyster mushrooms, chopped'),
        Ingredient('4 stalks', 'celery, diced'),
        Ingredient('250 grams', 'green beans, trimmed and cut'),
        Ingredient('400 grams', 'potatoes, cubed'),
        Ingredient('', 'butter'),
        Ingredient('4 cloves', 'garlic, minced'),
        Ingredient('75 grams', 'flour'),
        Ingredient('2 cups', 'veggie stock'),
        Ingredient('5 sprigs', 'fresh thyme, removed from stems'),
        Ingredient('1/2 cup', 'soy milk'),
        Ingredient('', 'salt & pepper'),
        Ingredient('2x recipe', 'pie dough'),
      ],
      steps: const [
        'Heat oven to 180°C.',
        'While dough is chilling, melt butter in a large pan & sauté onions on medium high until translucent.',
        'Add garlic, mushrooms & celery.  Cook another 5 minutes.',
        'Sprinkle flour on veggies & mix to coat.  Cook yet another 5 minutes.',
        'Slowly add veggie stock & mix.  (Should be a bit thick.)  Mix in soy milk.',
        'Add thyme, salt, and pepper.  Remove from heat.',
        'In a large pot, boil potatoes & green beans in salted water for 10 minutes.  Drain & add to sauce.  Mix.',
        'Roll out 2/3 dough.  In an oiled/buttered 9" x 13" glass dish, put the dough on the bottom & up the sides.  '
            'Make sure there are no holes.',
        'Pour the veggie sauce mixture into the dish.',
        'Roll out the other 1/3 of the dough.  Make it big enough to cover the top of the pie.  '
            'Cut a few slits in the dough to let steam escape.  Place on top of the pie, and fold or crimp the edges.',
        'Bake for 30-40 minutes at 180°C until golden brown.',
      ],
    ),
  ];
}
